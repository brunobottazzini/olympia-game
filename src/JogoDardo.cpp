#include "JogoDardo.h"
#include "MenuFases.h"

JogoDardo::JogoDardo(IVideoDriver* driver, 
			         ISceneManager* smgr,
			         IGUIEnvironment* guienv, 
			         IrrlichtDevice *device,
				     ISoundEngine* sound_engine,
					 struct sJogo* jogo){

	this->driver = driver;
	this->smgr = smgr;
	this->guienv = guienv;
	this->device = device;
	this->sound_engine = sound_engine;
	this->jogo = jogo;

	this->w_width = driver->getScreenSize().Width;  // largura da tela
	this->w_height = driver->getScreenSize().Height; // altura da tela




}


// M�TODO: createMenuPrincipal
// DESCRI��O: d� in�cio � cria��o da tela com o menu de escolha de fases
// ARGUMENTOS: nenhum
// RETORNO: void
void JogoDardo::createJogoDardo(short dificuldade){

	JogoDardo receiver_options_jogodardo(this->driver, this->smgr, this->guienv, this->device, this->sound_engine, this->jogo);
	this->device->setEventReceiver(&receiver_options_jogodardo);
	
	// Limpa a cena e os elementos GUI
	this->smgr->clear();
	this->guienv->clear();

	drawWindowButtons();

	
	drawLaneFloor();
	activateMainCamera();

	// Desenha a cena criada
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(0,0,0,0));
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}

}




void JogoDardo::drawMainChar(){

	//Main_Char* main_char = new Main_Char(this->device, this->driver, &this->receiver_options_jogodardo, "system//models//menina//menina_I.md2", "system//images//texture//menina.jpg",this->w_width, this->w_height, true);
	//main_char->setHUDToBurningVideo();

}




// Carrega e desenha a pista de lan�amento
void JogoDardo::drawLaneFloor(){

	scene::ILightSceneNode* sol = this->smgr->addLightSceneNode(0, core::vector3df(0,0,00),
	video::SColorf(1.0f, 1.0f, 1.0f, 1.0f), 60000000);
	SLight & l = sol->getLightData();
	l.Type = ELT_DIRECTIONAL;
	sol->enableCastShadow(true);
	sol->setRotation(vector3df(90,-90,20));
	scene::ILightSceneNode* sol2 = this->smgr->addLightSceneNode(0, core::vector3df(0,0,00),
	video::SColorf(1.0f, 1.0f, 1.0f, 1.0f), 60000000);
	SLight & l2 = sol2->getLightData();
	l2.Type = ELT_DIRECTIONAL;
	sol2->enableCastShadow(true);
	sol2->setRotation(vector3df(90,-90,20));


	// CARREGANDO E CONFIGURANDO CENARIO PISTA
	IAnimatedMesh* floor_mesh = this->smgr->getMesh("system//models//coliseum//pista.3DS");
	smgr->getMeshManipulator()->makePlanarTextureMapping(floor_mesh->getMesh(0), 0.004f);
	floor_mesh->setMaterialFlag(EMF_LIGHTING, true);
	floor_mesh->setMaterialFlag(EMF_NORMALIZE_NORMALS, true);
	this->smgr->getMeshManipulator()->makePlanarTextureMapping(
	floor_mesh->getMesh(0),0.011f);
	scene::ISceneNode* floor_scene = 0;
	IAnimatedMeshSceneNode* floor = this->smgr->addAnimatedMeshSceneNode( floor_mesh );
	floor->setMaterialTexture(0,this->driver->getTexture("system//images//texture//pista//pista.jpg"));
	//floor->setMaterialFlag(video::EMF_WIREFRAME, true);
	core::vector3df floorpos = floor->getAbsolutePosition();
	floorpos.Y -= 60;
	floor->setPosition(floorpos);
	floor->setRotation(vector3df(0,90,0));
	floor->setPosition(vector3df(3000,-60,100));
	floor->getMaterial(0).Shininess = 0.0f;
	// POSI��O DA SEGUNDA PISTA
	IAnimatedMeshSceneNode* floor2 = this->smgr->addAnimatedMeshSceneNode( floor_mesh );
	floor2->setMaterialTexture(0,this->driver->getTexture("system//images//texture//pista//pista.jpg"));
	core::vector3df floorpos2 = floor->getAbsolutePosition();
	floorpos2.Y -= 60;
	floor2->setPosition(floorpos);
	floor2->setRotation(vector3df(0,90,0));
	floor2->setPosition(vector3df(3000,-60,470));
	// FIM CARREGANDO E CONFIGURANDO CENARIO PISTA

}



void JogoDardo::activateMainCamera(){

	this->smgr->addCameraSceneNode(0, vector3df(0,30,-40), vector3df(0,5,0));
    scene::ICameraSceneNode* main_camera = this->device->getSceneManager()->getActiveCamera();
	//player1->setCameraControl(false);
	//player1->moveCameraControl(this->device);

}



void JogoDardo::drawSkipButton(){


	// Bot�o para iniciar novo jogo
	//IGUIButton* btn_intro_corrida = guienv->addButton(rect<s32>( w_width - 138, w_height - 145, w_width - 10, w_height - 79), 0, GUI_ID_BUTTON_IC_PULAR_INTRO, L"");
	//btn_intro_corrida->setImage(driver->getTexture("system//images//menus//btn_pular_intro.png"));


}


void JogoDardo::drawWindowButtons(){

	// Bot�o para minimizar a janela
	IGUIButton* btn_window_minimize = guienv->addButton(rect<s32>(w_width - 64, 0, w_width - 32, 32), 0, GUI_ID_BUTTON_JD_WINDOWS_MINIMIZE, L"");
	btn_window_minimize->setImage(driver->getTexture("system//images//menus//btn_window_minimize.png"));

	// Bot�o para fechar a janela
	IGUIButton* btn_window_close = guienv->addButton(rect<s32>(w_width - 32, 0, w_width, 32), 0, GUI_ID_BUTTON_JD_WINDOWS_CLOSE, L"");
	btn_window_close->setImage(driver->getTexture("system//images//menus//btn_window_close.png"));

}




// M�TODO: OnEvent - sobrecarga
// DESCRI��O: trata eventos ocorridos na tela (clique de mouse, teclas pressionadas, etc)
// ARGUMENTOS:
//     - const SEvent& event: struct contendo dados sobre o evento
// RETORNO: True se o evento foi processado
bool JogoDardo::OnEvent(const SEvent& event){

	/*MenuPrincipal* menu_principal = new MenuPrincipal(this->driver, 
		                                             this->smgr,
										             this->guienv, 
										             this->device,
										             this->sound_engine);*/


	/*MenuFases* menu_fases = new MenuFases(this->driver, 
		                                  this->smgr,
										  this->guienv, 
										  this->device,
										  this->sound_engine,
										  this->jogo);*/


	/*
		Neste metodo e descrito acoes que serao dadas caso haja um botao (que foi passado o ID) ira realizar
		Lembre-se que este ID e declarado no struct enum.
	*/
	if (event.EventType == EET_GUI_EVENT){

		s32 id = event.GUIEvent.Caller->getID();
		IGUIEnvironment* env = this->device->getGUIEnvironment();

		switch(event.GUIEvent.EventType){
			
			// Caso o tipo de evento seja clique em um bot�o da GUI
			case EGET_BUTTON_CLICKED:
				switch(id){

					/*case GUI_ID_BUTTON_IC_PULAR_INTRO:
						this->sound_engine->stopAllSounds();
						menu_fases->createMenuFases();
						return true;*/

				    case GUI_ID_BUTTON_JD_WINDOWS_MINIMIZE:
						this->device->minimizeWindow();
						return true;
					case GUI_ID_BUTTON_JD_WINDOWS_CLOSE:
						exit(1);
						return true;

					default:
						return false;

				}
				break;

			default:
				break;
		}
	}

	return false;
}




JogoDardo::~JogoDardo(void){
}
