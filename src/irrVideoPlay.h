#ifndef __IRRVIDEOPLAY_H_INCLUDED__
#define __IRRVIDEOPLAY_H_INCLUDED__

#include <cv.h>
#include<irrlicht.h>
#include <highgui.h> 

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;
using namespace std;

#ifdef _IRR_WINDOWS_
#pragma comment(lib, "highgui210.lib")
#pragma comment(lib, "highgui210d.lib")
#pragma comment(lib, "cv210.lib")
#pragma comment(lib, "cxcore210.lib")
#endif

class irrVideoPlay{
    public:
		 irrVideoPlay();
	     void setVideoPath(char path []);
		 void setFPS(int fps);
		 void playVideoInBurning(ISceneManager* smgr, IVideoDriver* driver);
		 void playVideo(ISceneManager* smgr, IVideoDriver* driver);
    private:
		ITexture *create_ITexture_from_CvCapture(IplImage* img, int frameH, int frameW, IVideoDriver* driver);
		IplImage  *frame;
		CvCapture *capture;
		int altura;
		int largura;
		int posx;
		int posy;
		int fps;
};

#endif
