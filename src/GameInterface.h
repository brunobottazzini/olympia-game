/*
Model of the Class to create the game interface (menu bar, exit button, etc)
�bner Zanetti
*/

#ifndef __GAMEINTERFACE_H_INCLUDED__
#define __GAMEINTERFACE_H_INCLUDED__

#include <irrlicht.h>
#include <irrKlang.h>
#include <string>

#include "Time.h"

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;
using namespace std;
using namespace irrklang;

#ifdef _IRR_WINDOWS_
#pragma comment(lib, "Irrlicht.lib")
#pragma comment(lib, "irrKlang.lib") // link with irrKlang.dll
#pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#endif



enum btnGameInterface
{
	GUI_ID_BUTTON_WINDOW_CLOSE = 100

};



class MenuEventReceiver_GAMEINTERFACE : public IEventReceiver{
	
	public:
		MenuEventReceiver_GAMEINTERFACE(IVideoDriver* driver, 
			                   ISceneManager* smgr,
			                   IGUIEnvironment* guienv, 
			                   IrrlichtDevice *device);

		virtual bool OnEvent(const SEvent& event);
		void createGameInterface();

	private:
		IVideoDriver* driver_interface;
		ISceneManager* smgr_interface;
		IGUIEnvironment* env_interface;
		IrrlichtDevice *dev_interface;

		void createWindowButtons();

};


#endif