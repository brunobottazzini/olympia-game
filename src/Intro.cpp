#include "Intro.h"

Intro::Intro(IVideoDriver* driver, ISceneManager* smgr, IGUIEnvironment* guienv, IrrlichtDevice *device, MyEventReceiver* receiver){
	this->driver = driver;
	this->smgr = smgr;
	this->guienv = guienv;
	this->device = device;
	this->fader = this->guienv->addInOutFader();
	this->receiver3 = receiver;
	this->ratio = 2;
}
void Intro::setRatio(int ratio){
	this->ratio = ratio;
}

void Intro::playIntroGame1(){
	ITimer* mainTime = device->getTimer();
	mainTime->setTime(0);
	mainTime->start();
	u32 t;
	this->sound_engine = createIrrKlangDevice();
	smgr->addCameraSceneNode(0, vector3df(0,30,-40), vector3df(0,5,0));
	scene::ISceneNode* bill;
	if(this->ratio == 1)bill = smgr->addBillboardSceneNode(0, core::dimension2d<f32>(100, 90));
	else if(this->ratio == 2)bill = smgr->addBillboardSceneNode(0, core::dimension2d<f32>(130, 90));
	else if(this->ratio == 3)bill = smgr->addBillboardSceneNode(0, core::dimension2d<f32>(120, 90));
	bill->setMaterialFlag(video::EMF_LIGHTING, false);
	bill->setPosition(vector3df(0.0f,0.0f,0.0f));
	bill->setScale(vector3df(0.1f,0.1f,0.1f));
	fader->fadeIn(500);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro//vd1.jpg"));
	ISound* intro_sound = this->sound_engine->play3D("system//images//vd//intro//intro.mp3",
	                              vec3df(0,0,0), false, false, true);
	
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		t = mainTime->getTime();
	    t = t / 1000; // TRANSFORM TO SECONDS
		if(t >= 5)break;
	}
	fader->fadeOut(500);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		if(teste)break;
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}
	fader->fadeIn(500);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro//vd2.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		t = mainTime->getTime();
	    t = t / 1000; // TRANSFORM TO SECONDS
		if(t >= 10)break;
		//if(teste || receiver3->IsKeyDown(KEY_SPACE))break;
	}
	fader->fadeOut(500);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		if(teste)break;
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}
		fader->fadeIn(500);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro//vd3.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		t = mainTime->getTime();
	    t = t / 1000; // TRANSFORM TO SECONDS
		if(t >= 15)break;
		//if(teste || receiver3->IsKeyDown(KEY_SPACE))break;
	}
	fader->fadeOut(500);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		if(teste)break;
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}
	fader->fadeIn(500);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro//vd4.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		t = mainTime->getTime();
	    t = t / 1000; // TRANSFORM TO SECONDS
		if(t >= 20)break;
		//if(teste || receiver3->IsKeyDown(KEY_SPACE))break;
	}
	fader->fadeOut(500);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		if(teste)break;
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}
	fader->fadeIn(500);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro//vd5.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		t = mainTime->getTime();
	    t = t / 1000; // TRANSFORM TO SECONDS
		if(t >= 30)break;
		//if(teste || receiver3->IsKeyDown(KEY_SPACE))break;
	}
	fader->fadeOut(500);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		if(teste)break;
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}
	fader->fadeIn(500);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro//vd6.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		t = mainTime->getTime();
	    t = t / 1000; // TRANSFORM TO SECONDS
		if(t >= 34)break;
		//if(teste || receiver3->IsKeyDown(KEY_SPACE))break;
	}
	fader->fadeOut(500);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		if(teste)break;
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}
	fader->fadeIn(500);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro//vd7.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		t = mainTime->getTime();
	    t = t / 1000; // TRANSFORM TO SECONDS
		if(t >= 37)break;
		//if(teste || receiver3->IsKeyDown(KEY_SPACE))break;
	}
	fader->fadeOut(500);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		if(teste)break;
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}
	fader->fadeIn(500);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro//vd8.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		t = mainTime->getTime();
	    t = t / 1000; // TRANSFORM TO SECONDS
		if(t >= 40)break;
		//if(teste || receiver3->IsKeyDown(KEY_SPACE))break;
	}
	fader->fadeOut(500);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		if(teste)break;
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}
	fader->fadeIn(500);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro//vd9.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		t = mainTime->getTime();
	    t = t / 1000; // TRANSFORM TO SECONDS
		if(t >= 46)break;
		//if(teste || receiver3->IsKeyDown(KEY_SPACE))break;
	}
	fader->fadeOut(500);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		if(teste)break;
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}
	fader->fadeIn(500);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro//vd10.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		t = mainTime->getTime();
	    t = t / 1000; // TRANSFORM TO SECONDS
		if(t >= 50)break;
		//if(teste || receiver3->IsKeyDown(KEY_SPACE))break;
	}
	fader->fadeOut(500);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		if(teste)break;
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}
	fader->fadeIn(500);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro//vd11.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		t = mainTime->getTime();
	    t = t / 1000; // TRANSFORM TO SECONDS
		if(t >= 56)break;
		//if(teste || receiver3->IsKeyDown(KEY_SPACE))break;
	}
	fader->fadeOut(200);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		if(teste)break;
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}
	fader->fadeIn(200);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro//vd12.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		t = mainTime->getTime();
	    t = t / 1000; // TRANSFORM TO SECONDS
		if(t >= 58)break;
		//if(teste || receiver3->IsKeyDown(KEY_SPACE))break;
	}
		fader->fadeOut(200);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		if(teste)break;
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}
	fader->fadeIn(200);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro//vd13.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		t = mainTime->getTime();
	    t = t / 1000; // TRANSFORM TO SECONDS
		if(t >= 60)break;
		//if(teste || receiver3->IsKeyDown(KEY_SPACE))break;
	}
			fader->fadeOut(200);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		if(teste)break;
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}
	fader->fadeIn(200);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro//vd14.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		t = mainTime->getTime();
	    t = t / 1000; // TRANSFORM TO SECONDS
		if(t >= 64)break;
		//if(teste || receiver3->IsKeyDown(KEY_SPACE))break;
	}
	fader->fadeOut(500);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		if(teste)break;
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}
	fader->fadeIn(500);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro//vd15.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		t = mainTime->getTime();
	    t = t / 1000; // TRANSFORM TO SECONDS
		if(intro_sound->isFinished())break;
		//if(teste || receiver3->IsKeyDown(KEY_SPACE))break;
	}
}
void Intro::playLogoIntro(){
	scene::ISceneNode* bill = smgr->addBillboardSceneNode(0, core::dimension2d<f32>(140, 100));
	bill->setMaterialFlag(video::EMF_LIGHTING, false);
	bill->setPosition(vector3df(0.0f,0.0f,0.0f));
	bill->setScale(vector3df(0.1f,0.1f,0.1f));
	if(receiver3->IsKeyDown(KEY_SPACE))fader->fadeIn(3000);
	bill->setMaterialTexture(0, driver->getTexture("system//images//intro//logo_unesp.jpg"));
	receiver3->clearKeyDown(KEY_SPACE);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		if(teste || receiver3->IsKeyDown(KEY_SPACE))break;
	}
	if(!receiver3->IsKeyDown(KEY_SPACE))fader->fadeOut(3000);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		if(teste || receiver3->IsKeyDown(KEY_SPACE))break;
	}
	receiver3->clearKeyDown(KEY_SPACE);
	if(!receiver3->IsKeyDown(KEY_SPACE))fader->fadeIn(3000);
	bill->setMaterialTexture(0, driver->getTexture("system//images//intro//logo_metrocamp.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		if(teste || receiver3->IsKeyDown(KEY_SPACE))break;
	}
	if(!receiver3->IsKeyDown(KEY_SPACE))fader->fadeOut(3000);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		if(teste || receiver3->IsKeyDown(KEY_SPACE))break;
	}
	bill->setMaterialTexture(0,driver->getTexture("system//images//intro//logo_tech.jpg"));
	receiver3->clearKeyDown(KEY_SPACE);
	if(!receiver3->IsKeyDown(KEY_SPACE))fader->fadeIn(3000);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		if(teste || receiver3->IsKeyDown(KEY_SPACE))break;
	}
	if(!receiver3->IsKeyDown(KEY_SPACE))fader->fadeOut(3000);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		if(teste || receiver3->IsKeyDown(KEY_SPACE))break;
	}
	bill->setMaterialTexture(0,driver->getTexture("system//images//intro//logo_tech2.jpg"));
	receiver3->clearKeyDown(KEY_SPACE);
	if(!receiver3->IsKeyDown(KEY_SPACE))fader->fadeIn(3000);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		if(teste || receiver3->IsKeyDown(KEY_SPACE))break;
	}
	if(!receiver3->IsKeyDown(KEY_SPACE))fader->fadeOut(3000);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		if(teste || receiver3->IsKeyDown(KEY_SPACE))break;
	}
}