#include "GameInterface.h"


/*  */
MenuEventReceiver_GAMEINTERFACE::MenuEventReceiver_GAMEINTERFACE(IVideoDriver* driver, 
	                                           ISceneManager* smgr, 
											   IGUIEnvironment* guienv, 
											   IrrlichtDevice *device){

	this->driver_interface = driver;
	this->smgr_interface = smgr;
	this->env_interface = guienv;
	this->dev_interface = device;
	
}


/* 
Cria um novo quiz
*/
void MenuEventReceiver_GAMEINTERFACE::createGameInterface(){

	
	MenuEventReceiver_GAMEINTERFACE receiver_options_game_interface(driver_interface, smgr_interface, env_interface, dev_interface);
	this->dev_interface->setEventReceiver(&receiver_options_game_interface);

	createWindowButtons();

}



void MenuEventReceiver_GAMEINTERFACE::createWindowButtons(){
	


	IGUIButton* btn_window_close = this->env_interface->addButton(rect<s32>(this->driver_interface->getScreenSize().Width - 30, 0, this->driver_interface->getScreenSize().Width, 30), 0, GUI_ID_BUTTON_WINDOW_CLOSE, L"X");
	//btn_answer_a->setImage(driver->getTexture("system//images//menus//btn_quiz_a.png"));


}




bool MenuEventReceiver_GAMEINTERFACE::OnEvent(const SEvent& event)
{

	/*
		Neste metodo sao descritas acoes que serao dadas caso haja um botao (que foi passado o ID) ira realizar
	*/
	if (event.EventType == EET_GUI_EVENT)
	{
		s32 id = event.GUIEvent.Caller->getID();
		IGUIEnvironment* env = this->dev_interface->getGUIEnvironment();

		switch(event.GUIEvent.EventType)
		{
			case EGET_SCROLL_BAR_CHANGED:
				break;

			case EGET_LISTBOX_CHANGED:
				break;

			case EGET_LISTBOX_SELECTED_AGAIN:
				break;

			case EGET_CHECKBOX_CHANGED:
				break;


			case EGET_BUTTON_CLICKED:
				switch(id)
				{
				case GUI_ID_BUTTON_WINDOW_CLOSE:
					exit(1);
					return true;
				default:
					return false;
				}
				break;

			default:
				break;
		}
	}

	return false;
}