/*
Classe que ira nos preparar subjanelas e acoes para serem tomada ao decorrer do jogo.
Muito Importante para customizacao do jogo.


Bruno Lara Bottazzini
*/
#include "Stage.h"
#include "Menu_Game.h"
#include "Quiz.h"
#include "Menufases.h"



MenuEventReceiver_MAINGAME::MenuEventReceiver_MAINGAME(SAppContext2 & context) : Context(context), 
	selectedDificuldade(0), som(1), musica(1), iniciar_corrida(0){
	/*
		Construtor da Classe de Menu repare que os valores iniciais estao sendo preparados.
	*/
  
	Context.som = som;
	Context.musica = musica;
	Context.iniciar_corrida = iniciar_corrida;
	Context.nivelDificuldade = selectedDificuldade;
	Context.iniciar_campanha = false;
	Context.okay_button_clicked = false;
	IGUIEnvironment* env = Context.device->getGUIEnvironment();
	this->window_choose_game = env->addWindow(
						rect<s32>(100 + 30, 0 + 30, 600 + 30, 400 + 30),
						false, // modal?
						L"Treinamento");
	this->window_choose_game->setVisible(false);
	this->window_choose_game->setEnabled(false);
	this->window_choose_game->getCloseButton()->setVisible(false);
	this->set_color_training = false;

	this->window = env->addWindow(
						rect<s32>(100 + 30, 0 + 30, 600 + 30, 400 + 30),
						false, // modal?
						L"Op��es");
	this->window->setVisible(false);
	this->window->setEnabled(false);
	this->window->getCloseButton()->setVisible(false);
	this->set_color_level = false;
}

bool MenuEventReceiver_MAINGAME::OnEvent(const SEvent& event)
{

	/*
		Neste metodo e descrito acoes que serao dadas caso haja um botao (que foi passado o ID) ira realizar
		Lembre-se que este ID e declarado no struct enum.
	*/

	//Stage* cena = new Stage();

	SAppContext3 context;
	sQuiz s_quiz;
	sQuestion s_question;

	IVideoDriver* driver = Context.device->getVideoDriver();
	ISceneManager* smgr = Context.device->getSceneManager();
	/*MenuEventReceiver_QUIZ* quiz = new MenuEventReceiver_QUIZ(driver, 
		                                                      smgr,
		                                                      Context.device->getGUIEnvironment(), 
															  Context.device,
															  this->Context.sound_engine,
															  &s_quiz,
															  &s_question);*/
	
	/*MenuFases* menu_fases = new MenuFases(driver, 
		                                  smgr,
		                                  Context.device->getGUIEnvironment(), 
										  Context.device,
										  this->Context.sound_engine);*/



	if (event.EventType == EET_GUI_EVENT)
	{
		s32 id = event.GUIEvent.Caller->getID();
		IGUIEnvironment* env = Context.device->getGUIEnvironment();


		// Testes
		


		switch(event.GUIEvent.EventType)
		{
			case EGET_SCROLL_BAR_CHANGED:
				if (id == GUI_ID_TRANSPARENCY_SCROLL_BAR_MAIN_GAME)
				{
					s32 pos = ((IGUIScrollBar*)event.GUIEvent.Caller)->getPos();
				
					for (u32 i=0; i<EGDC_COUNT ; ++i)
					{
						SColor col = env->getSkin()->getColor((EGUI_DEFAULT_COLOR)i);
						col.setAlpha(pos);
						env->getSkin()->setColor((EGUI_DEFAULT_COLOR)i, col);
					}
				
				}
				break;

			case EGET_LISTBOX_CHANGED:
				Context.nivelDificuldade = Context.dificuldadeBox->getSelected();
				break;

			case EGET_LISTBOX_SELECTED_AGAIN:
				Context.nivelDificuldade = Context.dificuldadeBox->getSelected();
				break;

			case EGET_CHECKBOX_CHANGED:
				Context.som = ((gui::IGUICheckBox*)Context.som_check)->isChecked();
				Context.som_check->setChecked(Context.som);

				Context.musica = ((gui::IGUICheckBox*)Context.musica_check)->isChecked();
				Context.musica_check->setChecked(Context.musica);
				break;


			case EGET_BUTTON_CLICKED:
				switch(id)
				{
				case GUI_ID_START_CAMPANHA:

					/*if(Context.iniciar_campanha == false){
						Context.iniciar_campanha = true;
					}else{
						Context.iniciar_campanha = false;
					}*/
					//menu_fases->createMenuFases();

					return true;
				case GUI_ID_QUIT_BUTTON_MAIN_GAME:
					Context.device->closeDevice();
					Context.device->drop();
					exit(1);
					return true;

				case GUI_ID_WINDOW_OPTIONS_START_RUNNING:
					if(Context.iniciar_corrida == false){
						Context.iniciar_corrida = true;
					}
					return true;
				

				case GUI_ID_SHOW_QUIZ:
					//this->Context.sound_engine->stopAllSounds();
					//menu_fases->createMenuFases();
					return true;


				case GUI_ID_WINDOW_OPTIONS_CHOOSE_CONTEST:
					{
						
					this->window_choose_game->setVisible(true);
					this->window_choose_game->setEnabled(true);
					if(this->set_color_training == false){
						set_color_training = true;
						env->addStaticText(L"Selecione a prova desejada para treino.",rect<s32>(60,60,200,120),
							false, // border?
							true, // wordwrap?
							this->window_choose_game);
											SColor col;
						col.set(95,157,63,53);
						env->getSkin()->setColor((EGDC_ACTIVE_BORDER), col);
						env->getSkin()->setColor((EGDC_HIGH_LIGHT), col);
						SColor col_back = env->getSkin()->getColor((EGDC_3D_FACE));
					
						env->addButton(rect<s32>(100, 140,100+110, 140 + 32), this->window_choose_game, GUI_ID_WINDOW_OPTIONS_START_RUNNING,
						L"Corrida", L"Jogue o nosso jogo de Corrida!");

						IGUIButton* dardo = env->addButton(rect<s32>(100, 240,100+110, 240 + 32), this->window_choose_game, GUI_ID_OK_BUTTON_OPTIONS_MAIN_GAME,
						L"Dardo", L"Jogue o nosso jogo de Lan�amento de Dardos!");

						IGUIButton* salto = env->addButton(rect<s32>(300, 140,300+110, 140 + 32), this->window_choose_game, GUI_ID_OK_BUTTON_OPTIONS_MAIN_GAME,
						L"Salto", L"Jogue o nosso jogo para dar Saltos e ser o melhor!");

						IGUIButton* disco = env->addButton(rect<s32>(300, 240,300+110, 240 + 32), this->window_choose_game, GUI_ID_OK_BUTTON_OPTIONS_MAIN_GAME,
						L"Disco", L"Jogue o nosso jogo de Lan�amento de Discos!");

						
						env->addButton(rect<s32>(350, 340,350+110, 340 + 32), this->window_choose_game, GUI_ID_OK_BUTTON_OPTIONS_TRAINNING,
						L"Cancelar", L"Cancela a escolha de modalidades para treinamento");
					
						dardo->setEnabled(false);
						salto->setEnabled(false);
						disco->setEnabled(false);
						}
					  return true;	 
					}
				case GUI_ID_OK_BUTTON_OPTIONS_TRAINNING:
					this->window_choose_game->setVisible(false);
					this->window_choose_game->setEnabled(false);
					return true;
				case GUI_ID_WINDOW_OPTIONS_MAIN_GAME: 
					{
					this->window->setVisible(true);
					this->window->setEnabled(true);
					if(this->set_color_level == false){
						this->set_color_level = true;
						env->addStaticText(L"Selecione o N�vel de Dificuldade",rect<s32>(60,60,200,120),
							false, // border?
							true, // wordwrap?
							this->window);
						SColor col;
						col.set(95,157,63,53);
						env->getSkin()->setColor((EGDC_ACTIVE_BORDER), col);
						env->getSkin()->setColor((EGDC_HIGH_LIGHT), col);

						Context.dificuldadeBox = env->addListBox(core::rect<int>(10,100,220,160), this->window, EGET_LISTBOX_CHANGED);
						Context.dificuldadeBox->addItem(L"F�cil");
						Context.dificuldadeBox->addItem(L"M�dio");
						Context.dificuldadeBox->addItem(L"D�ficil");
						Context.dificuldadeBox->setSelected(Context.nivelDificuldade);

						Context.som_check = env->addCheckBox(som, core::rect<int>(350,60,350+110,75),
							this->window, EGET_CHECKBOX_CHANGED, L"Habilitar Som");
						Context.som_check->setChecked(Context.som);

					
						Context.musica_check = env->addCheckBox(musica, core::rect<int>(350,90,350+120,105), 
							this->window, EGET_CHECKBOX_CHANGED, L"Habilitar a M�sica");
						Context.musica_check->setChecked(Context.musica);

						env->addButton(rect<s32>(350, 340,350+110, 340 + 32), this->window, GUI_ID_OK_BUTTON_OPTIONS_MAIN_GAME,
						L"OK", L"Salvar as op��es colocadas");
					}
					return true;
					}

				case GUI_ID_OK_BUTTON_OPTIONS_MAIN_GAME:
					Context.okay_button_clicked = true; // SINALIZA QUE IRA GRAVAR EM UM ARQUIVO
					//this->window->remove();
					this->window->setVisible(false);
					this->window->setEnabled(false);
					return true;
				default:
					return false;
				}
				break;

			default:
				break;
		}
	}

	return false;
}