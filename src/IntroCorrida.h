/*
Header para a classe que mostra a intro do jogo de corrida
�bner Zanetti
*/

#ifndef __INTROCORRIDA_H_INCLUDED__
#define __INTROCORRIDA_H_INCLUDED__

#include <irrlicht.h>
#include <irrKlang.h>

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;
using namespace std;
using namespace irrklang;

#ifdef _IRR_WINDOWS_
#pragma comment(lib, "Irrlicht.lib")
#pragma comment(lib, "irrKlang.lib") // link with irrKlang.dll
#pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#endif



enum eBtnIntroCorrida
{
	GUI_ID_BUTTON_IC_PULAR_INTRO = 1,
	GUI_ID_BUTTON_IC_WINDOWS_CLOSE = 16,
	GUI_ID_BUTTON_IC_WINDOWS_MINIMIZE = 17,
	GUI_ID_BUTTON_IC_MAIN_MENU = 18

};




class IntroCorrida : public IEventReceiver{

    public:
	    IntroCorrida(IVideoDriver* driver, 
			      ISceneManager* smgr,
			      IGUIEnvironment* guienv, 
			      IrrlichtDevice *device,
				  ISoundEngine* sound_engine,
				  struct sJogo* jogo);

		void createIntroCorrida();
		
		virtual bool OnEvent(const SEvent& event);
        virtual ~IntroCorrida(void);
		

	private:
		IVideoDriver* driver;
		ISoundEngine* sound_engine;
		ISound* intro_music;
		ISceneManager* smgr;
		IGUIEnvironment* guienv;
		IrrlichtDevice* device;
		IGUIInOutFader* intro_fader;
		IGUIInOutFader* fader;
		sJogo* jogo;

		int selected_gender;
        
		int w_width;
		int w_height;

		void playIntroCorrida();
		void playIntroCorridaNarration();
		void drawBackground();
		void drawWindowButtons();
		void drawSkipButton();


};


#endif
