#include "IntroGame.h"


IntroGame::IntroGame(IVideoDriver* driver,
			         ISceneManager* smgr,
			         IGUIEnvironment* guienv,
			         IrrlichtDevice *device,
				     ISoundEngine* sound_engine){

	this->driver = driver;
	this->smgr = smgr;
	this->guienv = guienv;
	this->device = device;
	this->sound_engine = sound_engine;

	this->w_width = driver->getScreenSize().Width;  // largura da tela
	this->w_height = driver->getScreenSize().Height; // altura da tela


}


// M�TODO: createMenuPrincipal
// DESCRI��O: d� in�cio � cria��o da tela com o menu de escolha de fases
// ARGUMENTOS: nenhum
// RETORNO: void
void IntroGame::createIntroGame(){

	IntroGame receiver_options_introgame(this->driver, this->smgr, this->guienv, this->device, this->sound_engine);
	this->device->setEventReceiver(&receiver_options_introgame);

	

	// Limpa a cena e os elementos GUI
	this->smgr->clear();
	this->guienv->clear();

	//drawIntroFrames();
	
	// Desenha a cena criada
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(0,0,0,0));

		this->smgr->drawAll();
		this->guienv->drawAll();

		playIntroGameNarration();

		playGameIntro();

		//drawBackground();

		//drawSkipButton();

		//drawWindowButtons();

		this->driver->endScene();
	}

}


void IntroGame::playIntroGameNarration(){

	this->intro_music = sound_engine->play3D("system//sounds//intro.mp3", vec3df(0,0,0), false, false, true);
	this->intro_music->setIsLooped(false);

}


void IntroGame::playGameIntro(){

	MenuNovoJogo* menu_novo_jogo = new MenuNovoJogo(this->driver,
		                                             this->smgr,
										             this->guienv,
										             this->device,
										             this->sound_engine);

	// C�digo para pular a Intro do Jogo (apenas para facilitar os testes)
	this->sound_engine->stopAllSounds();
	menu_novo_jogo->createMenuNovoJogo();

	ITimer* mainTime = device->getTimer();
	mainTime->setTime(0);
	mainTime->start();
	u32 t;

	smgr->addCameraSceneNode(0, vector3df(0,30,-40), vector3df(0,5,0));
	scene::ISceneNode* bill = smgr->addBillboardSceneNode(0, core::dimension2d<f32>(100, 100));
	bill->setMaterialFlag(video::EMF_LIGHTING, false);
	bill->setPosition(vector3df(0.0f,0.0f,0.0f));
	bill->setScale(vector3df(0.1f,0.1f,0.1f));
	smgr->addCameraSceneNode(0, vector3df(0,30,-40), vector3df(0,5,0));
	scene::ICameraSceneNode* camera = device->getSceneManager()->getActiveCamera();

	fader = device->getGUIEnvironment()->addInOutFader();
	fader->fadeIn(500);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro//vd1.jpg"));
	
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		t = mainTime->getTime();
	    t = t / 1000; // TRANSFORM TO SECONDS
		if(t >= 5)break;
	}
	fader->fadeOut(500);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		if(teste)break;
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}
	fader->fadeIn(500);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro//vd2.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		t = mainTime->getTime();
	    t = t / 1000; // TRANSFORM TO SECONDS
		if(t >= 10)break;
		//if(teste)break;
	}
	fader->fadeOut(500);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		if(teste)break;
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}
		fader->fadeIn(500);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro//vd3.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		t = mainTime->getTime();
	    t = t / 1000; // TRANSFORM TO SECONDS
		if(t >= 15)break;
		//if(teste)break;
	}
	fader->fadeOut(500);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		if(teste)break;
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}
	fader->fadeIn(500);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro//vd4.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		t = mainTime->getTime();
	    t = t / 1000; // TRANSFORM TO SECONDS
		if(t >= 20)break;
		//if(teste)break;
	}
	fader->fadeOut(500);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		if(teste)break;
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}
	fader->fadeIn(500);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro//vd5.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		t = mainTime->getTime();
	    t = t / 1000; // TRANSFORM TO SECONDS
		if(t >= 30)break;
		//if(teste)break;
	}
	fader->fadeOut(500);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		if(teste)break;
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}
	fader->fadeIn(500);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro//vd6.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		t = mainTime->getTime();
	    t = t / 1000; // TRANSFORM TO SECONDS
		if(t >= 34)break;
		//if(teste)break;
	}
	fader->fadeOut(500);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		if(teste)break;
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}
	fader->fadeIn(500);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro//vd7.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		t = mainTime->getTime();
	    t = t / 1000; // TRANSFORM TO SECONDS
		if(t >= 37)break;
		//if(teste)break;
	}
	fader->fadeOut(500);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		if(teste)break;
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}
	fader->fadeIn(500);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro//vd8.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		t = mainTime->getTime();
	    t = t / 1000; // TRANSFORM TO SECONDS
		if(t >= 40)break;
		//if(teste)break;
	}
	fader->fadeOut(500);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		if(teste)break;
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}
	fader->fadeIn(500);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro//vd9.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		t = mainTime->getTime();
	    t = t / 1000; // TRANSFORM TO SECONDS
		if(t >= 46)break;
		//if(teste)break;
	}
	fader->fadeOut(500);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		if(teste)break;
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}
	fader->fadeIn(500);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro//vd10.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		t = mainTime->getTime();
	    t = t / 1000; // TRANSFORM TO SECONDS
		if(t >= 50)break;
		//if(teste)break;
	}
	fader->fadeOut(500);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		if(teste)break;
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}
	fader->fadeIn(500);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro//vd11.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		t = mainTime->getTime();
	    t = t / 1000; // TRANSFORM TO SECONDS
		if(t >= 56)break;
		//if(teste)break;
	}
	fader->fadeOut(200);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		if(teste)break;
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}
	fader->fadeIn(200);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro//vd12.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		t = mainTime->getTime();
	    t = t / 1000; // TRANSFORM TO SECONDS
		if(t >= 58)break;
		//if(teste)break;
	}
		fader->fadeOut(200);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		if(teste)break;
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}
	fader->fadeIn(200);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro//vd13.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		t = mainTime->getTime();
	    t = t / 1000; // TRANSFORM TO SECONDS
		if(t >= 60)break;
		//if(teste)break;
	}
			fader->fadeOut(200);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		if(teste)break;
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}
	fader->fadeIn(200);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro//vd14.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		t = mainTime->getTime();
	    t = t / 1000; // TRANSFORM TO SECONDS
		if(t >= 64)break;
		//if(teste)break;
	}
	fader->fadeOut(500);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		if(teste)break;
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}
	fader->fadeIn(500);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro//vd15.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		t = mainTime->getTime();
	    t = t / 1000; // TRANSFORM TO SECONDS
		if(intro_music->isFinished()){
			this->sound_engine->stopAllSounds();
			menu_novo_jogo->createMenuNovoJogo();
		};
		//if(teste)break;



	}
	
}



void IntroGame::drawBackground(){


	IGUIImage* bg_img = guienv->addImage(core::rect<s32>( (w_width/2) - (1024) , (w_height/2) - (1024), (w_width/2) + (1024) , (w_height/2) + (1024) ));
	bg_img->setScaleImage(false);
	bg_img->setUseAlphaChannel(true);
	bg_img->setImage( driver->getTexture("system//images//menus//bg__intro_game.png") );

	IGUIImage* img_rodape = guienv->addImage(core::rect<s32>(0, w_height - 64, w_width, w_height), 0);
	img_rodape->setScaleImage(false);
	img_rodape->setUseAlphaChannel(true);
	img_rodape->setImage( driver->getTexture("system//images//menus//menus_rodape.png") );

	/*IGUIImage* img_game_title = guienv->addImage(core::rect<s32>( (w_width / 2) - 256 , 10, (w_height / 2) + 512, 266), 0);
	img_game_title->setScaleImage(false);
	img_game_title->setUseAlphaChannel(true);
	img_game_title->setImage( driver->getTexture("system//images//menus//menu_principal//img_game_title.png"));*/


}




void IntroGame::drawSkipButton(){


	// Bot�o para iniciar novo jogo
	IGUIButton* btn_intro_corrida = guienv->addButton(rect<s32>( w_width - 138, w_height - 145, w_width - 10, w_height - 79), 0, GUI_ID_BUTTON_PULAR_INTRO, L"");
	btn_intro_corrida->setImage(driver->getTexture("system//images//menus//btn_pular_intro.png"));


}


void IntroGame::drawWindowButtons(){

	// Bot�o para minimizar a janela
	IGUIButton* btn_window_minimize = guienv->addButton(rect<s32>(w_width - 64, 0, w_width - 32, 32), 0, GUI_ID_BUTTON_IG_WINDOWS_MINIMIZE, L"");
	btn_window_minimize->setImage(driver->getTexture("system//images//menus//btn_window_minimize.png"));

	// Bot�o para fechar a janela
	IGUIButton* btn_window_close = guienv->addButton(rect<s32>(w_width - 32, 0, w_width, 32), 0, GUI_ID_BUTTON_IG_WINDOWS_CLOSE, L"");
	btn_window_close->setImage(driver->getTexture("system//images//menus//btn_window_close.png"));

}




// M�TODO: OnEvent - sobrecarga
// DESCRI��O: trata eventos ocorridos na tela (clique de mouse, teclas pressionadas, etc)
// ARGUMENTOS:
//     - const SEvent& event: struct contendo dados sobre o evento
// RETORNO: True se o evento foi processado
bool IntroGame::OnEvent(const SEvent& event){

	/*MenuNovoJogo* menu_novo_jogo = new MenuNovoJogo(this->driver,
		                                             this->smgr,
										             this->guienv,
										             this->device,
										             this->sound_engine);*/


	/*
		Neste metodo e descrito acoes que serao dadas caso haja um botao (que foi passado o ID) ira realizar
		Lembre-se que este ID e declarado no struct enum.
	*/
	if (event.EventType == EET_GUI_EVENT){

		s32 id = event.GUIEvent.Caller->getID();
		IGUIEnvironment* env = this->device->getGUIEnvironment();

		switch(event.GUIEvent.EventType){

			// Caso o tipo de evento seja clique em um bot�o da GUI
			case EGET_BUTTON_CLICKED:
				switch(id){

					case GUI_ID_BUTTON_PULAR_INTRO:
						
						return true;

				    case GUI_ID_BUTTON_IG_WINDOWS_MINIMIZE:
						this->device->minimizeWindow();
						return true;
					case GUI_ID_BUTTON_IG_WINDOWS_CLOSE:
						exit(1);
						return true;

					default:
						return false;

				}
				break;

			default:
				break;
		}
	}

	return false;
}




IntroGame::~IntroGame(void){
}
