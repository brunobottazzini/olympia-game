#ifndef __STAGE_H_INCLUDED__
#define __STAGE_H_INCLUDED__

#include <irrlicht.h>
#include <irrKlang.h>
#include "Main_Char.h"
#include "Menu_Game.h"
#include "Menu_Questao.h"
#include "GameInterface.h"
#include "NPC.h"
#include "irrClock.h"
//#include "irrVideoPlay.h"
#include "IA.h"
#include "Colisao.h"

using namespace irr;
using namespace core;
using namespace video;
using namespace io;
using namespace gui;
using namespace std;
using namespace irrklang;

#ifdef _IRR_WINDOWS_
#pragma comment(lib, "Irrlicht.lib")
#pragma comment(lib, "irrKlang.lib") // link with irrKlang.dll
#pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#endif

class Stage
{
    public:
	     Stage(s32 render, bool fullscreen, bool antialiasing, s32 resolution, bool vsync, bool shadows);
         bool MountMenu();
         

    private:
		 MyEventReceiver receiver;
		 short GameCorrida(short dificuldade);
		 SAppContext2 load_game_options(SAppContext2 context);
		 void save_game_options(SAppContext2 context);
		 ISceneNode* LoadingScreenIn();
		 void LoadingScreenOut(MyEventReceiver* receiver_s, ISceneNode* bill_second);
		 bool Campanha(short dificuldade);
		 void setLightning(bool enabled);
		 int rand_intRange(int a, int b);
		 short getPosition(Main_Char* player1, NPC *cpu1, NPC *cpu2, NPC *cpu3);
		 short getPositionNPC(NPC *cpu1, Main_Char* player1, NPC *cpu2, NPC *cpu3);
		 
		 IVideoDriver* driver;
		 ISceneManager* smgr;
		 IGUIEnvironment* guienv;
		 IrrlichtDevice *device;
		 ISound* intro_music;
		 ISound* torcida;
		 ISound* torcida_gritando;
		 ISound* passo_esquerdo;
		 ISound* passo_direito;
		 ISound* musica_corrida;
		 ISound* musica_derrota;
		 ISound* musica_vitoria;
		 ISoundEngine* sound_engine;
		 ISoundEngine* steps_engine;
		 int window_height;
		 int window_weight;
		 short ratio;
		 bool lapflag;
		 bool burningTest;
		 bool lightning; 

		 MenuEventReceiver_GAMEINTERFACE* game_interface;

		 
};

#endif