/*
Header para a classe que mostra a tela de op��es para salvar um novo jogo
�bner Zanetti
*/

#ifndef __MENUNOVOJOGO_H_INCLUDED__
#define __MENUNOVOJOGO_H_INCLUDED__

#include <irrlicht.h>
#include <irrKlang.h>
#include "MenuPersonagemSelecao.h"

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;
using namespace std;
using namespace irrklang;

#ifdef _IRR_WINDOWS_
#pragma comment(lib, "Irrlicht.lib")
#pragma comment(lib, "irrKlang.lib") // link with irrKlang.dll
#pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#endif

// Estrutura para salvar informa��es da campanha do jogador
typedef struct sJogo{

	int opened_levels[11];  // array que indica quais fases est�o  liberadas - 0 = n�o; 1 = sim
	int gender;             // 0 = masculino; 1 = feminino
	int selected_gender;    // 0 = masculino; 1 = feminino
	int char_texture_index; // 1-16 indicando qual textura dever� ser usada para o personagem

};


enum eBtnMenuNovoJogo
{
	//GUI_ID_BUTTON_PULAR_INTRO = 1,
	GUI_ID_BUTTON_MNJ_WINDOWS_CLOSE = 16,
	GUI_ID_BUTTON_MNJ_WINDOWS_MINIMIZE = 17,
	GUI_ID_BUTTON_MNJ_MAIN_MENU = 18

};




class MenuNovoJogo : public IEventReceiver{

    public:
	    MenuNovoJogo(IVideoDriver* driver, 
			      ISceneManager* smgr,
			      IGUIEnvironment* guienv, 
			      IrrlichtDevice *device,
				  ISoundEngine* sound_engine);

		void createMenuNovoJogo();
		
		virtual bool OnEvent(const SEvent& event);
        virtual ~MenuNovoJogo(void);
		

	private:
		IVideoDriver* driver;
		ISoundEngine* sound_engine;
		ISceneManager* smgr;
		IGUIEnvironment* guienv;
		IrrlichtDevice *device;
		IGUIInOutFader* intro_fader;
		struct sJogo* jogo;
        
		int w_width;
		int w_height;

		void playGameIntro();
		void drawBackground();
		void drawWindowButtons();
		void drawSkipButton();

};


#endif
