/*
Header para a classe que gerencia o menu principal
�bner Zanetti
*/

#ifndef __MENUPRINCIPAL_H_INCLUDED__
#define __MENUPRINCIPAL_H_INCLUDED__

#include <irrlicht.h>
#include <irrKlang.h>


using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;
using namespace irrklang;

#ifdef _IRR_WINDOWS_
#pragma comment(lib, "Irrlicht.lib")
#pragma comment(lib, "irrKlang.lib") // link with irrKlang.dll
#pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#endif



enum eBtnMenuPrincipal
{
	GUI_ID_BUTTON_NOVO_JOGO = 1,
	GUI_ID_BUTTON_CONTINUAR = 2,
	GUI_ID_BUTTON_OPCOES = 3,
	GUI_ID_BUTTON_MP_WINDOWS_CLOSE = 16,
	GUI_ID_BUTTON_MP_WINDOWS_MINIMIZE = 17,
	GUI_ID_BUTTON_MP_MAIN_MENU = 18

};




class MenuPrincipal : public IEventReceiver{

    public:
	    MenuPrincipal(IVideoDriver* driver, 
			      ISceneManager* smgr,
			      IGUIEnvironment* guienv, 
			      IrrlichtDevice *device,
				  ISoundEngine* sound_engine);

		void createMenuPrincipal();
		
		virtual bool OnEvent(const SEvent& event);
        virtual ~MenuPrincipal(void);
		

	private:
		IVideoDriver* driver;
		ISoundEngine* sound_engine;
		ISceneManager* smgr;
		IGUIEnvironment* guienv;
		IrrlichtDevice *device;
        
		int w_width;
		int w_height;

		void drawBackground();
		void drawWindowButtons();
		void drawMenuButtons();

};


#endif
