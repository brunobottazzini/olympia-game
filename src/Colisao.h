#ifndef __COLISAO_H_INCLUDED__
#define __COLISAO_H_INCLUDED__

#define BORDA_DIREITA  100
#define BORDA_MEIO     190
#define BORDA_ESQUERDA 283
#define LINHA_CURVA    7164.6592

class Colisao{

	public:
		bool colisao( float z, float x);
		bool fezCurva;
	
};

#endif
