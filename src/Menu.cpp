/*
Classe que ira nos preparar subjanelas e acoes para serem tomada ao decorrer do jogo.
Muito Importante para customizacao e otimizacao de desempenho do jogo.


Bruno Lara Bottazzini
*/
#include "Menu.h"

MenuEventReceiver::MenuEventReceiver(SAppContext & context) : Context(context), selected(3), antialiasing(false), 
														shadow(0), synvertical(1), resolution(0),fullscreen(false) {
	/*
		Construtor da Classe de Menu repare que os valores iniciais estao sendo preparados.
	*/
	Context.renderOption = selected;
	Context.fullscreen = fullscreen;
	Context.antialiasing = antialiasing;
	Context.resolution = resolution;
	Context.shadow = shadow;
	Context.synvertical = synvertical;
	IGUIEnvironment* env = Context.device->getGUIEnvironment();
	this->window = env->addWindow(
						rect<s32>(100 + 30, 0 + 30, 600 + 30, 400 + 30),
						false, // modal?
						L"Op��es Avan�adas de V�deo");
	this->window->setVisible(false);
	this->window->setEnabled(false);
	this->window->getCloseButton()->setVisible(false);
	this->color_setted = false;
	//this->window->

}

bool MenuEventReceiver::OnEvent(const SEvent& event)
{
	/*
		Neste metodo e descrito acoes que serao dadas caso haja um botao (que foi passado o ID) ira realizar
		Lembre-se que este ID e declarado no struct enum.
	*/
	if (event.EventType == EET_GUI_EVENT)
	{
		s32 id = event.GUIEvent.Caller->getID();
		IGUIEnvironment* env = Context.device->getGUIEnvironment();

		switch(event.GUIEvent.EventType)
		{
			case EGET_SCROLL_BAR_CHANGED:
				if (id == GUI_ID_TRANSPARENCY_SCROLL_BAR)
				{
					s32 pos = ((IGUIScrollBar*)event.GUIEvent.Caller)->getPos();
				
					for (u32 i=0; i<EGDC_COUNT ; ++i)
					{
						SColor col = env->getSkin()->getColor((EGUI_DEFAULT_COLOR)i);
						col.setAlpha(pos);
						env->getSkin()->setColor((EGUI_DEFAULT_COLOR)i, col);
					}
				
				}
				break;

			case EGET_LISTBOX_CHANGED:
				Context.renderOption = Context.renderBox->getSelected();
				Context.resolution = Context.resolutionBox->getSelected();
				break;

			case EGET_LISTBOX_SELECTED_AGAIN:
				Context.renderOption = Context.renderBox->getSelected();
				Context.resolution = Context.resolutionBox->getSelected();
				break;

			case EGET_CHECKBOX_CHANGED:
				Context.fullscreen = ((gui::IGUICheckBox*)Context.full_check)->isChecked();
				Context.full_check->setChecked(Context.fullscreen);

				Context.antialiasing = ((gui::IGUICheckBox*)Context.anti_check)->isChecked();
				Context.anti_check->setChecked(Context.antialiasing);

				Context.shadow = ((gui::IGUICheckBox*)Context.shadow_check)->isChecked();
				Context.shadow_check->setChecked(Context.shadow);

				Context.synvertical = ((gui::IGUICheckBox*)Context.sync_check)->isChecked();
				Context.sync_check->setChecked(Context.synvertical);

				break;

			case EGET_BUTTON_CLICKED:
				switch(id)
				{
				case GUI_ID_START:
					if(Context.startButton == false){
						Context.startButton = true;
					}else{
						Context.startButton = false;
					}
					return true;
				case GUI_ID_QUIT_BUTTON:
					Context.device->closeDevice();
					return true;
					

				case GUI_ID_WINDOW_OPTIONS: 
					{

					this->window->setEnabled(true);
					this->window->setVisible(true);

					env->addStaticText(L"Selecione o tipo de Renderiza��o",rect<s32>(60,60,200,120),
						false, // border?
						true, // wordwrap?
						this->window);
					this->window->setDrawBackground(true);
					if(this->color_setted == false){
							this->color_setted = true;
							SColor col;
							col.set(95,157,63,53);
							env->getSkin()->setColor((EGDC_ACTIVE_BORDER), col);
							env->getSkin()->setColor((EGDC_HIGH_LIGHT), col);
							
					
						//env->getSkin()->setColor(EGUI_LBC_TEXT_HIGHLIGHT,col);
						Context.renderBox = env->addListBox(core::rect<int>(10,100,220,180), this->window, EGET_LISTBOX_CHANGED);					
						//SColor c1 = Context.renderBox->getItemOverrideColor(0,EGUI_LBC_TEXT_HIGHLIGHT);
						//c1.set(95,157,63,53);
						//Context.renderBox->setItemOverrideColor(1,EGUI_LBC_TEXT_HIGHLIGHT,c1);
						Context.renderBox->addItem(L"OpenGL");
						Context.renderBox->addItem(L"DirectX 8.1");
						Context.renderBox->addItem(L"DirectX 9.0c");
						Context.renderBox->addItem(L"Burning's Video 0.44");
						Context.renderBox->setSelected(Context.renderOption);

						env->addStaticText(L"Selecione a Resolu��o",rect<s32>(60,200,200,215),
							false, // border?
							true, // wordwrap?
							this->window);
						Context.resolutionBox = env->addListBox(core::rect<int>(10,240,220,320), this->window, EGET_LISTBOX_CHANGED);
						Context.resolutionBox->addItem(L"800x600 4:3");
						Context.resolutionBox->addItem(L"1024x768 4:3");
						Context.resolutionBox->addItem(L"1280x1024 4:3");
						Context.resolutionBox->addItem(L"1280x720 16:9");
						Context.resolutionBox->addItem(L"1280x800 16:10");
						Context.resolutionBox->addItem(L"1440x900 16:10");
						Context.resolutionBox->setSelected(Context.resolution);

						env->addButton(rect<s32>(350, 340,460, 372), this->window, GUI_ID_OK_BUTTON_OPTIONS,
						L"OK", L"Salvar as op��es colocadas");


						Context.full_check = env->addCheckBox(Context.fullscreen, core::rect<int>(350,60,350+110,75),
							this->window, EGET_CHECKBOX_CHANGED, L"Tela Cheia");
						Context.anti_check = env->addCheckBox(antialiasing, core::rect<int>(350,90,350+110,105),
							this->window, EGET_CHECKBOX_CHANGED, L"Antialiasing x2");
						Context.sync_check = env->addCheckBox(fullscreen, core::rect<int>(350,120,350+110,135),
							this->window, EGET_CHECKBOX_CHANGED, L"Sync Vertical");
						Context.shadow_check = env->addCheckBox(shadow, core::rect<int>(350,150,350+110,165),
							this->window, EGET_CHECKBOX_CHANGED, L"Sombras");

						Context.full_check->setChecked(Context.fullscreen);
						Context.anti_check->setChecked(Context.antialiasing);
						Context.sync_check->setChecked(Context.synvertical);
						Context.shadow_check->setChecked(Context.shadow);
					}

					return true;
					}

				case GUI_ID_OK_BUTTON_OPTIONS:
					//this->window->remove();
					this->window->setVisible(false);
					this->window->setEnabled(true);
					return true;

				case GUI_ID_FILE_OPEN_BUTTON:
					//Context.listbox->addItem(L"File open");
					env->addFileOpenDialog(L"Please choose a file.");
					return true;

				default:
					return false;
				}
				break;

			default:
				break;
		}
	}

	return false;
}