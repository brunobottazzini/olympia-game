#ifndef __NPC_H_INCLUDED__
#define __NPC_H_INCLUDED__

#include <irrlicht.h>

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;
using namespace std;

#ifdef _IRR_WINDOWS_
#pragma comment(lib, "Irrlicht.lib")
#pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#endif

class NPC
{
    public:
	     NPC(IrrlichtDevice *device, IVideoDriver* driver, char caminhoModelo[], char caminhoTextura[], bool lightning);
   		 void setPosition(float posX, float posY, float posZ);
		 void setPosition(vector3df npcPos);
		 void setAnimationMD2(irr::scene::EMD2_ANIMATION_TYPE anim);
		 void setAnimationMD2(char path[]);
		 void rotateNPC(float x, float y, float z);
		 void setLapFlag(short flag);
		 short getLapFlag();
		 void setMesh(char mesh[]);
		 void setModelSpeed(float speed);
		 IAnimatedMesh* getMesh();
		 IAnimatedMeshSceneNode* getNode();
		 vector3df getPosition();
    private:
		 IAnimatedMeshSceneNode* node;
		 IAnimatedMesh* mesh;
		 short LapFlag;
		 char* caminhotexture;
		 bool lightning;
		 float speed;
		 ISceneManager* smgr;
		 IVideoDriver* driver;

};

#endif