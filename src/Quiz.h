/*
Model of the Class to create the quiz system
�bner Zanetti
*/

#ifndef __QUIZ_H_INCLUDED__
#define __QUIZ_H_INCLUDED__

#include <irrlicht.h>
#include <irrKlang.h>
#include <string>

#include <time.h>


#ifndef __MENUNOVOJOGO_H_INCLUDED__
    #include "MenuFases.h"
#endif
#ifndef __MENUNOVOJOGO_H_INCLUDED__
    #include "MenuNovoJogo.h"
#endif

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;
using namespace std;
using namespace irrklang;

#ifdef _IRR_WINDOWS_
#pragma comment(lib, "Irrlicht.lib")
#pragma comment(lib, "irrKlang.lib") // link with irrKlang.dll
#pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#endif


#define QTD_SELECIONADAS 3

struct sQuiz{

	int how_many_questions;     // Quantas questoes o quiz possui
	int right_answers_to_pass;  // Quantidade m�nima de respostas que o jogador deve acertar
	int right_answers;          // Quantidade de respostas que o jogador acertou
	int actual_page;            // Informa a pagina atual do quiz
	int already_selected[QTD_SELECIONADAS];   // Armazena as perguntas j� sorteadas

};



struct sQuestion{

	char* image_path_question;  // Caminho da imagem da pergunta
	char* image_path_answer;    // Caminho da imagem da resposta
	int   right_answer;         // A resposta certa
	int   given_answer;         // A resposta dada

};


enum btnQuiz
{
	GUI_ID_BUTTON_QUIZ_A = 10,
	GUI_ID_BUTTON_QUIZ_B = 11,
	GUI_ID_BUTTON_QUIZ_C = 12,
	GUI_ID_BUTTON_QUIZ_D = 13,
	GUI_ID_BUTTON_FORWARD = 14,
	GUI_ID_BUTTON_RESULTS = 15,
	GUI_ID_BUTTON_GO_TO_LEVEL = 19,
	GUI_ID_BUTTON_WINDOWS_CLOSE = 16,
	GUI_ID_BUTTON_WINDOWS_MINIMIZE = 17,
	GUI_ID_BUTTON_QZ_MAIN_MENU = 18

};


enum letrasQuestoes{ A = 10, B = 11, C = 12, D = 13 };

enum efeitosSonoros{
	QUIZ_LOOP = 1, QUIZ_ACERTO = 2, QUIZ_ERRO = 3, QUIZ_VITORIA = 4, QUIZ_DERROTA = 5 
};


class MenuEventReceiver_QUIZ : public IEventReceiver{
	
	public:
		MenuEventReceiver_QUIZ(IVideoDriver* driver, 
			                   ISceneManager* smgr,
			                   IGUIEnvironment* guienv, 
			                   IrrlichtDevice *device,
							   ISoundEngine* sound_engine,
							   sQuiz* s_quiz,
							   sQuestion* s_question,
							   struct sJogo* jogo);

		virtual bool OnEvent(const SEvent& event);
		void createQuiz(sQuiz* s_quiz, int hmq, int ra, sQuestion* s_question);

	private:
		IVideoDriver* driver;
		ISoundEngine* sound_engine;
		ISound* intro_music;
		ISceneManager* smgr;
		IGUIEnvironment* guienv;
		IrrlichtDevice *device;
		ITexture* img;
		sQuiz* s_quiz;
		sQuestion* s_question;
		struct sJogo* jogo;
		
		void  updateQuiz(sQuiz* s_quiz, sQuestion* s_question);

		void  selectQuestion(sQuiz* s_quiz, sQuestion* s_question);

		void  drawQuestionScreen(sQuiz* s_quiz, sQuestion* s_question);
		void  drawAnswerScreen(sQuiz* s_quiz, sQuestion* s_question);
		void  drawResultScreen(sQuiz* s_quiz);

		void  drawPagination(sQuiz* s_quiz, sQuestion* s_question);
		void  drawAnswersButtons(sQuestion* s_question);
		void  drawForwardButton(sQuiz* s_quiz);
		void  drawGoToLevelScreenButton();
		void  drawWindowButtons();

		void  playSoundEffect(int what_sound, bool repeat);

        void  setQuizHowManyQuestions(sQuiz* s_quiz, int questions);
		int   getQuizHowManyQuestions(sQuiz* s_quiz);

		void  setQuizRightAnswersToPass(sQuiz* s_quiz, int ra);
		int   getQuizRightAnswersToPass(sQuiz* s_quiz);

		void  setQuizRightAnswers(sQuiz* s_quiz);
		int   getQuizRightAnswers(sQuiz* s_quiz);

		void  setQuizActualPage(sQuiz* s_quiz, int page);
        int   getQuizActualPage(sQuiz* s_quiz);

		void  setQuizAlreadySelected(sQuiz* s_quiz, int selected, int actual_page);
		int   isQuizAlreadySelected(sQuiz* s_quiz, int selected, int actual_page);

		void  setQuestionImagePath(sQuestion* s_question, char* image_path);
		char* getQuestionImagePath(sQuestion* s_question);

		void  setQuestionImagePathAnswer(sQuestion* s_question, char* image_path);
		char* getQuestionImagePathAnswer(sQuestion* s_question);

		void  setQuestionRightAnswer(sQuestion* s_question, int ra);
		int   getQuestionRightAnswer(sQuestion* s_question);

		void  setQuestionGivenAnswer(sQuestion* s_question, int given_answer);
		int   getQuestionGivenAnswer(sQuestion* s_question);

		int w_width;
		int w_height;

};


#endif