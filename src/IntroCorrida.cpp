#include "IntroCorrida.h"
#include "MenuFases.h"

IntroCorrida::IntroCorrida(IVideoDriver* driver,
			         ISceneManager* smgr,
			         IGUIEnvironment* guienv,
			         IrrlichtDevice *device,
				     ISoundEngine* sound_engine,
					 struct sJogo* jogo){

	this->driver = driver;
	this->smgr = smgr;
	this->guienv = guienv;
	this->device = device;
	this->sound_engine = sound_engine;
	this->jogo = jogo;
	this->selected_gender = jogo->selected_gender;
	
	this->w_width = driver->getScreenSize().Width;  // largura da tela
	this->w_height = driver->getScreenSize().Height; // altura da tela

}


// M�TODO: createMenuPrincipal
// DESCRI��O: d� in�cio � cria��o da tela com o menu de escolha de fases
// ARGUMENTOS: nenhum
// RETORNO: void
void IntroCorrida::createIntroCorrida(){

	IntroCorrida receiver_options_introcorrida(this->driver, this->smgr, this->guienv, this->device, this->sound_engine, this->jogo);
	this->device->setEventReceiver(&receiver_options_introcorrida);

	// indica que est� fase j� foi liberada
	this->jogo->opened_levels[0] = 2; // j� viu a intro
	this->jogo->opened_levels[1] = 1; // liberou a fase do jogo de corrida
	this->jogo->opened_levels[2] = 0;
	this->jogo->opened_levels[3] = 0;
	this->jogo->opened_levels[4] = 0;
	this->jogo->opened_levels[5] = 0;
	this->jogo->opened_levels[6] = 0;
	this->jogo->opened_levels[7] = 0;
	this->jogo->opened_levels[8] = 0;
	this->jogo->opened_levels[9] = 0;
	this->jogo->opened_levels[10] = 0;
	this->jogo->opened_levels[11] = 0;

	// Limpa a cena e os elementos GUI
	this->smgr->clear();
	this->guienv->clear();

	//drawBackground();

	//drawSkipButton();

	//drawWindowButtons();

    playIntroCorridaNarration();

	playIntroCorrida();

	

	// Desenha a cena criada
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(0,0,0,0));
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}

}



// M�TODO: playGameIntro
// DESCRI��O: inicia a anima��o de introdu��o do game
// ARGUMENTOS: nenhum
// RETORNO: void
void IntroCorrida::playIntroCorrida(){


	MenuFases* menu_fases = new MenuFases(this->driver,
		                                  this->smgr,
										  this->guienv,
										  this->device,
										  this->sound_engine,
										  jogo);

	// C�digo para pular a Intro Corrida (apenas para facilitar os testes)
	this->sound_engine->stopAllSounds();
    menu_fases->createMenuFases();

	drawSkipButton();

	scene::ISceneNode* bill = smgr->addBillboardSceneNode(0, core::dimension2d<f32>(100, 100));
	bill->setMaterialFlag(video::EMF_LIGHTING, false);
	bill->setPosition(vector3df(0.0f,0.0f,0.0f));
	bill->setScale(vector3df(0.1f,0.1f,0.1f));
	smgr->addCameraSceneNode(0, vector3df(0,30,-40), vector3df(0,5,0));
	scene::ICameraSceneNode* camera = device->getSceneManager()->getActiveCamera();

	fader = device->getGUIEnvironment()->addInOutFader();

	fader->fadeIn(3000);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro_corrida//corrida_000.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		if(teste)break;
	}
	fader->fadeOut(37000);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		if(teste)break;
	}

	fader->fadeIn(3000);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro_corrida//corrida_001.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		if(teste)break;
	}
	fader->fadeOut(18000);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		if(teste)break;
	}
	
	fader->fadeIn(3000);
	bill->setMaterialTexture(0, driver->getTexture("system//images//vd//intro_corrida//corrida_002.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		if(teste)break;
	}
	fader->fadeOut(31000);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		if(teste)break;
	}
	bill->setMaterialTexture(0,driver->getTexture("system//images//vd//intro_corrida//corrida_003.jpg"));
	
	fader->fadeIn(3000);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		if(teste)break;
	}
	fader->fadeOut(17000);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		if(teste)break;
	}

	bill->setMaterialTexture(0,driver->getTexture("system//images//vd//intro_corrida//corrida_004.jpg"));
	
	fader->fadeIn(3000);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		if(teste)break;
	}
	fader->fadeOut(76000);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		if(teste)break;

	}

    bill->setMaterialTexture(0,driver->getTexture("system//images//vd//intro_corrida//corrida_005.jpg"));
	
	fader->fadeIn(5000);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		if(teste)break;
	}
	fader->fadeOut(3000);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		if(teste){

		    this->sound_engine->stopAllSounds();
            menu_fases->createMenuFases();

		}

	}



}




void IntroCorrida::playIntroCorridaNarration(){

	this->intro_music = sound_engine->play3D("system//sounds//NARRACAO_CORRIDA1.ogg", vec3df(0,0,0), false, false, true);
	this->intro_music->setIsLooped(false);

}



void IntroCorrida::drawBackground(){


	IGUIImage* bg_img = guienv->addImage(core::rect<s32>(0 ,0, w_width, w_height), 0);
	bg_img->setScaleImage(true);
	bg_img->setImage( driver->getTexture("system//images//menus//bg_menus.jpg") );

	IGUIImage* img_rodape = guienv->addImage(core::rect<s32>(0, w_height - 64, w_width, w_height), 0);
	img_rodape->setScaleImage(false);
	img_rodape->setUseAlphaChannel(true);
	img_rodape->setImage( driver->getTexture("system//images//menus//menus_rodape.png") );

	/*IGUIImage* img_game_title = guienv->addImage(core::rect<s32>( (w_width / 2) - 256 , 10, (w_height / 2) + 512, 266), 0);
	img_game_title->setScaleImage(false);
	img_game_title->setUseAlphaChannel(true);
	img_game_title->setImage( driver->getTexture("system//images//menus//menu_principal//img_game_title.png"));*/


}



void IntroCorrida::drawSkipButton(){

	IGUIStaticText* txt_paginacao = this->guienv->addStaticText(L"", core::rect<s32>(15,300,200,350), false, false, 0);
	txt_paginacao->setOverrideColor(video::SColor(0xff597d3d));
	txt_paginacao->setTextAlignment(EGUIA_CENTER, EGUIA_UPPERLEFT);
	txt_paginacao->setText( L"" );
	stringw str = L"Genero ";
	str += this->selected_gender;
	txt_paginacao->setText( str.c_str() );

	// Bot�o para iniciar novo jogo
	IGUIButton* btn_intro_corrida = guienv->addButton(rect<s32>( w_width - 138, w_height - 145, w_width - 10, w_height - 79), 0, GUI_ID_BUTTON_IC_PULAR_INTRO, L"");
	btn_intro_corrida->setImage(driver->getTexture("system//images//menus//btn_pular_intro.png"));


}


void IntroCorrida::drawWindowButtons(){

	// Bot�o para minimizar a janela
	IGUIButton* btn_window_minimize = guienv->addButton(rect<s32>(w_width - 64, 0, w_width - 32, 32), 0, GUI_ID_BUTTON_IC_WINDOWS_MINIMIZE, L"");
	btn_window_minimize->setImage(driver->getTexture("system//images//menus//btn_window_minimize.png"));

	// Bot�o para fechar a janela
	IGUIButton* btn_window_close = guienv->addButton(rect<s32>(w_width - 32, 0, w_width, 32), 0, GUI_ID_BUTTON_IC_WINDOWS_CLOSE, L"");
	btn_window_close->setImage(driver->getTexture("system//images//menus//btn_window_close.png"));

}




// M�TODO: OnEvent - sobrecarga
// DESCRI��O: trata eventos ocorridos na tela (clique de mouse, teclas pressionadas, etc)
// ARGUMENTOS:
//     - const SEvent& event: struct contendo dados sobre o evento
// RETORNO: True se o evento foi processado
bool IntroCorrida::OnEvent(const SEvent& event){

	/*MenuPrincipal* menu_principal = new MenuPrincipal(this->driver,
		                                             this->smgr,
										             this->guienv,
										             this->device,
										             this->sound_engine);*/


	/*MenuFases* menu_fases = new MenuFases(this->driver,
		                                  this->smgr,
										  this->guienv,
										  this->device,
										  this->sound_engine,
										  this->jogo);*/


	/*
		Neste metodo e descrito acoes que serao dadas caso haja um botao (que foi passado o ID) ira realizar
		Lembre-se que este ID e declarado no struct enum.
	*/
	if (event.EventType == EET_GUI_EVENT){

		s32 id = event.GUIEvent.Caller->getID();
		IGUIEnvironment* env = this->device->getGUIEnvironment();

		switch(event.GUIEvent.EventType){

			// Caso o tipo de evento seja clique em um bot�o da GUI
			case EGET_BUTTON_CLICKED:
				switch(id){

					case GUI_ID_BUTTON_IC_PULAR_INTRO:
						
						return true;

				    case GUI_ID_BUTTON_IC_WINDOWS_MINIMIZE:
						this->device->minimizeWindow();
						return true;
					case GUI_ID_BUTTON_IC_WINDOWS_CLOSE:
						exit(1);
						return true;

					default:
						return false;

				}
				break;

			default:
				break;
		}
	}

	return false;
}




IntroCorrida::~IntroCorrida(void){
}
