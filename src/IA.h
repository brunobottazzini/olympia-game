#ifndef __IA_H_INCLUDED__
#define __IA_H_INCLUDED__

#include <irrlicht.h>

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;
using namespace std;

#ifdef _IRR_WINDOWS_
#pragma comment(lib, "Irrlicht.lib")
#pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#endif

class IA{
	public:
		IA(short dificuldade);
		void setDificuldade(short dificuldade);
		void refreshExternalVariables(float velocidade_npc, float velocidade_player,vector3df posicao_npc,vector3df posicao_player, int colocacao_npc, int colocacao_player);
		float getVelocidadeMin();
		float getAceleracao();
		bool shouldIrunFaster();
		void setThinking(short number);
		void resetStamina();
		float getCurrentSpeed();

	private:
		bool isNPCBehindPL();
		bool isNPCSpeedGood();
		bool isNPCFarAwayFromPL();
		bool isPLSpeedTooGood();
		bool isMyPlaceGood();
		bool isNPCMaxSpeed();
		bool isMyStaminaGood();
		float rand_FloatRange(float a, float b);
		short dificuldade;
		short thinking;
		float velocidade_npc;
		float velocidade_player;
		float velocidade_max;
		float forca_max;
		float resistencia;
		float velocidade_min;
		int colocacao_npc;
		int stamina;
		int iteracao;
		int colocacao_player;
		vector3df posicao_npc;
		vector3df posicao_npc2;
		vector3df posicao_npc3;
		vector3df posicao_player;
};

#endif
