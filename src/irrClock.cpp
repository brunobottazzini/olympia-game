#include "irrClock.h"


irrClock::irrClock(IrrlichtDevice * device){
	/*
	Construtor da Classe que ira marcar o tempo.
	*/
	this->mainTime = device->getTimer();
	this->device = device;
	this->milesegundos = 0;
	this->milesegundos_t = 0;
	this->segundos_t = 0;
	this->minutos_t = 0;
	this->mainTime->setTime(0);
}

void irrClock::startTime(){
	/*
	Inicia a contagem do tempo
	*/
	this->mainTime->start();
}

void irrClock::stopTime(){
	/*
	Para a contagem do tempo
	*/
	this->mainTime->stop();
}

void irrClock::setTime(u32 segundos){
	/*
	Seta o tempo que ira iniciar a contagem em segundos
	*/
	this->mainTime->setTime(segundos * 1000);
}

void irrClock::clearTime(){
	/*
	Limpa o cache do tempo.
	*/
	this->mainTime->setTime(0);
}

wchar_t* irrClock::Count(){
	/*
	Inicializa uma contagem em segundos retornando em
	wchar_t
	*/
	u32 t = this->mainTime->getTime();
	t = t / 1000; // TRANSFORM TO SECONDS
	wchar_t buffer[255];
	swprintf(buffer,255,L"   %i",t);
	return buffer;
}

wchar_t* irrClock::getClockInString(){
	/*
		Volta o tempo do cronometro do jogo
	(Marcacao de tempo ate em milesimos de segundos)
	*/
	this->milesegundos = this->mainTime->getTime();
	this->milesegundos_t = this->milesegundos % 1000;
	this->segundos_t = (this->milesegundos/1000) % 60;
	this->minutos_t = ((this->milesegundos/1000)/60) % 60;
	wchar_t buffer[100];
	swprintf(buffer,100,L"%i:%i %i",this->minutos_t,this->segundos_t, this->milesegundos_t);
	return buffer;
}
