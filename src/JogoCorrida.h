/*
Header para a classe do jogo de corrida
�bner Zanetti
*/

#ifndef __JOGOCORRIDA_H_INCLUDED__
#define __JOGOCORRIDA_H_INCLUDED__

#include <irrlicht.h>
#include <irrKlang.h>
#include "Main_Char.h"
#include "Menu_Game.h"
#include "Menu_Questao.h"
#include "GameInterface.h"
#include "NPC.h"
#include "irrClock.h"
//#include "irrVideoPlay.h"
#include "IA.h"
#include "Colisao.h"

#include "MenuNovoJogo.h"


using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;
using namespace std;
using namespace irrklang;

#ifdef _IRR_WINDOWS_
#pragma comment(lib, "Irrlicht.lib")
#pragma comment(lib, "irrKlang.lib") // link with irrKlang.dll
#pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#endif



enum eBtnJogoCorrida
{
	GUI_ID_BUTTON_JC_MENU_FASES = 1,
	GUI_ID_BUTTON_JC_WINDOWS_CLOSE = 16,
	GUI_ID_BUTTON_JC_WINDOWS_MINIMIZE = 17,
	GUI_ID_BUTTON_JC_MAIN_MENU = 18

};




class JogoCorrida : public IEventReceiver{

    public:
	    JogoCorrida(IVideoDriver* driver, 
			      ISceneManager* smgr,
			      IGUIEnvironment* guienv, 
			      IrrlichtDevice *device,
				  ISoundEngine* sound_engine,
				  struct sJogo* jogo);

		void createJogoCorrida(short dificuldade);
		short GameCorrida(short dificuldade);
		ISceneNode* LoadingScreenIn();
		
		virtual bool OnEvent(const SEvent& event);
        virtual ~JogoCorrida(void);
		

	private:
		IVideoDriver* driver;
		ISoundEngine* sound_engine;
		ISound* intro_music;
		ISceneManager* smgr;
		IGUIEnvironment* guienv;
		IrrlichtDevice* device;
		IGUIInOutFader* intro_fader;
		struct sJogo* jogo;

		int w_width;
		int w_height;

		int selected_gender;

		void playIntroCorrida();
		void playIntroCorridaNarration();
		void drawBackground();
		void drawWindowButtons();
		void drawSkipButton();


		 MyEventReceiver receiver;
		 
		 //SAppContext2 load_game_options(SAppContext2 context);
		 //void save_game_options(SAppContext2 context);
		 
		 void LoadingScreenOut(MyEventReceiver* receiver_s, ISceneNode* bill_second);
		 bool Campanha(short dificuldade);
		 void setLightning(bool enabled);
		 int rand_intRange(int a, int b);
		 short getPosition(Main_Char* player1, NPC *cpu1, NPC *cpu2, NPC *cpu3);
		 short getPositionNPC(NPC *cpu1, Main_Char* player1, NPC *cpu2, NPC *cpu3);
		 
		 ISound* torcida;
		 ISound* torcida_gritando;
		 ISound* passo_esquerdo;
		 ISound* passo_direito;
		 ISound* musica_corrida;
		 ISound* musica_derrota;
		 ISound* musica_vitoria;
		 //ISoundEngine* steps_engine;
		 int window_height;
		 int window_weight;
		 short ratio;
		 bool lapflag;
		 bool burningTest;
		 bool lightning; 

		 MenuEventReceiver_GAMEINTERFACE* game_interface;





};


#endif
