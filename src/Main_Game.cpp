/*
Arquivo de Cricao de Menu Inicial para Escolhas de Opcoes de Videos na otimizacao
de performance do jogo.

Bruno Bottazzini
*/
#include "Main_Game.h"

Main_Game::Main_Game()
{
	/*
		Neste Metodo e montado a nossa janela inicial onde ira ser feito um menu com 
		botoes.
	*/
	irr::SIrrlichtCreationParameters params;

    video::E_DRIVER_TYPE driverType;
    //driverType = video::EDT_OPENGL;
    //driverType = video::EDT_SOFTWARE;
	params.DriverType = driverType = video::EDT_BURNINGSVIDEO;
    // Keyboard handling initiating
    MyEventReceiver receiver2;// Initiating objec keyboard (Look at the keyboard.h)
	params.EventReceiver = &receiver2;
	params.WindowSize=core::dimension2d<u32>( 640, 480 );
	params.Doublebuffer = true;
	params.Bits=32;
    params.Fullscreen=false;
    params.AntiAlias=false;
    params.Stencilbuffer=false;
    params.Vsync=false;
	//ISoundEngine* sound_engine = createIrrKlangDevice();
	//ISound* music = engine->play3D("system/music/animiti.mp3",
	//                               vec3df(0,0,0), true, false, true);

	
	IrrlichtDevice *device = createDeviceEx(params);
    //createDevice( driverType, dimension2d<u32>(640, 480),32,false,false,false, &receiver2);
    if (!device)
      exit(1);

	device->setWindowCaption(L"Olympia");

	IVideoDriver* driver = device->getVideoDriver();
	ISceneManager* smgr = device->getSceneManager();
	IGUIEnvironment* guienv = device->getGUIEnvironment();
	SColor col_back;
	col_back.setAlpha(255);
	guienv->getSkin()->setColor((EGDC_3D_FACE), col_back);
	guienv->getSkin()->setColor((EGDC_3D_SHADOW), col_back);


    bool still_running = false;
	IGUIEnvironment* env = device->getGUIEnvironment();
	IGUIInOutFader* fader = env->addInOutFader();
	fader->fadeIn(1000);
	IGUISkin* skin = env->getSkin();
	//IGUIFont* font = env->getFont("system//font//Galatia.bmp");
	IGUIFont* font = env->getFont("system//font//Galatia//Galatia.xml");
	if (font)
	skin->setFont(font);

	skin->setFont(env->getBuiltInFont(), EGDF_TOOLTIP);

	env->addButton(rect<s32>(10,360,110,360 + 32), 0, GUI_ID_START,
		L"Jogar", L"Inicia o Jogo !!! :]");
	env->addButton(rect<s32>(470,440,470+160,440 + 32), 0, GUI_ID_WINDOW_OPTIONS,
		L"Configura��es Avan�adas", L"Op��es para Otimiza��o do jogo");
	//env->addButton(rect<s32>(10,440,110,440 + 32), 0, GUI_ID_QUIT_BUTTON,
		//L"Sair", L"Sai para o Sistema Operacional");

	SAppContext context;	   
	context.device = device;
	context.startButton = false;

	MenuEventReceiver receiver_menu(context);
	context = this->load_options(context);
	device->setEventReceiver(&receiver_menu);
	double grau = 0;
	bool once = false;

	scene::ISceneNode* bill = smgr->addBillboardSceneNode(0, core::dimension2d<f32>(100, 90));
	bill->setMaterialFlag(video::EMF_LIGHTING, false);
	bill->setPosition(vector3df(0.0f,0.0f,0.0f));
	bill->setScale(vector3df(0.1f,0.1f,0.1f));
	bill->setMaterialTexture(0, driver->getTexture("system//images//Logo.bmp"));

	//IGUIImage* image = guienv->addImage(driver->getTexture("system/images/Robo_3D.png"),core::position2d<s32>(5,5));
	//image->setImage("
	smgr->addCameraSceneNode(0, vector3df(0,30,-40), vector3df(0,5,0));
	scene::ICameraSceneNode*camera = device->getSceneManager()->getActiveCamera();
	while(device->run())
	{

		driver->beginScene(true, true, SColor(255,100,101,140));
		if(context.startButton == true){
				if(once == false){
					smgr->drawAll();
					guienv->drawAll();
   					driver->endScene();
					device->closeDevice();
					device->run(); // Muito importante rodar isto para garantir que o proximo device ira ser executado
					device->drop();
					this->save_options(context);
					this->newGame(context.renderOption, context.fullscreen, context.antialiasing, context.resolution, context.synvertical, context.shadow);
					once = true;
				}
			}else{
				core::vector3df cameraPos = camera->getAbsolutePosition();
				grau += 1;
				if(grau >= 360.0){
				grau = 0;
				}
		   }
		   smgr->drawAll();
		   guienv->drawAll();

   		  driver->endScene();

	}

	this->device->drop();

}

void Main_Game::save_options(SAppContext context){
	FILE * to_write_in = fopen("data//video.sys","w");
	char buffer[] = {context.renderOption, context.fullscreen,context.antialiasing,
						context.resolution,context.synvertical, context.shadow};
	fwrite(&buffer, 1,sizeof(buffer),to_write_in);
	fclose(to_write_in);
}

SAppContext Main_Game::load_options(SAppContext context){
	FILE * load;
	load = fopen("data//video.sys","r");
	char buffer[] = {context.renderOption, context.fullscreen,context.antialiasing,
						context.resolution,context.synvertical, context.shadow};
	if(load == NULL){
		//
		FILE * to_write_in = fopen("data//video.sys","w");
		fwrite(&buffer, 1,sizeof(buffer),to_write_in);
		fclose(to_write_in);
	}else{
		while((fread(&buffer,sizeof(buffer),1,load)) == 1){
			context.renderOption = buffer[0];
			context.fullscreen = buffer[1];
			context.antialiasing = buffer[2];
			context.resolution = buffer[3];
			context.synvertical = buffer[4];
			context.shadow = buffer[5];
		}
		fclose(load);
	}
	return context;
}

void Main_Game::newGame(s32 render, bool fullscreen, bool antialiasing, s32 resolution, bool vsync, bool shadows){
	/*
	Neste metodo nos iniciamos o Jogo em Si, perceba que e passado as opcoes que veio do menu burning
	no parametro da funcao.
	*/
	Stage* inicio_menu = new Stage(render,fullscreen,antialiasing,resolution,vsync,shadows);
}

int main()
{
/*
Funcao para dar pontape inicial no jogo instanciando
o objeto da classe que ira renderizar uma jenala em burning video onde o usuario
poderia iniciar o jogo e escolher preferencias de video.
*/
  Main_Game *Jogo = new Main_Game();
}