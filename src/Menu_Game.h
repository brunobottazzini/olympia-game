/*
Model of the Class to get Menu
Bruno Lara Bottazzini
*/
#ifndef __MENU_GAME_H_INCLUDED__
#define __MENU_GAME_H_INCLUDED__

#include <irrlicht.h>

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

#ifdef _IRR_WINDOWS_
#pragma comment(lib, "Irrlicht.lib")
#endif

struct SAppContext2
{
	IrrlichtDevice *device;
	IGUIListBox* dificuldadeBox;
	IGUIListBox* somBox;
	IGUICheckBox* som_check;
	IGUICheckBox* musica_check;
	ISoundEngine* sound_engine;
	s32 nivelDificuldade;
	bool som;
	bool musica;
	bool iniciar_campanha;
	bool iniciar_corrida;
	bool okay_button_clicked;
};

enum
{
	GUI_ID_QUIT_BUTTON_GAME = 101,
	GUI_ID_WINDOW_OPTIONS_MAIN_GAME,
	GUI_ID_WINDOW_OPTIONS_START_RUNNING = 141,
	GUI_ID_WINDOW_OPTIONS_CHOOSE_CONTEST = 140,
	GUI_ID_TRANSPARENCY_SCROLL_BAR_MAIN_GAME,
	GUI_ID_QUIT_BUTTON_MAIN_GAME = 101,
	GUI_ID_FILE_OPEN_BUTTON_MAIN_GAME,
	GUI_ID_START_CAMPANHA,
	GUI_ID_SHOW_QUIZ,
	GUI_ID_OK_BUTTON_OPTIONS_TRAINNING,
	GUI_ID_OK_BUTTON_OPTIONS_MAIN_GAME,
};

class MenuEventReceiver_MAINGAME : public IEventReceiver
{
public:
	MenuEventReceiver_MAINGAME(SAppContext2 & context);
	virtual bool OnEvent(const SEvent& event);
private:
	SAppContext2 & Context;
	s32 selectedDificuldade;
	bool musica;
	bool som;
	bool iniciar_corrida;
	bool set_color_training;
	bool set_color_level;
	IGUIWindow* window;
	IGUIWindow* window_choose_game;

};


#endif