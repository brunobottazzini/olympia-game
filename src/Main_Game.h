#ifndef __MAIN_GAME_H_INCLUDED__
#define __MAIN_GAME_H_INCLUDED__

#include <irrlicht.h>
#include <irrKlang.h>

#include "Menu.h"
#include "Stage.h"


 
using namespace irr;

//using namespace irrklang;


using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;
using namespace std;
using namespace irrklang;

#ifdef _IRR_WINDOWS_
#pragma comment(lib, "Irrlicht.lib")
#pragma comment(lib, "irrKlang.lib") // link with irrKlang.dll
#pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#endif

class Main_Game
{
    public:
	     Main_Game();
    private:
		 void PlayIntro();
		 SAppContext load_options(SAppContext context);
		 void save_options(SAppContext context);
		 void newGame(s32 render, bool fullscreen, bool antialiasing, s32 resolution, bool vsync, bool shadows);
		 MyEventReceiver receiver;// Initiating objec keyboard (Look at the keyboard.h)

		 IrrlichtDevice* device;

};

#endif