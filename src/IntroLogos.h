/*
Header para a classe que mostra a intro do jogo de corrida
�bner Zanetti
*/

#ifndef __INTROLOGOS_H_INCLUDED__
#define __INTROLOGOS_H_INCLUDED__

#include <irrlicht.h>
#include "keyboard.h"
#include <irrKlang.h>

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;
using namespace std;
using namespace irrklang;

#ifdef _IRR_WINDOWS_
#pragma comment(lib, "Irrlicht.lib")
#pragma comment(lib, "irrKlang.lib") // link with irrKlang.dll
#pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#endif



enum eBtnIntroLogos
{
	GUI_ID_BUTTON_IL_PULAR_INTRO = 1,
	GUI_ID_BUTTON_IL_WINDOWS_CLOSE = 16,
	GUI_ID_BUTTON_IL_WINDOWS_MINIMIZE = 17,
	GUI_ID_BUTTON_IL_MAIN_MENU = 18

};




class IntroLogos : public IEventReceiver{

    public:
	    IntroLogos(IVideoDriver* driver, 
			      ISceneManager* smgr,
			      IGUIEnvironment* guienv, 
			      IrrlichtDevice *device,
				  ISoundEngine* sound_engine,
				  MyEventReceiver* receiver
				  );

		void createIntroLogos();
		
		virtual bool OnEvent(const SEvent& event);
        virtual ~IntroLogos(void);
		

	private:
		IVideoDriver* driver;
		ISoundEngine* sound_engine;
		ISound* intro_music;
		ISceneManager* smgr;
		IGUIEnvironment* guienv;
		IrrlichtDevice* device;
		IGUIInOutFader* fader;
		MyEventReceiver* receiver_logos;
        
		int w_width;
		int w_height;

		void playIntroLogos();

		void drawBackground();
		void drawWindowButtons();
		void drawSkipButton();

};


#endif
