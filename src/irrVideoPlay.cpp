/*
Este arquivo e descrito como e a funcionalidade da Class que ira tocar video no
renderer do Irrlicht.

Ele faz uma conversao de pixeis de imagens do tipo IplImage pra Textura tambem e
necessario realizar uma refinacao de controle de fps para auxiliar a sincronia com 
o som.

Bruno Bottazzini
*/
#include "irrVideoPlay.h"

irrVideoPlay::irrVideoPlay(){
	/*
	Construtor da classe no qual inicia o fps em 25 para que na haja nenhum bug caso
	o programador esqueca de avisar a classe quantos fps devera ser tocado o video
	como 25 e o valor mais padrao entao foi resolvido deixar como default.
	Lembre-se sempre que e necessario setar o fps de acordo com que o video foi convertido
	*/
	this->fps = 25;
}

void irrVideoPlay::setVideoPath(char path[]){
	/*
		Aponta para qual arquivo o OpenCV ira iniciar a captura de video
	*/
	this->capture = cvCreateFileCapture(path);

}

void irrVideoPlay::setFPS(int fps){
	/*
		Inicia a quantidade de FPS que sera feito o video
		A soma de 12 e a constante nao qual o computador pode levar para realizar
		o swap de imagens isso e contavem e depende de cada computador.
		Deve ser testado em varios antes.
	*/
	this->fps = fps + 12;
}

void irrVideoPlay::playVideoInBurning(ISceneManager* smgr, IVideoDriver* driver){
		/*
		Metodo no qual realiza controle de play/fps para ser tocado o video.
		Este metodo e usado apenas para burning ja que ele tem que realizar
		swap no hd por nao consegui converter as texturas ja que ele diminui as
		resolucoes da mesma e acaba perdendo informacoes. Assim a apontar se perde
		crashando o jogo.
		Para solucao de contorno foi criado o sawp no hd
		*/
        IplImage  *frame;
		this->fps = this->fps + 100;
		smgr->clear();
        char buffer[29] = "system//temp//cachevideo.jpg";
        scene::ISceneNode* bill = smgr->addBillboardSceneNode(0, core::dimension2d<f32>(100, 100));
		smgr->addCameraSceneNode(0, vector3df(0,30,-40), vector3df(0,5,0));
        bill->setMaterialFlag(video::EMF_LIGHTING, false);
        bill->setPosition(vector3df(0.0f,0.0f,0.0f));
        bill->setScale(vector3df(0.1f,0.1f,0.1f));
        while(true){
                driver->beginScene(true, true, SColor(255,100,101,140));
                Sleep(1000/this->fps);;
                        frame = cvQueryFrame(capture);
                        if(frame){
                                cvSaveImage(buffer,frame);
                                int c = cvWaitKey( 1000 / this->fps);
                                bill->setMaterialTexture(0, driver->getTexture(buffer));
                                remove(buffer);
                        }else{
						 smgr->clear();
						 smgr->addCameraSceneNode(0,vector3df(0,30,-40), vector3df(0,5,0));
						 driver->endScene();
                         break;
                }
                        smgr->drawAll();
                        driver->endScene();
						driver->removeAllTextures(); // Memory Lick
        }
}

void irrVideoPlay::playVideo(ISceneManager* smgr, IVideoDriver* driver){
	/*
	Metodo no qual realiza controle de play/fps para ser tocado o video.
	*/
	IplImage  *frame;
	smgr->clear();
	scene::ISceneNode* bill = smgr->addBillboardSceneNode(0, core::dimension2d<f32>(100, 100));
	smgr->addCameraSceneNode(0, vector3df(0,30,-40), vector3df(0,5,0));
	bill->setMaterialFlag(video::EMF_LIGHTING, false);
	bill->setPosition(vector3df(0.0f,0.0f,0.0f));
	bill->setScale(vector3df(0.1f,0.1f,0.1f));
	int frameH    = (int) cvGetCaptureProperty(this->capture, CV_CAP_PROP_FRAME_HEIGHT);
    int frameW    = (int) cvGetCaptureProperty(this->capture, CV_CAP_PROP_FRAME_WIDTH);
	int maxFrame = (int) cvGetCaptureProperty(this->capture, CV_CAP_PROP_FRAME_COUNT);
	ITexture *image_buffer;
	while(true){
		driver->beginScene(true, true, SColor(255,100,101,140));
		Sleep(1000/this->fps);
			frame = cvQueryFrame(this->capture);
			if(frame){
				image_buffer = this->create_ITexture_from_CvCapture(frame,frameH,frameW,driver);
				int c = cvWaitKey( 1000 / this->fps);
				bill->setMaterialTexture(1, image_buffer);
				smgr->drawAll();
				driver->endScene();
			}else{
			driver->endScene();
			smgr->clear();
			smgr->addCameraSceneNode(0,vector3df(0,30,-40), vector3df(0,5,0));
			cvReleaseCapture(&this->capture);
			 break;
		}
		driver->removeTexture(image_buffer);
	}
}

ITexture* irrVideoPlay::create_ITexture_from_CvCapture(IplImage* img, int frameH, int frameW, IVideoDriver* driver)
{
	/*
	Metodo privado no qual realizar conversao de 1 frame passado por do tipo IplImage para ITexture.
	Isso faz com que retorne uma textura retirada de 1 frame de um video.
	*/
   char unique_tex_name[50];
   ITexture* cacheTexture = driver->addTexture(core::dimension2d<u32>(frameW, frameH), unique_tex_name, video::ECF_A1R5G5B5);     
   u8* pixels = (u8*)(cacheTexture->lock());
   u8* ardata = (u8*)img->imageData;
   int max_pixels = frameW * frameH;
   for(int i=0;i<max_pixels;i++)
   {
      *pixels = *ardata;
      pixels++; ardata++;
      *pixels = *ardata;
      pixels++; ardata++;
      *pixels = *ardata;
      pixels++; ardata++;
   
      pixels++;
   }
   cacheTexture->unlock();
   return cacheTexture;
} 