#ifndef __INTRO_H_INCLUDED__
#define __INTRO_H_INCLUDED__

#include <irrlicht.h>
#include "keyboard.h"
#include <irrKlang.h>

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;
using namespace std;
using namespace irrklang;

#ifdef _IRR_WINDOWS_
#pragma comment(lib, "Irrlicht.lib")
#pragma comment(lib, "irrKlang.lib") // link with irrKlang.dll
#pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#endif

class Intro
{
    public:
	     Intro(IVideoDriver* driver,ISceneManager* smgr, IGUIEnvironment* guienv, IrrlichtDevice *device, MyEventReceiver* receiver);
		 void playLogoIntro();
		 void playIntroGame1();
		 void setRatio(int ratio);
		 
    private:
		 IVideoDriver* driver;
		 ISoundEngine* sound_engine;
		 ISceneManager* smgr;
		 IGUIEnvironment* guienv;
		 IrrlichtDevice *device;
		 IGUIInOutFader* fader;
		 MyEventReceiver* receiver3;
		 int ratio;
};

#endif