#include "MenuFases.h"

#include "MenuPrincipal.h"
#include "IntroCorrida.h"
#include "Quiz.h"
#include "JogoCorrida.h"
#include "JogoDardo.h"

MenuFases::MenuFases(IVideoDriver* driver, 
			         ISceneManager* smgr,
			         IGUIEnvironment* guienv, 
			         IrrlichtDevice *device,
				     ISoundEngine* sound_engine,
					 struct sJogo* jogo){

	this->driver = driver;
	this->smgr = smgr;
	this->guienv = guienv;
	this->device = device;
	this->sound_engine = sound_engine;
	this->jogo = jogo;
	this->selected_gender = jogo->selected_gender;

	this->w_width = driver->getScreenSize().Width;  // largura da tela
	this->w_height = driver->getScreenSize().Height; // altura da tela
	
}


// M�TODO: createMenuFases
// DESCRI��O: d� in�cio � cria��o da tela com o menu de escolha de fases
// ARGUMENTOS: nenhum
// RETORNO: void
void MenuFases::createMenuFases(){

	MenuFases receiver_options_menufases(this->driver, this->smgr, this->guienv, this->device, this->sound_engine, this->jogo);
	this->device->setEventReceiver(&receiver_options_menufases);

	
	
	this->intro_music = sound_engine->play3D("system//music//intro.wav", vec3df(0,0,0), false, false, true);
	this->intro_music->setIsLooped(false);

	// Limpa a cena e os elementos GUI
	this->smgr->clear();
	this->guienv->clear();

	drawBackground();
	drawLevelsButtons();

	drawWindowButtons();

	// Desenha a cena criada
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(0,0,0,0));
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}

}



void MenuFases::drawBackground(){


	IGUIImage* bg_img = guienv->addImage(core::rect<s32>(0 ,0, w_width, w_height), 0);
	bg_img->setScaleImage(true);
	bg_img->setImage( driver->getTexture("system//images//menus//bg_menus.jpg") );

	IGUIImage* img_rodape = guienv->addImage(core::rect<s32>(0, w_height - 64, w_width, w_height), 0);
	img_rodape->setScaleImage(false);
	img_rodape->setUseAlphaChannel(true);
	img_rodape->setImage( driver->getTexture("system//images//menus//menus_rodape.png") );

	IGUIImage* bg_title = guienv->addImage(core::rect<s32>( (w_width / 2) - 256 , 50, (w_height / 2) + 512, 105), 0);
	bg_title->setScaleImage(false);
	bg_title->setUseAlphaChannel(true);
	bg_title->setImage( driver->getTexture("system//images//menus//menu_fases//title_selecao_fases.png"));

	IGUIImage* bg_game_title = guienv->addImage(core::rect<s32>(0 ,0, 256, 256), 0);
	bg_game_title->setScaleImage(false);
	bg_game_title->setUseAlphaChannel(true);
	bg_game_title->setImage( driver->getTexture("system//images//menus//btn_game_olympia.png") );

	/*IGUIStaticText* txt_paginacao = this->guienv->addStaticText(L"", core::rect<s32>(0,300,500,500), false, false, 0);
	txt_paginacao->setOverrideColor(video::SColor(0xff597d3d));
	txt_paginacao->setTextAlignment(EGUIA_CENTER, EGUIA_UPPERLEFT);
	txt_paginacao->setText( L"" );
	stringw str = L"";
	str += this->selected_gender;
	txt_paginacao->setText( str.c_str() );*/


}




void MenuFases::drawLevelsButtons(){

	ITexture* btn_textura;

	// Bot�o relativo a fase Intro Corrida
	IGUIButton* btn_intro_corrida = guienv->addButton(rect<s32>( (w_width / 2) - 222, 120, (w_width / 2) - 94, 248), 0, GUI_ID_BUTTON_FASE_1, L"");
	switch( this->jogo->opened_levels[0] ){
		case 0: 
		    btn_textura = driver->getTexture("system//images//menus//menu_fases//off//btn_corrida_intro.png");
			btn_intro_corrida->setEnabled(false);
	    break;

		case 1: 
			btn_textura = driver->getTexture("system//images//menus//menu_fases//on//btn_corrida_intro.png");
			btn_intro_corrida->setEnabled(true);
		break;

		case 2: 
			btn_textura = driver->getTexture("system//images//menus//menu_fases//done//btn_corrida_intro.png"); 
			btn_intro_corrida->setEnabled(true);
		break;
	}
    btn_intro_corrida->setImage(btn_textura);
		

	// Bot�o relativo a fase Jogo Corrida
	IGUIButton* btn_jogo_corrida = guienv->addButton(rect<s32>( (w_width / 2) - 64, 120, (w_width / 2) + 64, 248), 0, GUI_ID_BUTTON_FASE_2, L"");
	switch( this->jogo->opened_levels[1] ){
		case 0: 
		    btn_textura = driver->getTexture("system//images//menus//menu_fases//off//btn_corrida_jogo.png");
			btn_jogo_corrida->setEnabled(false);
	    break;

		case 1: 
			btn_textura = driver->getTexture("system//images//menus//menu_fases//on//btn_corrida_jogo.png");
			btn_jogo_corrida->setEnabled(true);
		break;

		case 2: 
			btn_textura = driver->getTexture("system//images//menus//menu_fases//done//btn_corrida_jogo.png"); 
			btn_jogo_corrida->setEnabled(true);
		break;
	}
    btn_jogo_corrida->setImage(btn_textura);

	// Bot�o relativo a fase Perguntas Corrida
	IGUIButton* btn_perguntas_corrida = guienv->addButton(rect<s32>( (w_width / 2) + 94, 120, (w_width / 2) + 222, 248), 0, GUI_ID_BUTTON_FASE_3, L"");
	switch( this->jogo->opened_levels[2] ){
		case 0: 
		    btn_textura = driver->getTexture("system//images//menus//menu_fases//off//btn_corrida_perguntas.png");
			btn_perguntas_corrida->setEnabled(false);
	    break;

		case 1: 
			btn_textura = driver->getTexture("system//images//menus//menu_fases//on//btn_corrida_perguntas.png");
			btn_perguntas_corrida->setEnabled(true);
		break;

		case 2: 
			btn_textura = driver->getTexture("system//images//menus//menu_fases//done//btn_corrida_perguntas.png"); 
			btn_perguntas_corrida->setEnabled(true);
		break;
	}
    btn_perguntas_corrida->setImage(btn_textura);

	// Bot�o relativo a fase Intro Dardo
	IGUIButton* btn_intro_dardo = guienv->addButton(rect<s32>( (w_width / 2) - 222, 268, (w_width / 2) - 94, 396), 0, GUI_ID_BUTTON_FASE_4, L"");
	switch( this->jogo->opened_levels[3] ){
		case 0: 
		    btn_textura = driver->getTexture("system//images//menus//menu_fases//off//btn_dardo_intro.png");
			btn_intro_dardo->setEnabled(false);
	    break;

		case 1: 
			btn_textura = driver->getTexture("system//images//menus//menu_fases//on//btn_dardo_intro.png");
			btn_intro_dardo->setEnabled(true);
		break;

		case 2: 
			btn_textura = driver->getTexture("system//images//menus//menu_fases//done//btn_dardo_intro.png"); 
			btn_intro_dardo->setEnabled(true);
		break;
	}
    btn_intro_dardo->setImage(btn_textura);

	// Bot�o relativo a fase Jogo Dardo
	IGUIButton* btn_jogo_dardo = guienv->addButton(rect<s32>( (w_width / 2) - 64, 268, (w_width / 2) + 64, 396), 0, GUI_ID_BUTTON_FASE_5, L"");
	switch( this->jogo->opened_levels[4] ){
		case 0: 
		    btn_textura = driver->getTexture("system//images//menus//menu_fases//off//btn_dardo_jogo.png");
			btn_jogo_dardo->setEnabled(false);
	    break;

		case 1: 
			btn_textura = driver->getTexture("system//images//menus//menu_fases//on//btn_dardo_jogo.png");
			btn_jogo_dardo->setEnabled(true);
		break;

		case 2: 
			btn_textura = driver->getTexture("system//images//menus//menu_fases//done//btn_dardo_jogo.png"); 
			btn_jogo_dardo->setEnabled(true);
		break;
	}
    btn_jogo_dardo->setImage(btn_textura);

	// Bot�o relativo a fase Perguntas Dardo
	IGUIButton* btn_perguntas_dardo = guienv->addButton(rect<s32>( (w_width / 2) + 94, 268, (w_width / 2) + 222, 396), 0, GUI_ID_BUTTON_FASE_6, L"");
	switch( this->jogo->opened_levels[5] ){
		case 0: 
		    btn_textura = driver->getTexture("system//images//menus//menu_fases//off//btn_dardo_perguntas.png");
			btn_perguntas_dardo->setEnabled(false);
	    break;

		case 1: 
			btn_textura = driver->getTexture("system//images//menus//menu_fases//on//btn_dardo_perguntas.png");
			btn_perguntas_dardo->setEnabled(true);
		break;

		case 2: 
			btn_textura = driver->getTexture("system//images//menus//menu_fases//done//btn_dardo_perguntas.png"); 
			btn_perguntas_dardo->setEnabled(true);
		break;
	}
    btn_perguntas_dardo->setImage(btn_textura);


	// Bot�o relativo a fase Intro Salto
	IGUIButton* btn_intro_salto = guienv->addButton(rect<s32>( (w_width / 2) - 222, 416, (w_width / 2) - 94, 544), 0, GUI_ID_BUTTON_FASE_7, L"");
	switch( this->jogo->opened_levels[6] ){
		case 0: 
		    btn_textura = driver->getTexture("system//images//menus//menu_fases//off//btn_salto_intro.png");
			btn_intro_salto->setEnabled(false);
	    break;

		case 1: 
			btn_textura = driver->getTexture("system//images//menus//menu_fases//on//btn_salto_intro.png");
			btn_intro_salto->setEnabled(true);
		break;

		case 2: 
			btn_textura = driver->getTexture("system//images//menus//menu_fases//done//btn_salto_intro.png"); 
			btn_intro_salto->setEnabled(true);
		break;
	}
    btn_intro_salto->setImage(btn_textura);

	// Bot�o relativo a fase Jogo Salto
	IGUIButton* btn_jogo_salto = guienv->addButton(rect<s32>( (w_width / 2) - 64, 416, (w_width / 2) + 64, 544), 0, GUI_ID_BUTTON_FASE_8, L"");
	switch( this->jogo->opened_levels[7] ){
		case 0: 
		    btn_textura = driver->getTexture("system//images//menus//menu_fases//off//btn_salto_jogo.png");
			btn_jogo_salto->setEnabled(false);
	    break;

		case 1: 
			btn_textura = driver->getTexture("system//images//menus//menu_fases//on//btn_salto_jogo.png");
			btn_jogo_salto->setEnabled(true);
		break;

		case 2: 
			btn_textura = driver->getTexture("system//images//menus//menu_fases//done//btn_salto_jogo.png"); 
			btn_jogo_salto->setEnabled(true);
		break;
	}
    btn_jogo_salto->setImage(btn_textura);

	// Bot�o relativo a fase Perguntas Salto
	IGUIButton* btn_perguntas_salto = guienv->addButton(rect<s32>( (w_width / 2) + 94, 416, (w_width / 2) + 222, 544), 0, GUI_ID_BUTTON_FASE_9, L"");
	switch( this->jogo->opened_levels[8] ){
		case 0: 
		    btn_textura = driver->getTexture("system//images//menus//menu_fases//off//btn_salto_perguntas.png");
			btn_perguntas_salto->setEnabled(false);
	    break;

		case 1: 
			btn_textura = driver->getTexture("system//images//menus//menu_fases//on//btn_salto_perguntas.png");
			btn_perguntas_salto->setEnabled(true);
		break;

		case 2: 
			btn_textura = driver->getTexture("system//images//menus//menu_fases//done//btn_salto_perguntas.png"); 
			btn_perguntas_salto->setEnabled(true);
		break;
	}
    btn_perguntas_salto->setImage(btn_textura);


	// Bot�o relativo a fase Intro Disco
	IGUIButton* btn_intro_disco = guienv->addButton(rect<s32>( (w_width / 2) - 222, 564, (w_width / 2) - 94, 692), 0, GUI_ID_BUTTON_FASE_10, L"");
	switch( this->jogo->opened_levels[9] ){
		case 0: 
		    btn_textura = driver->getTexture("system//images//menus//menu_fases//off//btn_disco_intro.png");
			btn_intro_disco->setEnabled(false);
	    break;

		case 1: 
			btn_textura = driver->getTexture("system//images//menus//menu_fases//on//btn_disco_intro.png");
			btn_intro_disco->setEnabled(true);
		break;

		case 2: 
			btn_textura = driver->getTexture("system//images//menus//menu_fases//done//btn_disco_intro.png"); 
			btn_intro_disco->setEnabled(true);
		break;
	}
    btn_intro_disco->setImage(btn_textura);

	// Bot�o relativo a fase Jogo Disco
	IGUIButton* btn_jogo_disco = guienv->addButton(rect<s32>( (w_width / 2) - 64, 564, (w_width / 2) + 64, 692), 0, GUI_ID_BUTTON_FASE_11, L"");
	switch( this->jogo->opened_levels[10] ){
		case 0: 
		    btn_textura = driver->getTexture("system//images//menus//menu_fases//off//btn_disco_jogo.png");
			btn_jogo_disco->setEnabled(false);
	    break;

		case 1: 
			btn_textura = driver->getTexture("system//images//menus//menu_fases//on//btn_disco_jogo.png");
			btn_jogo_disco->setEnabled(true);
		break;

		case 2: 
			btn_textura = driver->getTexture("system//images//menus//menu_fases//done//btn_disco_jogo.png"); 
			btn_jogo_disco->setEnabled(true);
		break;
	}
    btn_jogo_disco->setImage(btn_textura);

	// Bot�o relativo a fase Perguntas Disco
	IGUIButton* btn_perguntas_disco = guienv->addButton(rect<s32>( (w_width / 2) + 94, 564, (w_width / 2) + 222, 692), 0, GUI_ID_BUTTON_FASE_12, L"");
	switch( this->jogo->opened_levels[11] ){
		case 0: 
		    btn_textura = driver->getTexture("system//images//menus//menu_fases//off//btn_disco_perguntas.png");
			btn_perguntas_disco->setEnabled(false);
	    break;

		case 1: 
			btn_textura = driver->getTexture("system//images//menus//menu_fases//on//btn_disco_perguntas.png");
			btn_perguntas_disco->setEnabled(true);
		break;

		case 2: 
			btn_textura = driver->getTexture("system//images//menus//menu_fases//done//btn_disco_perguntas.png"); 
			btn_perguntas_disco->setEnabled(true);
		break;
	}
    btn_perguntas_disco->setImage(btn_textura);

}


void MenuFases::drawWindowButtons(){


	// Bot�o para voltar ao menu principal
	IGUIButton* btn_menu_principal = guienv->addButton(rect<s32>(10, w_height - 138, 138, w_height - 74), 0, GUI_ID_BUTTON_FASE_MAIN_MENU, L"");
	btn_menu_principal->setImage(driver->getTexture("system//images//menus//btn_voltar_menu_principal.png"));

	// Bot�o para minimizar a janela
	IGUIButton* btn_window_minimize = guienv->addButton(rect<s32>(w_width - 64, 0, w_width - 32, 32), 0, GUI_ID_BUTTON_FASE_WINDOWS_MINIMIZE, L"");
	btn_window_minimize->setImage(driver->getTexture("system//images//menus//btn_window_minimize.png"));

	// Bot�o para fechar a janela
	IGUIButton* btn_window_close = guienv->addButton(rect<s32>(w_width - 32, 0, w_width, 32), 0, GUI_ID_BUTTON_FASE_WINDOWS_CLOSE, L"");
	btn_window_close->setImage(driver->getTexture("system//images//menus//btn_window_close.png"));


}




// M�TODO: OnEvent - sobrecarga
// DESCRI��O: trata eventos ocorridos na tela (clique de mouse, teclas pressionadas, etc)
// ARGUMENTOS:
//     - const SEvent& event: struct contendo dados sobre o evento
// RETORNO: True se o evento foi processado
bool MenuFases::OnEvent(const SEvent& event){

	MenuPrincipal* menu_principal = new MenuPrincipal(this->driver, 
		                                              this->smgr,
		                                              this->guienv, 
										              this->device,
										              this->sound_engine);

	IntroCorrida* intro_corrida = new IntroCorrida(this->driver, 
		                                  this->smgr,
										  this->guienv, 
										  this->device,
										  this->sound_engine,
										  this->jogo);

	sQuiz s_quiz;
	sQuestion s_question;
	MenuEventReceiver_QUIZ* quiz = new MenuEventReceiver_QUIZ(this->driver, 
		                                                      this->smgr,
		                                                      this->guienv, 
															  this->device,
															  this->sound_engine,
															  &s_quiz,
															  &s_question,
															  this->jogo);


	JogoCorrida* jogo_corrida = new JogoCorrida(this->driver, 
		                                  this->smgr,
										  this->guienv, 
										  this->device,
										  this->sound_engine,
										  this->jogo);

	JogoDardo* jogo_dardo = new JogoDardo(this->driver, 
		                                  this->smgr,
										  this->guienv, 
										  this->device,
										  this->sound_engine,
										  this->jogo);

	/*
		Neste metodo e descrito acoes que serao dadas caso haja um botao (que foi passado o ID) ira realizar
		Lembre-se que este ID e declarado no struct enum.
	*/
	if (event.EventType == EET_GUI_EVENT){

		s32 id = event.GUIEvent.Caller->getID();
		IGUIEnvironment* env = this->device->getGUIEnvironment();

		switch(event.GUIEvent.EventType){
			
			// Caso o tipo de evento seja clique em um bot�o da GUI
			case EGET_BUTTON_CLICKED:
				switch(id){

					// Bot�o da fase Intro Corrida
				    case GUI_ID_BUTTON_FASE_1:
							this->sound_engine->stopAllSounds();
							intro_corrida->createIntroCorrida();
							return true;
                    
					// Bot�o da fase Jogo Corrida
				    case GUI_ID_BUTTON_FASE_2:
							this->sound_engine->stopAllSounds();
							jogo_corrida->createJogoCorrida(0);
							return true;

					// Bot�o da fase Perguntas Corrida
				    case GUI_ID_BUTTON_FASE_3:
							this->sound_engine->stopAllSounds();
							quiz->createQuiz(&s_quiz, 3, 2, &s_question);							
							//jogo_corrida->LoadingScreenIn();
							return true;

				    case GUI_ID_BUTTON_FASE_6:
							//this->sound_engine->stopAllSounds();
							//jogo_dardo->createJogoDardo(0);
							//jogo_corrida->LoadingScreenIn();
							return true;

							

					case GUI_ID_BUTTON_FASE_MAIN_MENU:
							menu_principal->createMenuPrincipal();
							return true;


				    case GUI_ID_BUTTON_FASE_WINDOWS_MINIMIZE:
						this->device->minimizeWindow();
						return true;
					case GUI_ID_BUTTON_FASE_WINDOWS_CLOSE:
						exit(1);
						return true;

					default:
						return false;

				}
				break;

			default:
				break;
		}
	}

	return false;
}




MenuFases::~MenuFases(void){
}
