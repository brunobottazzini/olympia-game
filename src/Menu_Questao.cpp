/*
Classe que ira nos preparar subjanelas e acoes para serem tomada ao decorrer do jogo.
Muito Importante para customizacao do jogo.


Bruno Lara Bottazzini
*/
#include "Menu_Questao.h"


MenuEventReceiver_QUESTIONS::MenuEventReceiver_QUESTIONS(SAppContext3 & context) : Context(context){
	/*
		Construtor da Classe de Menu repare que os valores iniciais estao sendo preparados.
	*/
	context.chosen_options = 0;
}

bool MenuEventReceiver_QUESTIONS::OnEvent(const SEvent& event)
{

	/*
		Neste metodo e descrito acoes que serao dadas caso haja um botao (que foi passado o ID) ira realizar
		Lembre-se que este ID e declarado no struct enum.
	*/
	if (event.EventType == EET_GUI_EVENT)
	{
		s32 id = event.GUIEvent.Caller->getID();
		IGUIEnvironment* env = Context.device->getGUIEnvironment();

		switch(event.GUIEvent.EventType)
		{
			case EGET_SCROLL_BAR_CHANGED:
				break;

			case EGET_LISTBOX_CHANGED:
				break;

			case EGET_LISTBOX_SELECTED_AGAIN:
				break;

			case EGET_CHECKBOX_CHANGED:
				break;


			/*case EGET_BUTTON_CLICKED:
				switch(id)
				{
				case GUI_ID_BUTTON_QUIZ_A:
					this->Context.chosen_options = 1;
					return true;
                case GUI_ID_BUTTON_QUIZ_B:
					this->Context.chosen_options = 2;
					return true;
				case GUI_ID_BUTTON_QUIZ_C:
					this->Context.chosen_options = 3;
					return true;
				case GUI_ID_BUTTON_QUIZ_D:
					this->Context.chosen_options = 4;
					return true;
				default:
					return false;
				}
				break;*/

			default:
				break;
		}
	}

	return false;
}