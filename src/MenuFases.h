/*
Header para a classe que gerencia o menu de sele��o de fases
�bner Zanetti
*/

#ifndef __MENUFASES_H_INCLUDED__
#define __MENUFASES_H_INCLUDED__

#include <irrlicht.h>
#include <irrKlang.h>

#include "MenuNovoJogo.h"


using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;
using namespace irrklang;

#ifdef _IRR_WINDOWS_
#pragma comment(lib, "Irrlicht.lib")
#pragma comment(lib, "irrKlang.lib") // link with irrKlang.dll
#pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#endif



enum eBtnMenuFases
{
	GUI_ID_BUTTON_FASE_1 = 1,
	GUI_ID_BUTTON_FASE_2 = 2,
	GUI_ID_BUTTON_FASE_3 = 3,
	GUI_ID_BUTTON_FASE_4 = 4,
	GUI_ID_BUTTON_FASE_5 = 5,
	GUI_ID_BUTTON_FASE_6 = 6,
	GUI_ID_BUTTON_FASE_7 = 7,
	GUI_ID_BUTTON_FASE_8 = 8,
	GUI_ID_BUTTON_FASE_9 = 9,
	GUI_ID_BUTTON_FASE_10 = 10,
	GUI_ID_BUTTON_FASE_11 = 11,
	GUI_ID_BUTTON_FASE_12 = 12,
	GUI_ID_BUTTON_FASE_WINDOWS_CLOSE = 16,
	GUI_ID_BUTTON_FASE_WINDOWS_MINIMIZE = 17,
	GUI_ID_BUTTON_FASE_MAIN_MENU = 18

};




class MenuFases : public IEventReceiver{

    public:
	    MenuFases(IVideoDriver* driver, 
			      ISceneManager* smgr,
			      IGUIEnvironment* guienv, 
			      IrrlichtDevice *device,
				  ISoundEngine* sound_engine,
				  sJogo* jogo);

		void createMenuFases();
		
		virtual bool OnEvent(const SEvent& event);
        virtual ~MenuFases(void);
		

	private:
		IVideoDriver* driver;
		ISoundEngine* sound_engine;
		ISound* intro_music;
		ISceneManager* smgr;
		IGUIEnvironment* guienv;
		IrrlichtDevice *device;
		sJogo* jogo;
        
		int selected_gender;

		int w_width;
		int w_height;

		void drawBackground();
		void drawWindowButtons();
		void drawLevelsButtons();

};


#endif
