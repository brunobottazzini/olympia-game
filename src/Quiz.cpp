#include "Quiz.h"


/*  */
MenuEventReceiver_QUIZ::MenuEventReceiver_QUIZ(IVideoDriver* driver, 
	                                           ISceneManager* smgr, 
											   IGUIEnvironment* guienv, 
											   IrrlichtDevice *device,
											   ISoundEngine* sound_engine,
											   sQuiz* s_quiz,
							                   sQuestion* s_question,
											   struct sJogo* jogo){

	this->driver = driver;
	this->smgr = smgr;
	this->guienv = guienv;
	this->device = device;
	this->sound_engine = sound_engine;
	this->s_quiz = s_quiz;
	this->s_question = s_question;
	this->s_quiz->right_answers = 0;
	this->jogo = jogo;

	this->w_width = driver->getScreenSize().Width;  // largura da tela
	this->w_height = driver->getScreenSize().Height; // altura da tela
	

}


/* 
Cria um novo quiz
*/
void MenuEventReceiver_QUIZ::createQuiz(sQuiz* s_quiz, int hmq, int ra, sQuestion* s_question){


	MenuEventReceiver_QUIZ receiver_options_quiz(this->driver, this->smgr, this->guienv, this->device, this->sound_engine, this->s_quiz, this->s_question, this->jogo);
	this->device->setEventReceiver(&receiver_options_quiz);
		
	setQuizHowManyQuestions(s_quiz, hmq);
	setQuizRightAnswersToPass(s_quiz, ra);
	setQuizActualPage(s_quiz, -1);
	
	updateQuiz(s_quiz, s_question);


}


void MenuEventReceiver_QUIZ::updateQuiz(sQuiz* s_quiz, sQuestion* s_question){

	//this->sound_engine->stopAllSounds();

	setQuizActualPage(s_quiz, getQuizActualPage(s_quiz) + 1);

	setQuestionImagePath(s_question, "");
	setQuestionImagePathAnswer(s_question, "");
	setQuestionRightAnswer(s_question, 0);

	selectQuestion(s_quiz, s_question);
	drawQuestionScreen(s_quiz, s_question);

}


void MenuEventReceiver_QUIZ::selectQuestion(sQuiz* s_quiz, sQuestion* s_question){
	
	char* image_path_question;
	char* image_path_answer;
	int right_answer, random_number;
	
	// Inicializa seed randomica
    srand (time(NULL));
	
	random_number = rand() % 20 + 1;

	while( isQuizAlreadySelected(s_quiz, random_number, getQuizActualPage(s_quiz)) > 0 ){

		random_number = rand() % 20 + 1;
	
	}

	setQuizAlreadySelected(s_quiz, random_number, getQuizActualPage(s_quiz));

	switch(random_number){

	case 1:
		image_path_question = "system//images//quiz//questao_01.jpg";
		image_path_answer = "system//images//quiz//resposta_01.jpg";
		right_answer = C;
	break;


	case 2:
		image_path_question = "system//images//quiz//questao_02.jpg";
		image_path_answer = "system//images//quiz//resposta_02.jpg";
		right_answer = C;
	break;


	case 3:
		image_path_question = "system//images//quiz//questao_03.jpg";
		image_path_answer = "system//images//quiz//resposta_03.jpg";
		right_answer = B;
	break;


	case 4:
		image_path_question = "system//images//quiz//questao_04.jpg";
		image_path_answer = "system//images//quiz//resposta_04.jpg";
		right_answer = A;
	break;


	case 5:
		image_path_question = "system//images//quiz//questao_05.jpg";
		image_path_answer = "system//images//quiz//resposta_05.jpg";
		right_answer = A;
	break;


	case 6:
		image_path_question = "system//images//quiz//questao_06.jpg";
		image_path_answer = "system//images//quiz//resposta_06.jpg";
		right_answer = B;
	break;


	case 7:
		image_path_question = "system//images//quiz//questao_07.jpg";
		image_path_answer = "system//images//quiz//resposta_07.jpg";
		right_answer = D;
	break;


	case 8:
		image_path_question = "system//images//quiz//questao_08.jpg";
		image_path_answer = "system//images//quiz//resposta_08.jpg";
		right_answer = D;
	break;


	case 9:
		image_path_question = "system//images//quiz//questao_09.jpg";
		image_path_answer = "system//images//quiz//resposta_09.jpg";
		right_answer = D;
	break;


	case 10:
		image_path_question = "system//images//quiz//questao_10.jpg";
		image_path_answer = "system//images//quiz//resposta_10.jpg";
		right_answer = C;
	break;


	case 11:
		image_path_question = "system//images//quiz//questao_11.jpg";
		image_path_answer = "system//images//quiz//resposta_11.jpg";
		right_answer = A;
	break;


	case 12:
		image_path_question = "system//images//quiz//questao_12.jpg";
		image_path_answer = "system//images//quiz//resposta_12.jpg";
		right_answer = A;
	break;


	case 13:
		image_path_question = "system//images//quiz//questao_13.jpg";
		image_path_answer = "system//images//quiz//resposta_13.jpg";
		right_answer = C;
	break;


	case 14:
		image_path_question = "system//images//quiz//questao_14.jpg";
		image_path_answer = "system//images//quiz//resposta_14.jpg";
		right_answer = D;
	break;


	case 15:
		image_path_question = "system//images//quiz//questao_15.jpg";
		image_path_answer = "system//images//quiz//resposta_15.jpg";
		right_answer = C;
	break;


	case 16:
		image_path_question = "system//images//quiz//questao_16.jpg";
		image_path_answer = "system//images//quiz//resposta_16.jpg";
		right_answer = A;
	break;


	case 17:
		image_path_question = "system//images//quiz//questao_17.jpg";
		image_path_answer = "system//images//quiz//resposta_17.jpg";
		right_answer = A;
	break;


	case 18:
		image_path_question = "system//images//quiz//questao_18.jpg";
		image_path_answer = "system//images//quiz//resposta_18.jpg";
		right_answer = B;
	break;


	case 19:
		image_path_question = "system//images//quiz//questao_19.jpg";
		image_path_answer = "system//images//quiz//resposta_19.jpg";
		right_answer = D;
	break;


	case 20:
		image_path_question = "system//images//quiz//questao_20.jpg";
		image_path_answer = "system//images//quiz//resposta_20.jpg";
		right_answer = B;
	break;

	}

	setQuestionImagePath(s_question, image_path_question);

	setQuestionImagePathAnswer(s_question, image_path_answer);

	setQuestionRightAnswer(s_question, right_answer);

}



void MenuEventReceiver_QUIZ::drawQuestionScreen(sQuiz* s_quiz, sQuestion* s_question){


	// Limpa a cena e os elementos GUI
	this->smgr->clear();
	this->guienv->clear();

	// Adiciona uma nova camera
	//ICameraSceneNode* camera = smgr->addCameraSceneNode(0, vector3df(0,30,-40), vector3df(1,1,0));
	
	// Cria uma nova cena e define a imagem da pergunta ser desenhada
	/*scene::ISceneNode* bill = this->smgr->addBillboardSceneNode(0, core::dimension2d<f32>(99, 75));
	bill->setMaterialFlag(video::EMF_LIGHTING, false);
	bill->setPosition(vector3df(0.0f,0.0f,0.0f));
	bill->setScale(vector3df(0.2f,0.2f,0.2f));
	bill->setMaterialTexture(0, driver->getTexture( getQuestionImagePath(s_question) ));*/

	this->img = driver->getTexture( getQuestionImagePath(s_question) );

	IGUIImage* bg_img = guienv->addImage(core::rect<s32>(0 ,0, driver->getScreenSize().Width, driver->getScreenSize().Height), 0);
	bg_img->setRelativePositionProportional(core::rect<f32>(-0.007, 0, 1, 1));
	bg_img->setScaleImage(true);
	bg_img->setImage( this->img );

	playSoundEffect(QUIZ_LOOP, true);

	// Carrega a imagem da tela de resposta
	this->img = driver->getTexture( getQuestionImagePathAnswer(s_question) );
	
	// Desenha os bot�es de resposta
	drawAnswersButtons(s_question);
	
	drawPagination(s_quiz, s_question);

	drawWindowButtons();

	// Desenha a cena criada
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(0,0,0,0));
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}
	
}


void MenuEventReceiver_QUIZ::drawAnswerScreen(sQuiz* s_quiz, sQuestion* s_question){

    this->guienv->clear();
	this->img = driver->getTexture( getQuestionImagePathAnswer(s_question) );

	IGUIImage* bg_img = guienv->addImage(core::rect<s32>(0 ,0, driver->getScreenSize().Width, driver->getScreenSize().Height), 0);
	bg_img->setRelativePositionProportional(core::rect<f32>(-0.007, 0, 1, 1));
	bg_img->setScaleImage(true);
	bg_img->setImage( this->img );
	
	this->sound_engine->stopAllSounds();
	//playSoundEffect(QUIZ_LOOP, true);

	if(getQuestionGivenAnswer(s_question) == getQuestionRightAnswer(s_question) ){
		playSoundEffect(QUIZ_ACERTO, false);
		setQuizRightAnswers(s_quiz);
	}else{
		playSoundEffect(QUIZ_ERRO, false);
	}

	drawForwardButton(s_quiz);

	drawWindowButtons();

}


void MenuEventReceiver_QUIZ::drawResultScreen(sQuiz* s_quiz){

    this->guienv->clear();

	this->sound_engine->stopAllSounds();

	if( getQuizRightAnswers(s_quiz) >= getQuizRightAnswersToPass(s_quiz) ){
		playSoundEffect(QUIZ_VITORIA, false);
		this->img = driver->getTexture("system//images//quiz//resultado_sucesso.jpg");
		this->jogo->opened_levels[2] = 1;
	}else{
		playSoundEffect(QUIZ_DERROTA, false);
		this->img = driver->getTexture("system//images//quiz//resultado_falha.jpg");
	}

	IGUIImage* bg_img = guienv->addImage(core::rect<s32>(0 ,0, driver->getScreenSize().Width, driver->getScreenSize().Height), 0);
	bg_img->setRelativePositionProportional(core::rect<f32>(-0.007, 0, 1, 1));
	bg_img->setScaleImage(true);
	bg_img->setImage( this->img );

	drawGoToLevelScreenButton();

}



void MenuEventReceiver_QUIZ::drawPagination(sQuiz* s_quiz, sQuestion* s_question){

    IGUIFont* fonte = device->getGUIEnvironment()->getFont("system//font//NewAthenaUnicode14px//newathenaunicode_14px.xml");

    IGUIStaticText* txt_paginacao = this->guienv->addStaticText(L"", core::rect<s32>(15,680,155,764), false, false, 0);
	txt_paginacao->setOverrideFont(fonte);
	txt_paginacao->setOverrideColor(video::SColor(0xff597d3d));
	txt_paginacao->setTextAlignment(EGUIA_CENTER, EGUIA_UPPERLEFT);

	int questions = getQuizHowManyQuestions(s_quiz);
	int pagina = getQuizActualPage(s_quiz);
	stringw str = L"Quest�o ";
	str += pagina + 1;
	str += L" de ";
	str += questions;
	/*str += L"\n";
	str += getQuizRightAnswers(s_quiz);
	str += L"\n";
	str += getQuizRightAnswersToPass(s_quiz);
	str += L"\n";
	str += s_quiz->already_selected[2];
	str += L"\n";
	str += s_quiz->already_selected[3];
		str += L"\n";
	str += s_quiz->already_selected[4];
		str += L"\n";
	str += s_quiz->already_selected[5];
		str += L"\n";
	str += s_quiz->already_selected[6];
		str += L"\n";
	str += s_quiz->already_selected[7];
		str += L"\n";
	str += s_quiz->already_selected[8];
			str += L"\n";
	str += s_quiz->already_selected[9];*/

	txt_paginacao->setText(str.c_str());

}



void MenuEventReceiver_QUIZ::drawAnswersButtons(sQuestion* s_question){

	int sm_x = (driver->getScreenSize().Width) / 2;  // meio da tela em x
	int sm_y = (driver->getScreenSize().Height) / 2; // meio da tela em y

	IGUIButton* btn_answer_a = guienv->addButton(rect<s32>(sm_x - 320, 520, sm_x - 256, 560), 0, GUI_ID_BUTTON_QUIZ_A, L"");
	btn_answer_a->setImage(driver->getTexture("system//images//menus//btn_quiz_a.png"));

	IGUIButton* btn_answer_b = guienv->addButton(rect<s32>(sm_x - 320, 585, sm_x - 256, 625), 0, GUI_ID_BUTTON_QUIZ_B, L"");
	btn_answer_b->setImage(driver->getTexture("system//images//menus//btn_quiz_b.png"));

	IGUIButton* btn_answer_c = guienv->addButton(rect<s32>(sm_x - 320, 650, sm_x - 256, 690), 0, GUI_ID_BUTTON_QUIZ_C, L"");
	btn_answer_c->setImage(driver->getTexture("system//images//menus//btn_quiz_c.png"));

	IGUIButton* btn_answer_d = guienv->addButton(rect<s32>(sm_x - 320, 715, sm_x - 256, 755), 0, GUI_ID_BUTTON_QUIZ_D, L"");
	btn_answer_d->setImage(driver->getTexture("system//images//menus//btn_quiz_d.png"));

}


void MenuEventReceiver_QUIZ::drawForwardButton(sQuiz* s_quiz){

	// Bot�o para avan�ar para a pr�xima tela
	IGUIButton* btn_forward = guienv->addButton(rect<s32>(w_width - 138, w_height - 74, w_width - 10, w_height - 10), 0, GUI_ID_BUTTON_FORWARD, L"");
	btn_forward->setImage(driver->getTexture("system//images//menus//btn_quiz_proxima.png"));

	// Bot�o para ver a tela de resultados
	IGUIButton* btn_results = guienv->addButton(rect<s32>(w_width - 138, w_height - 74, w_width - 10, w_height - 10), 0, GUI_ID_BUTTON_RESULTS, L"");
	btn_results->setImage(driver->getTexture("system//images//menus//btn_quiz_ver_resultado.png"));

	btn_forward->setVisible(false);
	btn_results->setVisible(false);

	if(getQuizActualPage(s_quiz) < getQuizHowManyQuestions(s_quiz) - 1){
		btn_forward->setVisible(true);
	}else{
		btn_results->setVisible(true);
	}

}




void MenuEventReceiver_QUIZ::drawGoToLevelScreenButton(){

	// Bot�o para avan�ar para a pr�xima tela
	IGUIButton* btn_go = guienv->addButton(rect<s32>(w_width - 138, w_height - 74, w_width - 10, w_height - 10), 0, GUI_ID_BUTTON_GO_TO_LEVEL, L"");
	btn_go->setImage(driver->getTexture("system//images//menus//btn_ir_menu_fases.png"));
}



void MenuEventReceiver_QUIZ::drawWindowButtons(){

	int width = driver->getScreenSize().Width;  // largura da tela
	int height = driver->getScreenSize().Height; // altura da tela

	// BOt�o para minimizar a janela
	IGUIButton* btn_window_minimize = guienv->addButton(rect<s32>(width-64, 0, width-32, 32), 0, GUI_ID_BUTTON_WINDOWS_MINIMIZE, L"");
	btn_window_minimize->setImage(driver->getTexture("system//images//menus//btn_window_minimize.png"));

	// Bot�o para fechar a janela
	IGUIButton* btn_window_close = guienv->addButton(rect<s32>(width-32, 0, width, 32), 0, GUI_ID_BUTTON_WINDOWS_CLOSE, L"");
	btn_window_close->setImage(driver->getTexture("system//images//menus//btn_window_close.png"));


}



void  MenuEventReceiver_QUIZ::playSoundEffect(int what_sound, bool repeat){

	char* sound_path;

	switch(what_sound){

	    case QUIZ_LOOP: sound_path = "system//sounds//QUIZ_LOOP_01.mp3"; break;
		case QUIZ_ACERTO: sound_path = "system//sounds//QUIZ_VITORIA_1.mp3"; break;
		case QUIZ_ERRO: sound_path = "system//sounds//QUIZ_DERROTA_1.mp3"; break;
		case QUIZ_VITORIA: sound_path = "system//sounds//VITORIA.mp3"; break;
		case QUIZ_DERROTA: sound_path = "system//sounds//DERROTA.mp3"; break;

	}

	ISound* sound_effect = this->sound_engine->play3D(sound_path, vec3df(0,0,0), false, false, true);;
	if(repeat) sound_effect->setIsLooped(true);

}




void MenuEventReceiver_QUIZ::setQuizHowManyQuestions(sQuiz* s_quiz, int questions){

	(*s_quiz).how_many_questions = questions;

}

int MenuEventReceiver_QUIZ::getQuizHowManyQuestions(sQuiz* s_quiz){

	return s_quiz->how_many_questions;

}



void MenuEventReceiver_QUIZ::setQuizRightAnswersToPass(sQuiz* s_quiz, int ra){

	s_quiz->right_answers_to_pass = ra;

}

int MenuEventReceiver_QUIZ::getQuizRightAnswersToPass(sQuiz* s_quiz){

	return s_quiz->right_answers_to_pass;

}





void MenuEventReceiver_QUIZ::setQuizRightAnswers(sQuiz* s_quiz){

	s_quiz->right_answers += 1;

}

int MenuEventReceiver_QUIZ::getQuizRightAnswers(sQuiz* s_quiz){

	return s_quiz->right_answers;

}




void MenuEventReceiver_QUIZ::setQuizActualPage(sQuiz* s_quiz, int page){

	s_quiz->actual_page = page;

}

int MenuEventReceiver_QUIZ::getQuizActualPage(sQuiz* s_quiz){

	return s_quiz->actual_page;

}




void MenuEventReceiver_QUIZ::setQuizAlreadySelected(sQuiz* s_quiz, int selected, int actual_page){
    
	s_quiz->already_selected[actual_page] = selected;

}


int MenuEventReceiver_QUIZ::isQuizAlreadySelected(sQuiz* s_quiz, int selected, int actual_page){

	int i, found = 0;

	for(i = 0; i < QTD_SELECIONADAS; i++){

		if(s_quiz->already_selected[i] == selected){
			found ++;
		}

	}
	
	return found;

}




void MenuEventReceiver_QUIZ::setQuestionImagePath(sQuestion* s_question, char* image_path){

	s_question->image_path_question = image_path;

}

char* MenuEventReceiver_QUIZ::getQuestionImagePath(sQuestion* s_question){

	return s_question->image_path_question;

}



void MenuEventReceiver_QUIZ::setQuestionImagePathAnswer(sQuestion* s_question, char* image_path){

	s_question->image_path_answer = image_path;

}

char* MenuEventReceiver_QUIZ::getQuestionImagePathAnswer(sQuestion* s_question){

	return s_question->image_path_answer;

}



void MenuEventReceiver_QUIZ::setQuestionRightAnswer(sQuestion* s_question, int ra){

	s_question->right_answer = ra;

}

int MenuEventReceiver_QUIZ::getQuestionRightAnswer(sQuestion* s_question){

	return s_question->right_answer;

}



void MenuEventReceiver_QUIZ::setQuestionGivenAnswer(sQuestion* s_question, int given_answer){

	s_question->given_answer = given_answer;

}

int MenuEventReceiver_QUIZ::getQuestionGivenAnswer(sQuestion* s_question){

	return s_question->given_answer;

}




bool MenuEventReceiver_QUIZ::OnEvent(const SEvent& event)
{

	MenuFases* menu_fases = new MenuFases(this->driver, 
		                                  this->smgr,
										  this->guienv, 
										  this->device,
										  this->sound_engine,
										  this->jogo);

	/*
		Neste metodo e descrito acoes que serao dadas caso haja um botao (que foi passado o ID) ira realizar
		Lembre-se que este ID e declarado no struct enum.
	*/
	if (event.EventType == EET_GUI_EVENT)
	{
		s32 id = event.GUIEvent.Caller->getID();
		IGUIEnvironment* env = this->device->getGUIEnvironment();

		switch(event.GUIEvent.EventType)
		{
			case EGET_SCROLL_BAR_CHANGED:
				break;

			case EGET_LISTBOX_CHANGED:
				break;

			case EGET_LISTBOX_SELECTED_AGAIN:
				break;

			case EGET_CHECKBOX_CHANGED:
				break;


			case EGET_BUTTON_CLICKED:
				switch(id)
				{
				case GUI_ID_BUTTON_QUIZ_A:
					setQuestionGivenAnswer(this->s_question, A);
					drawAnswerScreen(this->s_quiz, this->s_question);
					return true;
                case GUI_ID_BUTTON_QUIZ_B:
					setQuestionGivenAnswer(this->s_question, B);
					drawAnswerScreen(this->s_quiz, this->s_question);
					return true;
				case GUI_ID_BUTTON_QUIZ_C:
					setQuestionGivenAnswer(this->s_question, C);
					drawAnswerScreen(this->s_quiz, this->s_question);
					return true;
				case GUI_ID_BUTTON_QUIZ_D:
					setQuestionGivenAnswer(this->s_question, D);
					drawAnswerScreen(this->s_quiz, this->s_question);
					return true;
				case GUI_ID_BUTTON_FORWARD:
					updateQuiz(this->s_quiz, this->s_question);
					return true;
				case GUI_ID_BUTTON_RESULTS:
					drawResultScreen(this->s_quiz);
					return true;

				case GUI_ID_BUTTON_GO_TO_LEVEL:
					this->sound_engine->stopAllSounds();
					this->intro_music = sound_engine->play3D("system//music//intro.wav", vec3df(0,0,0), false, false, true);
	                this->intro_music->setIsLooped(true);
					menu_fases->createMenuFases();
					return true;

				case GUI_ID_BUTTON_WINDOWS_MINIMIZE:
					this->device->minimizeWindow();
					return true;
				case GUI_ID_BUTTON_WINDOWS_CLOSE:
					exit(1);
					return true;
				default:
					return false;
				}
				break;

			default:
				break;
		}
	}

	return false;
}