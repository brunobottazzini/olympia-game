#include "NPC.h"

NPC::NPC (IrrlichtDevice *device, IVideoDriver* driver, char caminhoModelo [], char caminhoTextura [],	bool lightning)
{
	this->smgr = device->getSceneManager();
    this->mesh = this->smgr->getMesh(caminhoModelo);
	this->node = this->smgr->addAnimatedMeshSceneNode( mesh );
	this->caminhotexture = caminhoTextura;
	this->driver = driver;
	this->speed = 2.0;
	this->lightning = lightning;
	if (node)
	{
		node->setMD2Animation(scene::EMAT_STAND);
		node->setMaterialFlag(EMF_LIGHTING, lightning);
		node->setMaterialTexture( 0, driver->getTexture(caminhoTextura) );
		if(lightning == true){
		this->node->addShadowVolumeSceneNode();
		smgr->setShadowColor(video::SColor(150,0,0,0));
		//this->node->setScale(core::vector3df(1,1,1));
		this->node->setMaterialFlag(video::EMF_NORMALIZE_NORMALS, lightning);
		}
	}
	this->LapFlag = 0;
}

void NPC::setMesh(char caminho_mesh[]){
	IAnimatedMesh* tempMesh = this->smgr->getMesh(caminho_mesh);
	this->node->setMesh(tempMesh);
	this->node->setMaterialTexture( 0, driver->getTexture(this->caminhotexture) );
			this->node->addShadowVolumeSceneNode();
			node->setMaterialFlag(EMF_LIGHTING, false); 
			//smgr->setShadowColor(video::SColor(150,0,0,0));
			//this->node->setScale(core::vector3df(1,1,1));
			//this->node->setMaterialFlag(video::EMF_NORMALIZE_NORMALS, this->ligthning);
}

IAnimatedMesh* NPC::getMesh(){
	return this->mesh;
}

void NPC::setLapFlag(short flag){
	this->LapFlag = flag;
}

short NPC::getLapFlag(){
	return this->LapFlag;
}

IAnimatedMeshSceneNode* NPC::getNode(){
	return this->node;
}

void NPC::setPosition(float posX, float posY, float posZ)
{
	 core::vector3df playerPos;
     playerPos.X = posX;
	 playerPos.Y = posY;
	 playerPos.Z = posZ;
	 this->node->setPosition(playerPos);
}

void NPC::setModelSpeed(float speed){
	this->speed = speed;
	if(speed >= 2.0 && speed <= 3.9){
		this->node->setAnimationSpeed(75);
	}else if (speed >= 4.0 && speed <= 5.9){
		this->node->setAnimationSpeed(95);
	}else if (speed >= 6.0 && speed <= 7.5){
		this->node->setAnimationSpeed(115);
	}
}
void NPC::setPosition(core::vector3df npcPos){
	this->node->setPosition(npcPos);
}

void NPC::rotateNPC(float x, float y, float z){
	this->node->setRotation(core::vector3df(x,y,z));
}

void NPC::setAnimationMD2(irr::scene::EMD2_ANIMATION_TYPE anim){
	this->node->setMD2Animation(anim);
}

void NPC::setAnimationMD2(char path[]){
	this->node->setMD2Animation(path);
}

vector3df NPC::getPosition(){
	return this->node->getPosition();
}