#include "keyboard_menu.h"
   // This is the one method that we have to implement
   bool MyEventReceiver_menu::OnEvent(const SEvent& event)
   {
      // Remember whether each key is down or up
      if (event.EventType == irr::EET_KEY_INPUT_EVENT)
      {
         KeyIsDown[event.KeyInput.Key] = event.KeyInput.PressedDown;
      }
      return false;
   }

   // This is used to check whether a key is being held down
   bool MyEventReceiver_menu::IsKeyDown(EKEY_CODE keyCode)
   {
      return KeyIsDown[keyCode];
   }

   bool MyEventReceiver_menu::IsKeyUp(EKEY_CODE keyCode)
   {
      if((KeyIsDown[keyCode]) && (!KeyIsDownPrev[keyCode]))
      {
         KeyIsDownPrev[keyCode] = KeyIsDown[keyCode];
         return false;
      }
      if( (!KeyIsDown[keyCode]) && (KeyIsDownPrev[keyCode]) )
      {
         KeyIsDownPrev[keyCode] = false;
         return true;;//the key has been pressed and released
      }
      return false;
   }
   
   bool MyEventReceiver_menu::IsKeyToggled(EKEY_CODE keyCode)
   {
      if(KeyIsDown[keyCode])
      {
         if(!KeyIsToggled[keyCode])
         {
            KeyIsToggled[keyCode] = true;
            return true;
         }
         KeyIsDownPrev[keyCode] = KeyIsDown[keyCode];
      }
      else
      {
         if(IsKeyUp(keyCode))
         {
            if(KeyIsToggled[keyCode])
            {
               if(KeyIsDownPrev[keyCode])
               {
                  KeyIsToggled[keyCode] = false;
                  KeyIsDownPrev[keyCode] = false; 
                  return true;
               }
               else
               {
                  return true;
               }
            }
         }
      }
      return false;
   }
   void MyEventReceiver_menu::clearKeyDown(EKEY_CODE keyCode)
   {
      KeyIsDown[keyCode] = false;
   }

   MyEventReceiver_menu::MyEventReceiver_menu()
   {
      for (u32 i=0; i<KEY_KEY_CODES_COUNT; ++i)
      {
         KeyIsDown[i] = false;
         KeyIsDownPrev[i] = false;
         KeyIsToggled[i] = false;
         KeyInToggle[i] = false;
      }
   }