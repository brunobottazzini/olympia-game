/*
Arquivo de Cricao do Personagem para a corrida, Tambem ira realizar controle 
da HUD e Jogabilidade
*/
#include "Main_Char.h"

Main_Char::Main_Char(IrrlichtDevice* device, IVideoDriver* driver, MyEventReceiver* receiver, char caminhoModelo [], char caminhoTextura [], int size_w, int size_h, bool lightning){
	/*	
		Construtor da Classe onde ira ser passado para nos os parametros necessarios de inicializacao do 
		personagem para corrida.

		Lembrando que este metodo cria Uma malha animada do arquivo passado pelo caminhoModelo
	*/
	//// Initialize Vars Needed
	this->receiver = receiver; // Necess�rio passa endere�o da variavel para ser manipulado corretamente.
	this->direction = 0; 
	this->zdirection = 0;
	this->iteracao = 0;
	this->iteracao_dec = 0;
	this->iteracao_dec2 = 0;
	this->stamina_bar_size = 0;
	this->interacao_sw_foot = 0;
	this->change_x = 0;
	this->velocidade_max = VELOCIDADE_MIN;
	this->switch_feet = false;
	this->still_running = false;
	this->lapFlag = false;
	this->driver = driver;
	this->size_w = size_w;
	this->size_h = size_h;
	this->caminhotexture = caminhoTextura;
	this->ligthning = lightning;
	////
	this->smgr = device->getSceneManager();
	this->mesh = smgr->getMesh(caminhoModelo);
	this->node = smgr->addAnimatedMeshSceneNode( mesh );
	//this->node->setAnimationSpeed(VELOCIDADE_ANIM_MAX);
	if (node)
	{
		//this->node->setMaterialFlag(EMF_LIGHTING, true);
		this->node->setMaterialFlag(EMF_LIGHTING, lightning);
		this->node->getMaterial(0).TextureLayer[2].BilinearFilter = false;
		this->node->setMaterialFlag(video::EMF_BACK_FACE_CULLING, false);
		//this->node->setMaterialFlag(
		//this->node->setMD2Animation(scene::EMAT_STAND);
		this->node->setMaterialTexture( 0, driver->getTexture(caminhoTextura) );
		if(lightning == true){
			//this->node->addShadowVolumeSceneNode();
			//smgr->setShadowColor(video::SColor(150,0,0,0));
			this->node->setScale(core::vector3df(1,1,1));
			this->node->setMaterialFlag(video::EMF_NORMALIZE_NORMALS, lightning);
		}
		this->node->setAnimationSpeed(60);
		//this->node->set
		this->setSpeed = true;
		//this->node->setMD2Animation("idle");
	}
	
	// SONS DO PERSONAGEM
		this->sound_engine_2 = createIrrKlangDevice();
		this->ofegante = sound_engine_2->play3D("system//sounds//OFEGANTE.mp3",
	                              vec3df(0,0,0), false, false, true);
		this->ofegante->setIsLooped(true);
		this->muito_ofegante = sound_engine_2->play3D("system//sounds//EXAUSTO.mp3",
	                              vec3df(0,0,0), false, false, true);
		this->muito_ofegante->setIsLooped(true);
		
		this->ofegante->setIsPaused(true);
		this->muito_ofegante->setIsPaused(true);

	// FIM DO SONS DO PERSONAGEM
	// MOUNTING INTERFACE HUD
		this->foot_left = device->getGUIEnvironment()->addImage(driver->getTexture("system//images//ingame//foot//foot_l_b.png"),core::position2d<s32>(10,this->size_h-140));
		this->foot_right = device->getGUIEnvironment()->addImage(driver->getTexture("system//images//ingame//foot//foot-ri_b.png"),core::position2d<s32>(65,this->size_h-140));
		//this->arrow_right = device->getGUIEnvironment()->addImage(driver->getTexture("system//images//ingame//arrows//arrow_ri_b.png"),core::position2d<s32>(135,this->size_h-140));
		//this->arrow_left = device->getGUIEnvironment()->addImage(driver->getTexture("system//images//ingame//arrows//arrow_l_b.png"),core::position2d<s32>(265,this->size_h-140));
		this->arrow_right = device->getGUIEnvironment()->addImage(driver->getTexture("system//images//ingame//arrows//arrow_ri_b.png"),core::position2d<s32>(this->size_w-285,this->size_h-140));
		this->arrow_left = device->getGUIEnvironment()->addImage(driver->getTexture("system//images//ingame//arrows//arrow_l_b.png"),core::position2d<s32>(this->size_w-150,this->size_h-140));
		
		this->stamina = device->getGUIEnvironment()->addImage(driver->getTexture("system//images//ingame//stamina//player1//stamina0_p1.png"),core::position2d<s32>(5,this->size_h-450));
		this->showInterface(false);
	//
}

void Main_Char::setMesh(char caminho_mesh[]){
	IAnimatedMesh* tempMesh = smgr->getMesh(caminho_mesh);
	this->node->setMesh(tempMesh);
	this->node->setMaterialTexture( 0, driver->getTexture(this->caminhotexture) );
	if(this->ligthning == true){
			this->node->addShadowVolumeSceneNode();
			node->setMaterialFlag(EMF_LIGHTING, false); 
			//smgr->setShadowColor(video::SColor(150,0,0,0));
			//this->node->setScale(core::vector3df(1,1,1));
			//this->node->setMaterialFlag(video::EMF_NORMALIZE_NORMALS, this->ligthning);
	}
}

void Main_Char::showInterface(bool show){
	/*
		Metodo de controle deixar interface visivel
	*/
	this->foot_left->setVisible(show);
	this->foot_right->setVisible(show);
	this->arrow_left->setVisible(show);
	this->arrow_right->setVisible(show);
	this->stamina->setVisible(show);
}

void Main_Char::setScale(float scaX, float scaY, float scaZ){
	/*
		Metodo de controle de escala do modelo do personagem carregado
	*/
	vector3df scale(scaX, scaY, scaZ);
	this->node->setScale(scale);
}

void Main_Char::setScale(vector3df scale){
	/*
		Metodo (SOBRECARGA) de controle de escala do modelo do personagem carregado
	*/
	this->node->setScale(scale);
}

void Main_Char::setPosition(core::vector3df npcPos){
	/*
		Metodo de controle de posi��o do personagem principal
	*/
	this->node->setPosition(npcPos);
}

void Main_Char::setPosition(float posX, float posY, float posZ)
{
	/*
		Metodo (SOBRECARGA) de controle de posi��o do personagem principal
	*/
	 core::vector3df playerPos;
     playerPos.X = posX;
	 playerPos.Y = posY;
	 playerPos.Z = posZ;
	 this->node->setPosition(playerPos);
	 this->enabledMouse = false;
}

void Main_Char::rotateNPC(float x, float y, float z){
	/*
		Metodo para rotacionar Modelo do personagem
	*/
	this->node->setRotation(core::vector3df(x,y,z));
}

void Main_Char::setAnimationMD2(irr::scene::EMD2_ANIMATION_TYPE anim){
	/*
		Meotdo para selecionar tipo de animacao do personagem
	*/
	this->node->setMD2Animation(anim);
}

void Main_Char::setAnimationMD2(char path[]){
	/*
		Meotdo(SOBRE CARGA) para selecionar tipo de animacao do personagem
	*/
	this->node->setMD2Animation(path);
}


IAnimatedMeshSceneNode* Main_Char::getNode(){
	/*
	Metodo para Pegar o Node do personagem
	*/
	return this->node;
}
vector3df Main_Char::getPosition(){
	/*
	Metodo para Pegar a posicao do Personagem
	*/
	return this->node->getPosition();
}

float Main_Char::getVelocidade(){
	/*
		Metodo para Pegar a velocidade atual do personagem
	*/
	return this->velocidade_max;
}

bool Main_Char::Jogabilidade(ICameraSceneNode* camera){
	/*
		Aqui e realizado, a jogabilidade de CORRIDA do personagem principal.
		Onde ele ira alternar os botoes at� para conseguir correr mais rapido
		Lembrando que neste metodo tambem ha um controle de FADIGA (STAMINA)
		para que o personagem venha a se cansar.
	*/
	bool key_esc_test = this->receiver->IsKeyDown(KEY_ESCAPE);
	bool key_esc_test_up = this->receiver->IsKeyUp(KEY_ESCAPE);
	
	this->key_a_down = this->receiver->IsKeyDown(KEY_KEY_A);
	this->key_a_up = this->receiver->IsKeyUp(KEY_KEY_A);

	this->key_d_down = this->receiver->IsKeyDown(KEY_KEY_D);
	this->key_d_up = this->receiver->IsKeyUp(KEY_KEY_D);
	
	this->key_q_down = this->receiver->IsKeyDown(KEY_KEY_Q);
	this->key_q_up = this->receiver->IsKeyUp(KEY_KEY_Q);
	
	this->key_e_down = this->receiver->IsKeyDown(KEY_KEY_E);
	this->key_e_up = this->receiver->IsKeyUp(KEY_KEY_E);

	bool key_p_test = this->receiver->IsKeyDown(KEY_KEY_P); // TESTING KEY
	
	if(this->key_a_down){
		this->foot_left->setImage(this->driver->getTexture("system//images//ingame//foot//foot_l_r.png"));
	}else if(this->key_a_up){
		this->foot_left->setImage(this->driver->getTexture("system//images//ingame//foot//foot_l_b.png"));
	}
	if(this->key_d_down){
		this->foot_right->setImage(this->driver->getTexture("system//images//ingame//foot//foot_ri_r.png"));
	}else if(this->key_d_up){
		this->foot_right->setImage(this->driver->getTexture("system//images//ingame//foot//foot-ri_b.png"));
	}

	if(this->key_e_down){
			this->change_x += SENSIBILIDADE;
			if(this->change_x >= MAX_SENSE)this->change_x -= SENSIBILIDADE;
			this->arrow_left->setImage(this->driver->getTexture("system//images//ingame//arrows//arrow_l_r.png"));
	}else{
		if(this->change_x > -0.0000){
			this->change_x -= SENSIBILIDADE;
		}
		if(this->change_x > 0.2000)this->change_x = 0;
	}
	if(this->key_q_down){
			this->change_x -= SENSIBILIDADE;
			if(this->change_x <= (-1*MAX_SENSE))this->change_x += SENSIBILIDADE;
			this->arrow_right->setImage(this->driver->getTexture("system//images//ingame//arrows//arrow_ri_r.png"));
	}else{
		if(this->change_x < 0.0000){
			this->change_x += SENSIBILIDADE;
		}
		if(this->change_x < -0.2000)this->change_x = 0;
	}


	if(this->key_q_up){
		this->arrow_right->setImage(this->driver->getTexture("system//images//ingame//arrows//arrow_ri_b.png"));
	}

	if(this->key_e_up){
		this->arrow_left->setImage(this->driver->getTexture("system//images//ingame//arrows//arrow_l_b.png"));
	}

	if(this->stamina_bar_size <= 0){
		this->stamina->setImage(driver->getTexture("system//images//ingame//stamina//player1//stamina0_p1.png"));
	}else if(this->stamina_bar_size > 0 && this->stamina_bar_size < 2){
		this->stamina->setImage(driver->getTexture("system//images//ingame//stamina//player1//stamina1_p1.png"));
		if(!this->ofegante->getIsPaused()){
			this->ofegante->setIsPaused(true);
			this->muito_ofegante->setIsPaused(true);
		}
	}else if(this->stamina_bar_size >= 2 && this->stamina_bar_size < 3){
		this->stamina->setImage(driver->getTexture("system//images//ingame//stamina//player1//stamina2_p1.png"));
	}else if(this->stamina_bar_size >= 3 && this->stamina_bar_size < 4){
		if(this->ofegante->getIsPaused()){
			this->ofegante->setIsPaused(false);
			this->muito_ofegante->setIsPaused(true);
		}
		this->stamina->setImage(driver->getTexture("system//images//ingame//stamina//player1//stamina3_p1.png"));
	}else if(this->stamina_bar_size >= 4 && this->stamina_bar_size < 5){
		this->stamina->setImage(driver->getTexture("system//images//ingame//stamina//player1//stamina4_p1.png"));
	}else if(this->stamina_bar_size >= 5 && this->stamina_bar_size < 6){
		this->stamina->setImage(driver->getTexture("system//images//ingame//stamina//player1//stamina5_p1.png"));
	}else if(this->stamina_bar_size >= 6){
		if(this->muito_ofegante->getIsPaused()){
			this->ofegante->setIsPaused(true);
			this->muito_ofegante->setIsPaused(false);
		}
		this->stamina->setImage(driver->getTexture("system//images//ingame//stamina//player1//stamina6_p1.png"));
	}

			// True Jogabilidade
	// Set Running --- With run faster press a and d
	if(this->velocidade_max < VELOCIDADE_MIN)this->velocidade_max = VELOCIDADE_MIN;
	core::vector3df cameraPos = camera->getAbsolutePosition();
	core::vector3df playerPos = this->node->getAbsolutePosition();
	playerPos.Z = (playerPos.Z + sin( (-1 * this->direction) * PI / 180.0f ) * this->velocidade_max);
	playerPos.X = (playerPos.X + cos((this->direction) * PI / 180.0f ) * this->velocidade_max);
	this->node->setPosition(playerPos);
	
	if(this->stamina_bar_size >= 6){ // ESTOROU LIMITE DE STAMINA
		this->velocidade_max = VELOCIDADE_MIN;
		this->node->setAnimationSpeed(60);
	}

	if(this->velocidade_max < 4){
		this->node->setAnimationSpeed(75);
	} else if(this->velocidade_max >= 4 && this->velocidade_max <= 5){
		this->node->setAnimationSpeed(95);
	}else if(this->velocidade_max > 5 ){
		this->node->setAnimationSpeed(115);
	}
	
	if((this->key_d_down) == true && (this->key_a_up == true)){
			this->iteracao_dec2 = 0;
			this->iteracao_dec = 0;
			core::vector3df cameraPos = camera->getAbsolutePosition();
			core::vector3df playerPos = this->node->getAbsolutePosition();
			playerPos.Z = (playerPos.Z + sin( (-1 * this->direction) * PI / 180.0f ) * this->velocidade_max/2);
			playerPos.X = (playerPos.X + cos((this->direction) * PI / 180.0f ) * this->velocidade_max/2);
			this->iteracao = this->iteracao + 1;
			if(this->iteracao == 5)if(this->stamina_bar_size <= 6)this->stamina_bar_size += 0.5;
			if(this->iteracao >= 5){
				this->iteracao = 0;
				if(this->velocidade_max <= VELOCIDADE_MAX)this->velocidade_max = this->velocidade_max + SUM_VELOCIDADE;
			}
	}
	
	if(this->key_a_down == true && this->key_d_up == true){
		this->iteracao_dec2 = 0;
		this->iteracao_dec = 0;
		core::vector3df cameraPos = camera->getAbsolutePosition();
		core::vector3df playerPos = this->node->getAbsolutePosition();
		playerPos.Z = (playerPos.Z + sin( (-1 * this->direction) * PI / 180.0f ) * this->velocidade_max/2);
		playerPos.X = (playerPos.X + cos((this->direction) * PI / 180.0f ) * this->velocidade_max/2);
		this->iteracao = this->iteracao + 1;
		if(this->iteracao == 5)if(this->stamina_bar_size <= 6)this->stamina_bar_size += 0.5;
		if(this->iteracao >= 5){
			this->iteracao = 0;
			if(this->velocidade_max <= VELOCIDADE_MAX)this->velocidade_max = this->velocidade_max + SUM_VELOCIDADE;
		}		
	}

	if(this->key_a_down == false && this->key_d_down == false){
		if(this->velocidade_max >= VELOCIDADE_MIN){
			this->iteracao_dec = this->iteracao_dec + 1;
			if(this->iteracao_dec >= 50){
				this->iteracao_dec = 0;
				if(this->stamina_bar_size >= 0){
					this->stamina_bar_size -= 1;
					if(this->velocidade_max >= VELOCIDADE_MIN+0.2){
						this->velocidade_max = this->velocidade_max - SUB_VELOCIDADE;
					}
				}
			}
		}
	}
	if(this->key_a_down == true && this->key_d_down == true){
		if(this->velocidade_max >= VELOCIDADE_MIN){
			this->iteracao_dec2 = this->iteracao_dec2 + 1;
			if(this->iteracao_dec2 >= 50){
				this->iteracao_dec2 = 0;
				if(this->stamina_bar_size >= 0){
					this->stamina_bar_size -= 1;
					if(this->velocidade_max >= VELOCIDADE_MIN+0.2){
						this->velocidade_max = this->velocidade_max - SUB_VELOCIDADE;
					}
				}
			}
		}
	}
	if(key_esc_test == true && key_esc_test_up == false){
		return 1;
	}
	
	if(key_p_test == true){
	//core::vector3df playerPos = node->getAbsolutePosition();
		this->getPosition();
	}
	return false;
}

void Main_Char::setCameraControl(bool enabled){
	/*
		Deixa disponivel para o usuario mexer para os lados ou para cima.
	*/
	this->enabledMouse = enabled;
}

void Main_Char::turnCameraPlayer(IrrlichtDevice *device, float distance_x, float distance_y,float distance_z, float deepness, float speed){
	/*
		Metodo que faz com que a camera gire em volta do jogador na hora que
		acabar a corrida.
	*/
	core::position2d<f32> cursorPos;
	 cursorPos.X += 0.5;
	 cursorPos.Y = 1.5;
	//if(this->enabledMouse == false){
			// cursorPos.X = 0.5;
			//cursorPos.Y = 1.5;
	//}else{
		//cursorPos = device->getCursorControl()->getRelativePosition();
	//} 
	scene::ICameraSceneNode* camera = device->getSceneManager()->getActiveCamera();
     core::vector3df cameraPos = camera->getAbsolutePosition();
     
     //float change_x = ( cursorPos.X - 0.5 ) * 256.0f; // ---- Free Look Mouse
     float change_y = ( cursorPos.Y - 0.5 ) * 256.0f;
	 change_x = speed;
     this->direction += change_x;
     this->zdirection -= change_y;
     if( this->zdirection <- 90 )
         this->zdirection = -90;
     else
     if( this->zdirection > 90 )
         this->zdirection = 90;
     device->getCursorControl()->setPosition( 0.5f, 0.5f );
     
     core::vector3df playerPos = node->getPosition();
     
     float xf = playerPos.X - cos( this->direction * PI / 180.0f ) * 64.0f;
     float yf = playerPos.Y - sin( this->zdirection * PI / 180.0f ) * 64.0f;
     float zf = playerPos.Z + sin( this->direction * PI / 180.0f ) * 64.0f;
     
     camera->setPosition( core::vector3df( xf+deepness, yf, zf ) );
     camera->setTarget( core::vector3df( playerPos.X+distance_x, playerPos.Y+distance_y, playerPos.Z+distance_z ) );
	 //this->node->setRotation( core::vector3df( 0, this->direction-90, 0 ) );
}

void Main_Char::clearCameraControl(IrrlichtDevice *device){
	/*
		Metodo que faz com que o jogador jogue o jogo em 
		3rd pessoa.
	*/
     core::position2d<f32> cursorPos;
	 cursorPos.X = 0.5;
	 cursorPos.Y = 1.5;
	//if(this->enabledMouse == false){
			// cursorPos.X = 0.5;
			//cursorPos.Y = 1.5;
	//}else{
		//cursorPos = device->getCursorControl()->getRelativePosition();
	//} 
	scene::ICameraSceneNode* camera = device->getSceneManager()->getActiveCamera();
     core::vector3df cameraPos = camera->getAbsolutePosition();
     
     //float change_x = ( cursorPos.X - 0.5 ) * 256.0f; // ---- Free Look Mouse
     float change_y = ( cursorPos.Y - 0.5 ) * 256.0f;
     device->getCursorControl()->setPosition( 0.5f, 0.5f );
     
     core::vector3df playerPos = node->getPosition();
	 this->zdirection = -90;
	 this->direction = 0;
	 this->change_x = 0;

     float xf = playerPos.X - cos( this->direction * PI / 180.0f ) * 128.0f;
     float yf = playerPos.Y - sin( this->zdirection * PI / 180.0f ) * 96.0f;
     float zf = playerPos.Z + sin( this->direction * PI / 180.0f ) * 64.0f;
     
     camera->setPosition( core::vector3df( xf, yf, zf ) );
	 
     camera->setTarget( core::vector3df( playerPos.X, playerPos.Y+25.0f, playerPos.Z ) );
	// this->node->setRotation( core::vector3df( 0, this->direction-90, 0 ) );
}
void Main_Char::moveCameraControl(IrrlichtDevice *device)
{
	/*
		Metodo que faz com que o jogador jogue o jogo em 
		3rd pessoa.
	*/
     core::position2d<f32> cursorPos;
	 cursorPos.X = 0.5;
	 cursorPos.Y = 1.5;
	//if(this->enabledMouse == false){
			// cursorPos.X = 0.5;
			//cursorPos.Y = 1.5;
	//}else{
		//cursorPos = device->getCursorControl()->getRelativePosition();
	//} 
	scene::ICameraSceneNode* camera = device->getSceneManager()->getActiveCamera();
     core::vector3df cameraPos = camera->getAbsolutePosition();
     
     //float change_x = ( cursorPos.X - 0.5 ) * 256.0f; // ---- Free Look Mouse
     float change_y = ( cursorPos.Y - 0.5 ) * 256.0f;
     this->direction += change_x;
     this->zdirection -= change_y;
     if( this->zdirection <- 90 )
         this->zdirection = -90;
     else
     if( this->zdirection > 90 )
         this->zdirection = 90;
     device->getCursorControl()->setPosition( 0.5f, 0.5f );
     
     core::vector3df playerPos = node->getPosition();
     
     float xf = playerPos.X - cos( this->direction * PI / 180.0f ) * 128.0f;
     float yf = playerPos.Y - sin( this->zdirection * PI / 180.0f ) * 96.0f;
     float zf = playerPos.Z + sin( this->direction * PI / 180.0f ) * 64.0f;
     
     camera->setPosition( core::vector3df( xf, yf, zf ) );
     camera->setTarget( core::vector3df( playerPos.X, playerPos.Y+25.0f, playerPos.Z ) );
	 this->node->setRotation( core::vector3df( 0, this->direction-90, 0 ) );
}

void Main_Char::setHUDToBurningVideo(){
	/*
	Este metodo resolve o problema de proporcao em video que o burning video faz.
	Ja que o Burning reduz o maximo de resolucao para que consiga ganhar performance
	na renderizacao.
	*/
	this->foot_left->setRelativePosition(core::position2d<s32>(10,this->size_h-140));
	this->foot_right->setRelativePosition(core::position2d<s32>(70,this->size_h-139));
	this->arrow_right->setRelativePosition(core::position2d<s32>(125,this->size_h-140));
	this->arrow_left->setRelativePosition(core::position2d<s32>(250,this->size_h-140));
	this->stamina->setRelativePosition(core::position2d<s32>(5,this->size_h-430));
	this->foot_left->updateAbsolutePosition();
	this->foot_right->updateAbsolutePosition();
	this->arrow_right->updateAbsolutePosition();
	this->arrow_left->updateAbsolutePosition();
	this->stamina->updateAbsolutePosition();
}

bool Main_Char::getLapFlag(){
	/*
		E retornado uma flag alertando que o jogador ja fez a primeira volta
		e ele esta realizando sua ultima volta
	*/
	return this->lapFlag;
}

void Main_Char::setLapFlag(bool enabled){
	/*
		E setado uma flag alertando que o jogador ja fez a primeira volta
		e ele esta realizando sua ultima volta
	*/
	this->lapFlag = enabled;
}

void Main_Char::setPauseAllSounds(){
	this->ofegante->stop();
	this->muito_ofegante->stop();
}
