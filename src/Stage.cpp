#include "Stage.h"
#include "IntroLogos.h"

Stage::Stage(s32 render, bool fullscreen, bool antialiasing, s32 resolution, bool vsync, bool shadows)
{
	this->burningTest = false;
   	irr::SIrrlichtCreationParameters params;
	video::E_DRIVER_TYPE driverType;
    switch(render){
		case 0:	driverType = video::EDT_OPENGL;this->setLightning(true); break;
		case 1:	driverType = video::EDT_DIRECT3D8;this->setLightning(true); break;
		case 2:	driverType = video::EDT_DIRECT3D9;this->setLightning(true); break;
		case 3:	driverType = video::EDT_BURNINGSVIDEO;this->burningTest = true,this->setLightning(false); break;
		case 4:	driverType = video::EDT_SOFTWARE;this->setLightning(true); break;
		default: driverType = video::EDT_BURNINGSVIDEO,this->burningTest = true;this->setLightning(false); break;
	}
	switch(resolution){
		case 0: this->ratio = 1,this->window_weight = 800, this->window_height = 600, params.WindowSize=core::dimension2d<u32>( 800, 600 ); break;
		case 1: this->ratio = 1,this->window_weight = 1024, this->window_height = 768, params.WindowSize=core::dimension2d<u32>( 1024, 768 ); break;
		case 2: this->ratio = 1,this->window_weight = 1080, this->window_height = 1024, params.WindowSize=core::dimension2d<u32>( 1080, 1024 ); break;
		case 3: this->ratio = 2,this->window_weight = 1280, this->window_height = 720,  params.WindowSize=core::dimension2d<u32>( 1280, 720 ); break;
		case 4: this->ratio = 3,this->window_weight = 1280, this->window_height = 800,  params.WindowSize=core::dimension2d<u32>( 1280, 800 ); break;
		case 5: this->ratio = 3,this->window_weight = 1440, this->window_height = 900,  params.WindowSize=core::dimension2d<u32>( 1440, 900 ); break;
		default:this->ratio = 1,this->window_weight = 800, this->window_height = 600,  params.WindowSize=core::dimension2d<u32>( 800, 600 ); break;
	}
    params.DriverType=driverType;
    params.Bits=32;
    params.Fullscreen=fullscreen;
    params.EventReceiver=&receiver;
    params.AntiAlias=antialiasing;
    params.Stencilbuffer=shadows;
    params.Vsync=vsync;
	
	this->device = createDeviceEx(params);
	
    // Keyboard handling initiating


    if (!device)
            exit(1);

    this->device->setWindowCaption(L"Olympia");
	//this->device->getCursorControl()->setVisible(true);
	//this->device->run();
    this->driver = this->device->getVideoDriver();
    this->smgr = this->device->getSceneManager();
    this->guienv = this->device->getGUIEnvironment();
	// Removing Alpha from User interface
	SColor col_back;
	col_back.setAlpha(255);
	guienv->getSkin()->setColor((EGDC_3D_FACE), col_back);
	guienv->getSkin()->setColor((EGDC_3D_SHADOW), col_back);
	//Done Removing Alpha from User interface
	smgr->addCameraSceneNode(0, vector3df(0,30,-40), vector3df(0,5,0));
	scene::ICameraSceneNode*camera = device->getSceneManager()->getActiveCamera();
	this->sound_engine = createIrrKlangDevice();
	this->steps_engine = createIrrKlangDevice();
    this->intro_music = sound_engine->play3D("system//music//intro.wav",
	                              vec3df(0,0,0), false, false, true);
	this->intro_music->setIsLooped(true);
//	if(this->burningTest == 1)intro->playVideoInBurning(this->smgr,this->driver);
	//else intro->playVideo(this->smgr,this->driver);

	//Intro * logointro = new Intro(this->driver,this->smgr,this->guienv,this->device, &this->receiver);
	//logointro->playLogoIntro();

	IntroLogos* intro_logos = new IntroLogos(this->driver, 
		                                     this->smgr,
		                                     this->guienv, 
										     this->device,
										     this->sound_engine,
											 &this->receiver
											 );
	intro_logos->createIntroLogos();


	while(this->MountMenu() == false);

}

bool Stage::MountMenu(){

	this->smgr->clear();
	this->guienv->clear();
	this->device->getCursorControl()->setVisible(true);
	/*IGUIEnvironment* env = device->getGUIEnvironment();
	IGUISkin* skin = env->getSkin();
	IGUIFont* font = env->getFont("system//font//Galatia//Galatia.xml");
	if (font)
	skin->setFont(font);
	// IMAGEM DE FUNDO
	IGUIInOutFader* fader = env->addInOutFader();
	fader->fadeIn(2000);

	smgr->addCameraSceneNode(0, vector3df(0,30,-40), vector3df(0,5,0));
	scene::ICameraSceneNode*camera = device->getSceneManager()->getActiveCamera();
	scene::ISceneNode* bill;

	if(this->ratio == 1)bill = smgr->addBillboardSceneNode(0, core::dimension2d<f32>(100, 90));
	else if(this->ratio == 2)bill = smgr->addBillboardSceneNode(0, core::dimension2d<f32>(130, 90));
	else if(this->ratio == 3)bill = smgr->addBillboardSceneNode(0, core::dimension2d<f32>(120, 90));

	bill->setMaterialFlag(video::EMF_LIGHTING, false);
	bill->setPosition(vector3df(0.0f,0.0f,0.0f));
	bill->setScale(vector3df(0.1f,0.1f,0.1f));
	bill->setMaterialTexture(0, driver->getTexture("system//images//Logo.jpg"));
	// ------

	skin->setFont(env->getBuiltInFont(), EGDF_TOOLTIP);
	env->addButton(rect<s32>(10,440,110,440 + 32), 0, GUI_ID_START_CAMPANHA,
		L"Jogos Ol�mpicos", L"Comece uma campanha pelos jogos Ol�mpicos");
	env->addButton(rect<s32>(10,480,110,480 + 32), 0, GUI_ID_WINDOW_OPTIONS_CHOOSE_CONTEST,
		L"Treinamento", L"Escolha as provas para Treinar!");
	env->addButton(rect<s32>(10,520,110,520 + 32), 0, GUI_ID_WINDOW_OPTIONS_MAIN_GAME,
		L"Op��es", L"Op��es para o Jogo");
	env->addButton(rect<s32>(10,560,110,560 + 32), 0, GUI_ID_QUIT_BUTTON_MAIN_GAME,
		L"Sair", L"Sai para o Sistema Operacional");

	
	//Botao de teste do quiz
	IGUIButton* btnQuiz = env->addButton(rect<s32>(10,600,110,600 + 32), 0, GUI_ID_SHOW_QUIZ,
		L"", L"Exibir uma quest�o");
	btnQuiz->setImage(driver->getTexture("system//images//menus//menu_btn_testar_quiz.png"));

	SAppContext2 context_main;
	context_main.device = this->device;
	context_main.sound_engine = this->sound_engine;
	MenuEventReceiver_MAINGAME receiver_menu(context_main);
	context_main = this->load_game_options(context_main);
	this->device->setEventReceiver(&receiver_menu);*/


	// STARTS MUSIC IF IT IS NECESSARY
	if(this->intro_music->isFinished() == true){
		this->intro_music = this->sound_engine->play3D("system//music//intro.wav",
	                              vec3df(0,0,0), false, false, true);
		this->intro_music->setIsLooped(true);
	}

	// Inicializa a interfae do jogo
	
	while(this->device->run())
    {
		
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		/*if(context_main.okay_button_clicked == true){
			this->save_game_options(context_main);
			context_main.okay_button_clicked = false;
		}
		if(context_main.iniciar_campanha == true && context_main.okay_button_clicked == false){
			context_main.iniciar_campanha = false;
			while(this->Campanha(context_main.nivelDificuldade) == false);
			break;
		}
		if(context_main.iniciar_corrida == true  && context_main.okay_button_clicked == false){
			context_main.iniciar_corrida = false;
			this->GameCorrida(context_main.nivelDificuldade);
			break;
		}*/
		this->smgr->drawAll();
        this->guienv->drawAll();
        this->driver->endScene();
		
	}

	return false;
}

void Stage::save_game_options(SAppContext2 context){
	FILE * to_write_in = fopen("data//ingame.sys","w");
	char buffer[] = {context.som,context.musica,context.nivelDificuldade};
	fwrite(&buffer, 1,sizeof(buffer),to_write_in);
	fclose(to_write_in);
}

SAppContext2 Stage::load_game_options(SAppContext2 context){
	FILE * load;
	load = fopen("data//ingame.sys","r");
	char buffer[] = {context.som,context.musica,context.nivelDificuldade};
	if(load == NULL){
		FILE * to_write_in = fopen("data//ingame.sys","w");
		fwrite(&buffer, 1,sizeof(buffer),to_write_in);
		fclose(to_write_in);
	}else{
		while((fread(&buffer,sizeof(buffer),1,load)) == 1){
			context.som = buffer[0];
			context.musica = buffer[1];
			context.nivelDificuldade = buffer[2];
		}
		fclose(load);
	}
	return context;
}

bool Stage::Campanha(short dificuldade){
	this->smgr->clear();
	this->guienv->clear();
	this->intro_music->stop();
	Intro * initiatingIntro = new Intro(this->driver,this->smgr,this->guienv,this->device,&this->receiver);
	initiatingIntro->setRatio(this->ratio);
	initiatingIntro->playIntroGame1();
	short continuar = this->GameCorrida(dificuldade);
	if(continuar == 2)
		return true;
	this->device->getCursorControl()->setVisible(true);
	IGUISkin* skin = guienv->getSkin();
	//IGUIFont* font = guienv->getFont("system//font//Galatia.bmp");
	IGUIFont* font = guienv->getFont("system//font//Galatia//Galatia.xml");
	if (font)
	skin->setFont(font);
	skin->setFont(guienv->getBuiltInFont(), EGDF_TOOLTIP);
	if(continuar == 1){
	   this->smgr->clear();
	   this->guienv->clear();
	   smgr->addCameraSceneNode(0, vector3df(0,30,-40), vector3df(0,5,0));
	   this->receiver.clearKeyDown(KEY_SPACE);
	   scene::ISceneNode* bill;
	   if(this->ratio == 1)bill = smgr->addBillboardSceneNode(0, core::dimension2d<f32>(100, 90));
	   else if(this->ratio == 2)bill = smgr->addBillboardSceneNode(0, core::dimension2d<f32>(130, 90));
	   else if(this->ratio == 3)bill = smgr->addBillboardSceneNode(0, core::dimension2d<f32>(120, 90));
	   bill->setMaterialFlag(video::EMF_LIGHTING, false);
	   bill->setPosition(vector3df(0.0f,0.0f,0.0f));
	   bill->setScale(vector3df(0.1f,0.1f,0.1f));
	   bill->setMaterialTexture(0, driver->getTexture("system//images//pergunta.jpg"));
		MyEventReceiver * receiver_s = &this->receiver;
		this->device->setEventReceiver(receiver_s);
		//bool key_space_down;
	   SAppContext3 context;
	   MenuEventReceiver_QUESTIONS receiver_menu(context);
	   device->setEventReceiver(&receiver_menu);
	   context.device = this->device;
	   /*IGUIEnvironment* env = device->getGUIEnvironment();
	   env->addButton(rect<s32>(10,360,110,360 + 32), 0, GUI_ID_BUTTON_QUIZ_A,
	   L"A", L"");
		env->addButton(rect<s32>(10,400,110,400 + 32), 0, GUI_ID_BUTTON_QUIZ_B,
		L"B", L"");
		env->addButton(rect<s32>(10,440,110,440 + 32), 0, GUI_ID_BUTTON_QUIZ_C,
		L"C", L"");
		env->addButton(rect<s32>(10,480,110,480 + 32), 0, GUI_ID_BUTTON_QUIZ_D,
		L"D", L"");*/
	   bool right = false;
	   while(this->device->run()){
			this->driver->beginScene(true, true, SColor(255,100,101,140));
			if(context.chosen_options == 2){
				right = true;
				break;
			}else if(context.chosen_options != 0){
				right = false;
				break;
			}
			smgr->drawAll();
			this->guienv->drawAll();
			this->driver->endScene();
		}
	
	if(right == true){
		this->smgr->clear();
		this->guienv->clear();
		smgr->addCameraSceneNode(0, vector3df(0,30,-40), vector3df(0,5,0));
		this->receiver.clearKeyDown(KEY_SPACE);
		scene::ISceneNode* bill;
		if(this->ratio == 1)bill = smgr->addBillboardSceneNode(0, core::dimension2d<f32>(100, 90));
		else if(this->ratio == 2)bill = smgr->addBillboardSceneNode(0, core::dimension2d<f32>(130, 90));
		else if(this->ratio == 3)bill = smgr->addBillboardSceneNode(0, core::dimension2d<f32>(120, 90));
		bill->setMaterialFlag(video::EMF_LIGHTING, false);
		bill->setPosition(vector3df(0.0f,0.0f,0.0f));
		bill->setScale(vector3df(0.1f,0.1f,0.1f));
		bill->setMaterialTexture(0, driver->getTexture("system//images//parabens.jpg"));
		MyEventReceiver * receiver_s = &this->receiver;
		this->device->setEventReceiver(receiver_s);
		bool key_space_down;
		while(this->device->run()){
			this->driver->beginScene(true, true, SColor(255,100,101,140));
			key_space_down = receiver_s->IsKeyDown(KEY_SPACE);
			if(key_space_down == true) break;
			smgr->drawAll();
			this->guienv->drawAll();
			this->driver->endScene();
		}
	return right;
	}else if(right == false){
		this->smgr->clear();
		this->guienv->clear();
		smgr->addCameraSceneNode(0, vector3df(0,30,-40), vector3df(0,5,0));
		this->receiver.clearKeyDown(KEY_SPACE);
		scene::ISceneNode* bill;
		if(this->ratio == 1)bill = smgr->addBillboardSceneNode(0, core::dimension2d<f32>(100, 90));
		else if(this->ratio == 2)bill = smgr->addBillboardSceneNode(0, core::dimension2d<f32>(130, 90));
		else if(this->ratio == 3)bill = smgr->addBillboardSceneNode(0, core::dimension2d<f32>(120, 90));
		bill->setMaterialFlag(video::EMF_LIGHTING, false);
		bill->setPosition(vector3df(0.0f,0.0f,0.0f));
		bill->setScale(vector3df(0.1f,0.1f,0.1f));
		bill->setMaterialTexture(0, driver->getTexture("system//images//desculpe.jpg"));
		MyEventReceiver * receiver_s = &this->receiver;
		this->device->setEventReceiver(receiver_s);
		bool key_space_down;
		while(this->device->run()){
			this->driver->beginScene(true, true, SColor(255,100,101,140));
			key_space_down = receiver_s->IsKeyDown(KEY_SPACE);
			if(key_space_down == true) break;
			smgr->drawAll();
			this->guienv->drawAll();
			this->driver->endScene();
		}
		return right;
    }
  }
}

ISceneNode* Stage::LoadingScreenIn(){
	this->driver->beginScene(true, true, SColor(255,100,101,140));
	smgr->addCameraSceneNode(0, vector3df(0,30,-40), vector3df(0,5,0));
	this->guienv->getSkin()->setFont(this->guienv->getFont("system/font/athena_contagem/Athena.xml"));
	scene::ISceneNode* bill;
	if(this->ratio == 1)bill = smgr->addBillboardSceneNode(0, core::dimension2d<f32>(100, 90));
	else if(this->ratio == 2)bill = smgr->addBillboardSceneNode(0, core::dimension2d<f32>(130, 90));
	else if(this->ratio == 3)bill = smgr->addBillboardSceneNode(0, core::dimension2d<f32>(120, 90));
	bill->setMaterialFlag(video::EMF_LIGHTING, false);
	bill->setPosition(vector3df(0.0f,0.0f,0.0f));
	bill->setScale(vector3df(0.1f,0.1f,0.1f));
	bill->setMaterialTexture(0, driver->getTexture("system//images//loading//Carregando.jpg"));
	IGUIInOutFader* fader = this->guienv->addInOutFader();
	fader->fadeIn(100);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		if(teste == true)break;
	}
	return bill;
}

void Stage::LoadingScreenOut(MyEventReceiver* receiver_s, ISceneNode* bill_second){
	bill_second->setVisible(false);
	this->driver->beginScene(true, true, SColor(255,100,101,140));
	smgr->addCameraSceneNode(0, vector3df(0,30,-40), vector3df(0,5,0));
	this->guienv->getSkin()->setFont(this->guienv->getFont("system/font/athena_contagem/Athena.xml"));
	scene::ISceneNode* bill;
	if(this->ratio == 1)bill = smgr->addBillboardSceneNode(0, core::dimension2d<f32>(100, 90));
	else if(this->ratio == 2)bill = smgr->addBillboardSceneNode(0, core::dimension2d<f32>(130, 90));
	else if(this->ratio == 3)bill = smgr->addBillboardSceneNode(0, core::dimension2d<f32>(120, 90));
	bill->setMaterialFlag(video::EMF_LIGHTING, false);
	bill->setPosition(vector3df(0.0f,0.0f,0.0f));
	bill->setScale(vector3df(0.1f,0.1f,0.1f));
	bill->setMaterialTexture(0, driver->getTexture("system//images//loading//Carregado.jpg"));
	bool key_space_down;
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		key_space_down = receiver_s->IsKeyDown(KEY_SPACE);
		//if(key_space_down == true) break;
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		break;
	
	}
	bill->setVisible(false);
}

short Stage::GameCorrida(short dificuldade){
	// PREPARANDO O SCENARIO
	this->smgr->clear();
	this->guienv->clear();
	this->receiver.clearKeyDown(KEY_ESCAPE);
	this->device->setEventReceiver(&this->receiver);
	// INICIANDO A TELA DE LOADING!
	ISceneNode* bill_loading_screen = this->LoadingScreenIn();
	//

	// COLOCANDO EM MEMORIA TUDO QUE FOR NECESSARIO
	this->device->getCursorControl()->setVisible(false);
	//IAnimatedMesh* house_mesh = this->smgr->getMesh("system//models//house1.3ds");
	//house_mesh->setMaterialFlag(EMF_LIGHTING, this->lightning);
	//IAnimatedMeshSceneNode* house = this->smgr->addAnimatedMeshSceneNode( house_mesh );
	//driver->setTextureCreationFlag(video::ETCF_CREATE_MIP_MAPS, true);
	//house->setMaterialTexture(0,	driver->getTexture("system//images//1.jpg"));

	if(this->lightning == true){
		scene::ILightSceneNode* sol = this->smgr->addLightSceneNode(0, core::vector3df(0,0,00),
		video::SColorf(1.0f, 1.0f, 1.0f, 1.0f), 60000000);
		 SLight & l = sol->getLightData();
		 l.Type = ELT_DIRECTIONAL;
		 sol->enableCastShadow(true);
		 sol->setRotation(vector3df(90,-90,20));
		scene::ILightSceneNode* sol2 = this->smgr->addLightSceneNode(0, core::vector3df(0,0,00),
		video::SColorf(1.0f, 1.0f, 1.0f, 1.0f), 60000000);
		 SLight & l2 = sol2->getLightData();
		 l2.Type = ELT_DIRECTIONAL;
		 sol2->enableCastShadow(true);
		 sol2->setRotation(vector3df(90,-90,20));
		//scene::ISceneNode* sol2 = this->smgr->addLightSceneNode(0, core::vector3df(4429.1719,100,144.02632),
		//video::SColorf(1.0f, 1.0f, 1.0f, 0.1f), 800000.0f);
		//scene::ISceneNode* sol3 = this->smgr->addLightSceneNode(0, core::vector3df(5050.1719,1004,4.02632),
		//video::SColorf(1.0f, 1.0f, 1.0f, 0.1f), 800000.0f);
	}

	this->driver->setTextureCreationFlag(video::ETCF_CREATE_MIP_MAPS, false);
	
	// CARREGANDO E CONFIGURANDO CENARIO PISTA
	IAnimatedMesh* floor_mesh = this->smgr->getMesh("system//models//coliseum//pista.3DS");
	smgr->getMeshManipulator()->makePlanarTextureMapping(floor_mesh->getMesh(0), 0.004f);
	floor_mesh->setMaterialFlag(EMF_LIGHTING, this->lightning);
	floor_mesh->setMaterialFlag(EMF_NORMALIZE_NORMALS, this->lightning);
	this->smgr->getMeshManipulator()->makePlanarTextureMapping(
	floor_mesh->getMesh(0),0.011f);
	scene::ISceneNode* floor_scene = 0;
	IAnimatedMeshSceneNode* floor = this->smgr->addAnimatedMeshSceneNode( floor_mesh );
	floor->setMaterialTexture(0,this->driver->getTexture("system//images//texture//pista//pista.jpg"));
	//floor->setMaterialFlag(video::EMF_WIREFRAME, true);
	core::vector3df floorpos = floor->getAbsolutePosition();
	floorpos.Y -= 60;
	floor->setPosition(floorpos);
	floor->setRotation(vector3df(0,90,0));
	floor->setPosition(vector3df(3000,-60,100));
	floor->getMaterial(0).Shininess = 0.0f;
	// POSI��O DA SEGUNDA PISTA
	IAnimatedMeshSceneNode* floor2 = this->smgr->addAnimatedMeshSceneNode( floor_mesh );
	floor2->setMaterialTexture(0,this->driver->getTexture("system//images//texture//pista//pista.jpg"));
	core::vector3df floorpos2 = floor->getAbsolutePosition();
	floorpos2.Y -= 60;
	floor2->setPosition(floorpos);
	floor2->setRotation(vector3df(0,90,0));
	floor2->setPosition(vector3df(3000,-60,470));
	// FIM CARREGANDO E CONFIGURANDO CENARIO PISTA


	// CARREGANDO E CONFIGURANDO CENARIO PISTA PEDRAS 7171.6719
	IAnimatedMesh* floor_mesh_stones = this->smgr->getMesh("system//models//coliseum//pista_pedra.3DS");
	smgr->getMeshManipulator()->makePlanarTextureMapping(floor_mesh_stones->getMesh(0), 0.004f);
	floor_mesh_stones->setMaterialFlag(EMF_LIGHTING, this->lightning);
	floor_mesh_stones->setMaterialFlag(EMF_NORMALIZE_NORMALS, this->lightning);
	this->smgr->getMeshManipulator()->makePlanarTextureMapping(
	floor_mesh_stones->getMesh(0),0.021f);
	IAnimatedMeshSceneNode* floor_sones = this->smgr->addAnimatedMeshSceneNode( floor_mesh_stones );
	floor_sones->setMaterialTexture(0,this->driver->getTexture("system//images//texture//pista//pedra_pista.jpg"));
	//floor->setMaterialFlag(video::EMF_WIREFRAME, true);
	core::vector3df floorpos_stone = floor_sones->getAbsolutePosition();
	floorpos_stone.Y -= 60;
	floor_sones->setPosition(floorpos_stone);
	floor_sones->setRotation(vector3df(0,90,0));
	floor_sones->setPosition(vector3df(7164.6592,-60,100));
	if(lightning == true){
		floor_sones->addShadowVolumeSceneNode();
		smgr->setShadowColor(video::SColor(150,0,0,0));
		floor_sones->setScale(core::vector3df(1,1,1));
		floor_sones->setMaterialFlag(video::EMF_NORMALIZE_NORMALS, lightning);
	}
	// PISTA PEDRASC2
	IAnimatedMeshSceneNode* floor_sones2 = this->smgr->addAnimatedMeshSceneNode( floor_mesh_stones );
	floor_sones2->setMaterialTexture(0,this->driver->getTexture("system//images//texture//pista//pedra_pista.jpg"));
	//floor->setMaterialFlag(video::EMF_WIREFRAME, true);
	core::vector3df floorpos_stone2 = floor_sones->getAbsolutePosition();
	floorpos_stone2.Y -= 60;
	floor_sones2->setPosition(floorpos_stone);
	floor_sones2->setRotation(vector3df(0,90,0));
	floor_sones2->setPosition(vector3df(7164.6592,-60,470));
	if(lightning == true){
		floor_sones2->addShadowVolumeSceneNode();
		smgr->setShadowColor(video::SColor(150,0,0,0));
		floor_sones2->setScale(core::vector3df(1,1,1));
		floor_sones2->setMaterialFlag(video::EMF_NORMALIZE_NORMALS, lightning);
	}
	// FIM CARREGANDO E CONFIGURANDO CENARIO PISTA PEDRAS
	// CARREGANDO PISTA PEDRA INICIO
		IAnimatedMeshSceneNode* floor_sones3 = this->smgr->addAnimatedMeshSceneNode( floor_mesh_stones );
	floor_sones3->setMaterialTexture(0,this->driver->getTexture("system//images//texture//pista//pedra_pista.jpg"));
	//floor->setMaterialFlag(video::EMF_WIREFRAME, true);
	core::vector3df floorpos_stone3 = floor_sones->getAbsolutePosition();
	floorpos_stone3.Y -= 60;
	floor_sones3->setPosition(floorpos_stone);
	floor_sones3->setRotation(vector3df(0,90,0));
	floor_sones3->setPosition(vector3df(-786.32806,-60,470));
	if(lightning == true){
		floor_sones3->addShadowVolumeSceneNode();
		smgr->setShadowColor(video::SColor(150,0,0,0));
		floor_sones3->setScale(core::vector3df(1,1,1));
		floor_sones3->setMaterialFlag(video::EMF_NORMALIZE_NORMALS, lightning);
	}
	IAnimatedMeshSceneNode* floor_sones4 = this->smgr->addAnimatedMeshSceneNode( floor_mesh_stones );
	floor_sones4->setMaterialTexture(0,this->driver->getTexture("system//images//texture//pista//pedra_pista.jpg"));
	//floor->setMaterialFlag(video::EMF_WIREFRAME, true);
	core::vector3df floorpos_stone4 = floor_sones->getAbsolutePosition();
	floorpos_stone4.Y -= 60;
	floor_sones4->setPosition(floorpos_stone);
	floor_sones4->setRotation(vector3df(0,90,0));
	floor_sones4->setPosition(vector3df(-786.32806,-60,100));
	if(lightning == true){
		floor_sones4->addShadowVolumeSceneNode();
		smgr->setShadowColor(video::SColor(150,0,0,0));
		floor_sones4->setScale(core::vector3df(1,1,1));
		floor_sones4->setMaterialFlag(video::EMF_NORMALIZE_NORMALS, lightning);
	}
	// FIM CARREGaNDO PISTA PEDRA FINAL

	// CARREGANDO E CONFIGURANDO CENARIO MARCADOR
	IAnimatedMesh* mesh_marker = this->smgr->getMesh("system//models//coliseum//pista_marcador.3DS");
	smgr->getMeshManipulator()->makePlanarTextureMapping(mesh_marker->getMesh(0), 0.004f);
	mesh_marker->setMaterialFlag(EMF_LIGHTING, this->lightning);
	mesh_marker->setMaterialFlag(EMF_NORMALIZE_NORMALS, this->lightning);
	this->smgr->getMeshManipulator()->makePlanarTextureMapping(
	mesh_marker->getMesh(0),0.021f);
	IAnimatedMeshSceneNode* marker_floor = this->smgr->addAnimatedMeshSceneNode( mesh_marker );
	marker_floor->setMaterialTexture(0,this->driver->getTexture("system//images//texture//pista//marcador.jpg"));
	//floor->setMaterialFlag(video::EMF_WIREFRAME, true);
	marker_floor->setPosition(floorpos_stone);
	marker_floor->setRotation(vector3df(0,90,0));
	marker_floor->setPosition(vector3df(7164.6592,-60,190.94026));
	
	IAnimatedMeshSceneNode* marker_floor2 = this->smgr->addAnimatedMeshSceneNode( mesh_marker );
	marker_floor2->setMaterialTexture(0,this->driver->getTexture("system//images//texture//pista//marcador.jpg"));
	//floor->setMaterialFlag(video::EMF_WIREFRAME, true);
	marker_floor2->setPosition(floorpos_stone);
	marker_floor2->setRotation(vector3df(0,90,0));
	marker_floor2->setPosition(vector3df(7164.6592,-60,8.4390116));

	IAnimatedMeshSceneNode* marker_floor3 = this->smgr->addAnimatedMeshSceneNode( mesh_marker );
	marker_floor3->setMaterialTexture(0,this->driver->getTexture("system//images//texture//pista//marcador.jpg"));
	//floor->setMaterialFlag(video::EMF_WIREFRAME, true);
	marker_floor3->setPosition(floorpos_stone);
	marker_floor3->setRotation(vector3df(0,90,0));
	marker_floor3->setPosition(vector3df(7164.6592,-60,380.54938));

	IAnimatedMeshSceneNode* marker_floor4 = this->smgr->addAnimatedMeshSceneNode( mesh_marker );
	marker_floor4->setMaterialTexture(0,this->driver->getTexture("system//images//texture//pista//marcador.jpg"));
	//floor->setMaterialFlag(video::EMF_WIREFRAME, true);
	marker_floor4->setPosition(floorpos_stone);
	marker_floor4->setRotation(vector3df(0,90,0));
	marker_floor4->setPosition(vector3df(7164.6592,-60,560.06445));

	// FIM CARREGANDO E CONFIGURANDO CENARIO MARCADOR
	float pos = -786.32806;
	for(int i = 0; i <= 190; i++){
		IAnimatedMesh* mesh_marker = this->smgr->getMesh("system//models//coliseum//pista_marcador.3DS");
		smgr->getMeshManipulator()->makePlanarTextureMapping(mesh_marker->getMesh(0), 0.004f);
		mesh_marker->setMaterialFlag(EMF_LIGHTING, this->lightning);
		mesh_marker->setMaterialFlag(EMF_NORMALIZE_NORMALS, this->lightning);
		this->smgr->getMeshManipulator()->makePlanarTextureMapping(
		mesh_marker->getMesh(0),0.021f);
		IAnimatedMeshSceneNode* marker_floor = this->smgr->addAnimatedMeshSceneNode( mesh_marker );
		marker_floor->setMaterialTexture(0,this->driver->getTexture("system//images//texture//pista//marcador.jpg"));
		//floor->setMaterialFlag(video::EMF_WIREFRAME, true);
		marker_floor->setPosition(floorpos_stone);
		marker_floor->setRotation(vector3df(0,90,0));
		marker_floor->setPosition(vector3df(pos,-90,-85.94026));
		pos = pos + 42;
	}

	/*
		IAnimatedMesh* torcida = this->smgr->getMesh("system//models//coliseum//plateia_D.md2");
		
	     IAnimatedMeshSceneNode* torcida_node = smgr->addAnimatedMeshSceneNode( torcida );
		 torcida->setMaterialFlag(EMF_LIGHTING, false);
		 torcida_node->getMaterial(0).SpecularColor.set(0,0,0,0);
		 torcida_node->getMaterial(0).EmissiveColor.set(255,255,255,255);
		 torcida_node->setMaterialTexture(0,this->driver->getTexture("system//images//texture//pista//publico.png"));
		 //this->smgr->getMeshManipulator()->makePlanarTextureMapping(
			// torcida->getMesh(0),0.021f);
		 torcida_node->setMaterialFlag(EMF_NORMALIZE_NORMALS, true);
		 torcida_node->setMaterialFlag(EMF_COLOR_MATERIAL, true);
		 torcida_node->setMaterialType(EMT_TRANSPARENT_ALPHA_CHANNEL);
		 torcida_node->setMaterialType(EMT_TRANSPARENT_ADD_COLOR);
		 torcida_node->setMaterialType(EMT_TRANSPARENT_ALPHA_CHANNEL_REF);
		 torcida_node->setPosition(floorpos_stone);
		 torcida_node->setRotation(vector3df(0, 90,0));
		 torcida_node->setPosition(vector3df(pos+600,-60,45.94026));
		 //torcida_node->setPosition(vector3df(pos+550,-10, 400.94026));
		 pos = pos + 42;
    
	*/

	// CARREGANDO GRAMA DO COLISEO
		IAnimatedMeshSceneNode* grama_node;
		IAnimatedMesh* grama;
		this->smgr = device->getSceneManager();
		grama = smgr->getMesh("system//models//coliseum//grama.md2");
		grama_node = smgr->addAnimatedMeshSceneNode( grama );
		grama_node->setMaterialFlag(EMF_LIGHTING, lightning);
		grama_node->setMaterialTexture( 0, driver->getTexture("system//images//texture//pista//grama.jpg"));
		grama_node->setRotation(vector3df(0,90,0));
		grama_node->setPosition(vector3df(3010,-60,280));
		grama_node->setScale(vector3df(2,2,1));
	// FIM CARREGaNDO GRAMA DO COLISEO

	// CARREGANDO PANORAMA
	driver->setTextureCreationFlag(video::ETCF_CREATE_MIP_MAPS, false);
	scene::ISceneNode* skyboxNode;
	skyboxNode = this->smgr->addSkyBoxSceneNode(
		driver->getTexture("system/images/panorama/panorama_up.jpg"),
		driver->getTexture("system/images/panorama/panorama_dn.jpg"),
		driver->getTexture("system/images/panorama/panorama_lf.jpg"),
		driver->getTexture("system/images/panorama/panorama_rt.jpg"),
		driver->getTexture("system/images/panorama/panorama_ft.jpg"),
		driver->getTexture("system/images/panorama/panorama_bk.jpg"));
		//driver->getTexture("system/images/panorama/p.jpg"),
		//driver->getTexture("system/images/panorama/p.jpg"),
		//driver->getTexture("system/images/panorama/p.jpg"),
		//driver->getTexture("system/images/panorama/p.jpg"),
		//driver->getTexture("system/images/panorama/p.jpg"),
	//driver->getTexture("system/images/panorama/p.jpg"));

	driver->setTextureCreationFlag(video::ETCF_CREATE_MIP_MAPS, true);
	//FIM CARREGANDO PANORAMA
	  
	int numbers[] = {-1, -1, -1};
	  int cachenumber = -2;
	  int index = 0;
	  while(numbers[2] == -1){
			cachenumber = rand()%11;
			if(numbers[0] != cachenumber && numbers[1] != cachenumber && numbers[2] != cachenumber){
				numbers[index] = cachenumber; 
				index = index + 1;
			}
	  }
	  char * texture_path_npc1 = "system//images//texture//menina2vermelho.jpg";
	  char * texture_path_npc2 = "system//images//texture//meninaazul.jpg";
	  char * texture_path_npc3 = "system//images//texture//menina3bege.jpg";
	  if(numbers[0] == 0)texture_path_npc1 = "system//images//texture//menina.jpg";
	  else if (numbers[0] == 1)texture_path_npc1 = "system//images//texture//menina2.jpg";
	  else if (numbers[0] == 2)texture_path_npc1 = "system//images//texture//menina3.jpg";
	  else if (numbers[0] == 3)texture_path_npc1 = "system//images//texture//menina2azul.jpg";
	  else if (numbers[0] == 4)texture_path_npc1 = "system//images//texture//menina2bege.jpg";
	  else if (numbers[0] == 5)texture_path_npc1 = "system//images//texture//menina2vermelho.jpg";
	  else if (numbers[0] == 6)texture_path_npc1 = "system//images//texture//menina3azul.jpg";
	  else if (numbers[0] == 7)texture_path_npc1 = "system//images//texture//menina3bege.jpg";
	  else if (numbers[0] == 8)texture_path_npc1 = "system//images//texture//menina3vermelho.jpg";
	  else if (numbers[0] == 9)texture_path_npc1 = "system//images//texture//meninaazul.jpg";
	  else if (numbers[0] == 10)texture_path_npc1 = "system//images//texture//meninabege.jpg";
	  else if (numbers[0] == 11)texture_path_npc1 = "system//images//texture//meninavermelho.jpg";
	  if(numbers[1] == 0)texture_path_npc2 = "system//images//texture//menina.jpg";
	  else if (numbers[1] == 1)texture_path_npc2 = "system//images//texture//menina2.jpg";
	  else if (numbers[1] == 2) texture_path_npc2 = "system//images//texture//menina3.jpg";
	  else if (numbers[1] == 3) texture_path_npc2 = "system//images//texture//menina2azul.jpg";
	  else if (numbers[1] == 4) texture_path_npc2 = "system//images//texture//menina2bege.jpg";
	  else if (numbers[1] == 5) texture_path_npc2 = "system//images//texture//menina2vermelho.jpg";
	  else if (numbers[1] == 6) texture_path_npc2 = "system//images//texture//menina3azul.jpg";
	  else if (numbers[1] == 7) texture_path_npc2 = "system//images//texture//menina3bege.jpg";
	  else if (numbers[1] == 8) texture_path_npc2 = "system//images//texture//menina3vermelho.jpg";
	  else if (numbers[1] == 9) texture_path_npc2 = "system//images//texture//meninaazul.jpg";
	  else if (numbers[1] == 10) texture_path_npc2 = "system//images//texture//meninabege.jpg";
	  else if (numbers[1] == 11) texture_path_npc2 = "system//images//texture//meninavermelho.jpg";
	  if(numbers[2] == 0) texture_path_npc3 = "system//images//texture//menina.jpg";
	  else if (numbers[2] == 1) texture_path_npc3 = "system//images//texture//menina2.jpg";
	  else if (numbers[2] == 2) texture_path_npc3 = "system//images//texture//menina3.jpg";
	  else if (numbers[2] == 3) texture_path_npc3 = "system//images//texture//menina2azul.jpg";
	  else if (numbers[2] == 4) texture_path_npc3 = "system//images//texture//menina2bege.jpg";
	  else if (numbers[2] == 5) texture_path_npc3 = "system//images//texture//menina2vermelho.jpg";
	  else if (numbers[2] == 6) texture_path_npc3 = "system//images//texture//menina3azul.jpg";
	  else if (numbers[2] == 7) texture_path_npc3 = "system//images//texture//menina3bege.jpg";
	  else if (numbers[2] == 8) texture_path_npc3 = "system//images//texture//menina3vermelho.jpg";
	  else if (numbers[2] == 9) texture_path_npc3 = "system//images//texture//meninaazul.jpg";
	  else if (numbers[2] == 10) texture_path_npc3 = "system//images//texture//meninabege.jpg";
	  else if (numbers[2] == 11) texture_path_npc3 = "system//images//texture//meninavermelho.jpg";
	// CARREGANDO NPC
	NPC *cpu1 = new NPC(this->device, this->driver,"system//models//menina//menina_I.md2", texture_path_npc1,false);

	cpu1->setPosition(0,-100,100);
	cpu1->rotateNPC(0.000f,-90.000f,0.000f);

	NPC *cpu2 = new NPC(this->device, this->driver,"system//models//menina//menina_I.md2", texture_path_npc2,false);

	cpu2->setPosition(0,-100,140);
	cpu2->rotateNPC(0.000f,-90.000f,0.000f);

	NPC *cpu3 = new NPC(this->device, this->driver,"system//models//menina//menina_I.md2", texture_path_npc3,false);

	cpu3->setPosition(0,-100,180);
	cpu3->rotateNPC(0.000f,-90.000f,0.000f);
	// FIM CARREGAMENTO NPCs

	// INICIANDO PLAYER1 
	Main_Char* player1 = new Main_Char(this->device, this->driver, &this->receiver, "system//models//menina//menina_I.md2", "system//images//texture//menina.jpg",this->window_weight, this->window_height, this->lightning);
	if(this->burningTest == true)player1->setHUDToBurningVideo();
	// FIM INICIANDO PLAYER1

	// CONFIGURANDO FONTES
    this->guienv->getSkin()->setFont(this->guienv->getFont("system/font/athena_contagem/Athena.xml"));
	//
	// CARREGANDO IMAGEM PARA MOSTRAR POSICAO NA HUD
	IGUIImage * position_small = device->getGUIEnvironment()->addImage(driver->getTexture("system//images//ingame//position//player1//1.png"),core::position2d<s32>(80,100));
	// FIM CARREGANDO IMAGEM PARA MOSTRA POSICAO NA HUD

	// CARREGANDO CONTADOR PARA DAR PARTIDA INICIAL
	IGUIImage * counter = device->getGUIEnvironment()->addImage(driver->getTexture("system//images//ingame//signal//3.png"),core::position2d<s32>(this->window_weight*0.4,this->window_height*0.4));
	IGUIImage * vai_image;
	if(this->ratio == 1) vai_image = device->getGUIEnvironment()->addImage(driver->getTexture("system//images//ingame//signal//vai.png"),core::position2d<s32>(this->window_weight*0.2,this->window_height*0.4));
	else if(this->ratio == 2) vai_image = device->getGUIEnvironment()->addImage(driver->getTexture("system//images//ingame//signal//vai.png"),core::position2d<s32>(this->window_weight*0.25,this->window_height*0.4));
	else if(this->ratio == 3) vai_image = device->getGUIEnvironment()->addImage(driver->getTexture("system//images//ingame//signal//vai.png"),core::position2d<s32>(this->window_weight*0.25,this->window_height*0.4));
	//FIM CARREGANDO CONTADOR PARA DAR PARTIDA INICIAL
	
	// CARREGANDO IMAGEM PARA MOSTRAR POSICAO FINAL
	IGUIImage * position_final_image;
	if(this->ratio == 1) position_final_image = device->getGUIEnvironment()->addImage(driver->getTexture("system//images//ingame//position//finalPosition//1.png"),core::position2d<s32>(this->window_weight*0.2,this->window_height*0.1));
	else if(this->ratio == 2) position_final_image = device->getGUIEnvironment()->addImage(driver->getTexture("system//images//ingame//position//finalPosition//1.png"),core::position2d<s32>(this->window_weight*0.3,this->window_height*0.1));
	else if(this->ratio == 3) position_final_image = device->getGUIEnvironment()->addImage(driver->getTexture("system//images//ingame//position//finalPosition//1.png"),core::position2d<s32>(this->window_weight*0.3,this->window_height*0.1));
	position_final_image->setVisible(false);
	IGUIImage * continue_space;
	if(this->ratio == 1) continue_space = device->getGUIEnvironment()->addImage(driver->getTexture("system//images//ingame//continue//espaco_continuar.png"),core::position2d<s32>(-200,-300));
	else if(this->ratio == 2) continue_space = device->getGUIEnvironment()->addImage(driver->getTexture("system//images//ingame//continue//espaco_continuar.png"),core::position2d<s32>(-50,-300));
	else if(this->ratio == 3) continue_space = device->getGUIEnvironment()->addImage(driver->getTexture("system//images//ingame//continue//espaco_continuar.png"),core::position2d<s32>(-0,-300));
	//FIM CARREGANDO IMAGEM PARA MOSTRAR POSICAO FINAL

	// CARREGANDO CASO COLISAO
	IGUIImage * ultrapassou_raia;
	if(this->ratio == 1) ultrapassou_raia = device->getGUIEnvironment()->addImage(driver->getTexture("system//images//ingame//messages//saiu_raia_03.png"),core::position2d<s32>(this->window_weight*0.3,this->window_height*0.4));
	else if(this->ratio == 2) ultrapassou_raia = device->getGUIEnvironment()->addImage(driver->getTexture("system//images//ingame//messages//saiu_raia_03.png"),core::position2d<s32>(this->window_weight*0.3,this->window_height*0.4));
	else if(this->ratio == 3) ultrapassou_raia = device->getGUIEnvironment()->addImage(driver->getTexture("system//images//ingame//messages//saiu_raia_03.png"),core::position2d<s32>(this->window_weight*0.3,this->window_height*0.4));
	//FIM  CARREGANDO CASO COLISAO

	// CARREGANDO CASO QUEIMAR LARGADA
	IGUIImage * queimou_largada_imagem;
	if(this->ratio == 1) queimou_largada_imagem = device->getGUIEnvironment()->addImage(driver->getTexture("system//images//ingame//messages//queimou_largada_03.png"),core::position2d<s32>(this->window_weight*0.3,this->window_height*0.4));
	else if(this->ratio == 2) queimou_largada_imagem = device->getGUIEnvironment()->addImage(driver->getTexture("system//images//ingame//messages//queimou_largada_03.png"),core::position2d<s32>(this->window_weight*0.3,this->window_height*0.4));
	else if(this->ratio == 3) queimou_largada_imagem = device->getGUIEnvironment()->addImage(driver->getTexture("system//images//ingame//messages//queimou_largada_03.png"),core::position2d<s32>(this->window_weight*0.3,this->window_height*0.4));
	//FIM  CARREGANDO CASO QUEIMAR LARGADA

	// CARREGANDO IMAGEM DE TEMPO PARA HUD
	IGUIImage * time = device->getGUIEnvironment()->addImage(driver->getTexture("system//images//ingame//time//player1//time_p1.png"),core::position2d<s32>(10,0));
	//FIM CARREGANDO IMAGEM DE TEMPO PARA HUD
	
	
	// CONFIGURANDO PLAYER1 
	player1->setPosition(-820.32806f, -61.000f, 144.02632);
	//player1->setPosition(7164.6592f,0.000f,23.626f); //--- PARA O FIM DA PISTA
	//player1->setScale(0.6,0.6,0.6);
	player1->setScale(1,1,1);
	player1->rotateNPC(0.000f,90.000f,0.000f);
	player1->setCameraControl(false);
	player1->moveCameraControl(this->device);
	//FIM CONFIGURANDO PLAYER1 

	// CONFIGURANDO NPCS
	bool cacheNPC = false;
	bool breakcount = false;

	cpu1->setPosition(-820.32806f,-61.000f, -36.028538f);
	cpu1->rotateNPC(0.000f,-90.000f,0.000f);
	cpu2->setPosition(-820.32806f,-61.000f, 333.37823f);
	cpu2->rotateNPC(0.000f,-90.000f,0.000f);
	cpu3->setPosition(-820.32806f,-61.000f, 514.06787);
	cpu3->rotateNPC(0.000f,-90.000f,0.000f);
	//FIM CONFIGURANDO NPCS

	// INICIANDO INTELIGENCIA ARTIFICIAL
	IA* inteligenceArtificial_npc1 = new IA(dificuldade);
	IA* inteligenceArtificial_npc2 = new IA(dificuldade);
	IA* inteligenceArtificial_npc3 = new IA(dificuldade);
	//FIM INICIANDO INTELIGENCIA ARTIFICIAL

	// INICIANDO TEXTO PARA CONTAR O TEMPO
	IGUIStaticText* textTEMPO = guienv->addStaticText(L"",rect<s32>(0,0,700,256), false,false);
	//textTEMPO->setOverrideFont
	SColor test = textTEMPO->getOverrideColor();
	test.set(255,255,255,255); // Set COLOR (Alpha, Red, Blue, Green)
	textTEMPO->setOverrideColor(test);
    // textTEMPO->setRelativePosition(rect<s32>((this->window_weight*0.5),0,(this->window_weight)*1.5,256));
	textTEMPO->setRelativePosition(rect<s32>(100,0,(this->window_weight),256));
	//FIM INICIANDO TEXTO PARA CONTAR O TEMPO

	// CONFIGURANDO VARIAVEIS PARA COMPARAR OS TEMPOS E TEMPO QUE UMA IMAGEM FICA NA TELA
	wchar_t* clock;
	int i = 0;
	//FIM CONFIGURANDO VARIAVEIS PARA COMPARAR OS TEMPOS E TEMPO QUE UMA IMAGEM FICA NA TELA

	// CONFIGURANDO VELOCIDADE MINIMA DADA PELA I.A.
	float velocidade_npc_1 = inteligenceArtificial_npc1->getVelocidadeMin();
	float velocidade_npc_2 = inteligenceArtificial_npc2->getVelocidadeMin();
	float velocidade_npc_3 = inteligenceArtificial_npc3->getVelocidadeMin();
	//FIM CONFIGURANDO VELOCIDADE MINIMA DADA PELA I.A.

	// INICIANDO VARIAVEIS QUE IR�O RETORNAR POSICAO DOS PERSONAGENS
	short colocacao_player;
	short colocacao_npc;
	//FIM  INICIANDO VARIAVEIS QUE IR�O RETORNAR POSICAO DOS PERSONAGENS


	//FIM DE COLOCAR EM MEMORIA TUDO QUE FOR NECESSARIO
	
	// DESABILITANDO VISIBILIDADE DOS ITENS DO CENARIO PARA MOSTRAR APENAS A TELA DE LOADING FOI CONCLUIDA COM SUCESSO
	floor->setVisible(false);
	skyboxNode->setVisible(false);
	player1->getNode()->setVisible(false);
	cpu1->getNode()->setVisible(false);
	cpu2->getNode()->setVisible(false);
	cpu3->getNode()->setVisible(false);
	position_small->setVisible(false);
	vai_image->setVisible(false);
	counter->setVisible(false);
	time->setVisible(false);
	floor_sones->setVisible(false);
	floor_sones2->setVisible(false);
	marker_floor->setVisible(false);
	continue_space->setVisible(false);
	continue_space->setVisible(false);
	ultrapassou_raia->setVisible(false);
	queimou_largada_imagem->setVisible(false);
	// FIM DESABILITANDO VISIBILIDADE DOS ITENS DO CENARIO PARA MOSTRAR APENAS A TELA DE LOADING FOI CONCLUIDA COM SUCESSO

	// MOSTRANDO A TELA DE LOADING FINAL
	this->LoadingScreenOut(&this->receiver,bill_loading_screen);
	//FIM MOSTRANDO A TELA DE LOADING FINAL

	// MOSTRANDO TODOS OS ITENS NECESSARIOS NA TELA
	floor->setVisible(true);
	floor2->setVisible(true);
	skyboxNode->setVisible(true);
	player1->getNode()->setVisible(true);
	player1->showInterface(true);
	cpu1->getNode()->setVisible(true);
	cpu2->getNode()->setVisible(true);
	cpu3->getNode()->setVisible(true);
	position_small->setVisible(true);
	counter->setVisible(true);
	time->setVisible(true);
	floor_sones->setVisible(true);
	floor_sones2->setVisible(true);
	marker_floor->setVisible(true);
	// FIM MOSTRANDO TODOS OS ITENS NECESSARIOS NA TELA

	// CONFIGURANDO CAMERA PARA SER MOSTRADA EM 3 PESSOA APOS TELA DE LOADING
	this->smgr->addCameraSceneNode(0, vector3df(0,30,-40), vector3df(0,5,0));
    scene::ICameraSceneNode* camera = this->device->getSceneManager()->getActiveCamera();
	player1->setCameraControl(false);
	player1->moveCameraControl(this->device);
	//FIM CONFIGURANDO CAMERA PARA SER MOSTRADA EM 3 PESSOA APOS TELA DE LOADING

	// CONFIGURANDO TEMPO
	irrClock* counting = new irrClock(this->device);
	counting->clearTime();
	counting->setTime(1); // IN SECONDS
	counting->startTime();
	//FIM CONFIGURANDO TEMPO

	// CONFIGURANDO FORMAS DE PENSAMENTO DA IA
	if(dificuldade == 0){
		inteligenceArtificial_npc1->setThinking(1);
		inteligenceArtificial_npc2->setThinking(1);
		inteligenceArtificial_npc3->setThinking(1);
	}else if(dificuldade == 1){
		inteligenceArtificial_npc1->setThinking(1);
		inteligenceArtificial_npc2->setThinking(2);
		inteligenceArtificial_npc3->setThinking(3);
	}else{
		inteligenceArtificial_npc1->setThinking(3);
		inteligenceArtificial_npc2->setThinking(2);
		inteligenceArtificial_npc3->setThinking(3);
	}

	// SETTING MODELS SPEED
	cpu1->setModelSpeed(inteligenceArtificial_npc1->getCurrentSpeed());
	cpu2->setModelSpeed(inteligenceArtificial_npc2->getCurrentSpeed());
	cpu3->setModelSpeed(inteligenceArtificial_npc3->getCurrentSpeed());

	//FIM CONFIGURANDO FORMAS DE PENSAMENTO DA IA

	// CONFIGURANDO FLAG AVISANDO SE O JOGADOR SAIU DO JOGO COM ESC
	bool quit = false;
	//FIM CONFIGURANDO FLAG AVISANDO SE O JOGADOR SAIU DO JOGO COM ESC

	// CONFIGURANDO COLISAO
	Colisao* p_collide = new Colisao();
	int ending_options = 0;

	// FIM CONFIGURANDO COLISAO

	// CONFIGURANDO INICIAIS CASO QUEIMAR LARGADA
	MyEventReceiver * receiver = &this->receiver;
	bool key_a_down;
	bool key_d_down;
	bool queimou_largada = false;
	bool alternando = false;
	short count_passos = 0; // Conta quantos passos para indicar queimou largada
	//FIM CONFIGURANDO INICIAIS CASO QUEIMAR LARGADA

	// CONFIGURANDO VARIAVEIS PARA TROCA DE ANIMA��O
	short change_animation = 0;
	short tam_frame;
	// FIM CONFIGURANDO VARIAVEIS PARA TROCA DE ANIMA��O

	// PARA QUALQUER SOM QUE ESTEJA TOCANDO
	this->intro_music->stop();
	// FIM PARA QUALQUER SOM QUE ESTEJA TOCANDO
	int qntd_frames;
	int current_animation_speed;
	bool once_largada = false;
	bool once_set_largando = false;
	bool once_animation = false;
	// vars para iniciar largada

	// SONS DA TORCIDA
	this->torcida = sound_engine->play3D("system//sounds//APLAUSOS_3_LOOP.mp3",
	                              vec3df(0,0,0), false, false, true);
	this->torcida->setIsLooped(true);
	this->musica_corrida = sound_engine->play3D("system//sounds//CORRIDA_LOOP.mp3",
	                              vec3df(0,0,0), false, false, true);
	this->musica_corrida->setIsLooped(true);
	this->musica_derrota = sound_engine->play3D("system//sounds//DERROTA.mp3",
	                              vec3df(0,0,0), false, false, true);
	this->musica_derrota->setIsLooped(false);
	this->musica_vitoria = sound_engine->play3D("system//sounds//VITORIA.mp3",
	                              vec3df(0,0,0), false, false, true);
	this->musica_vitoria->setIsLooped(false);

	this->musica_vitoria->setIsPaused(true);
	this->musica_derrota->setIsPaused(true);
	//this->torcida->setIsPaused(true);
	bool once_play_torcida = false;
	bool once_paused = false;
	bool entrada = false;
	bool play_once_steps = false;
	bool play_once_steps_2 = false;
	this->steps_engine->setSoundVolume(0.3);
	//

	// fim de vars para iniciar largada
	// INICIANDO LOOP DO JOGO
    while(this->device->run())
    {
			
			if(entrada == false){
				this->torcida_gritando = sound_engine->play3D("system//sounds//APLAUSOS_2.mp3",
	                             vec3df(0,0,0), false, false, true);
				this->torcida_gritando->setIsLooped(false);

				bool key_cut_run = false;
				player1->setMesh("system//models//menina//menina_I.md2");
				player1->setCameraControl(false);
				counter->setVisible(false);
				int dispause = 0;
				while(true){
					this->driver->beginScene(true, true, SColor(255,100,101,140));
					player1->turnCameraPlayer(this->device,0,75,0,-300,1);
					dispause++;
					this->smgr->drawAll();
					this->guienv->drawAll();
					this->driver->endScene();
					if(dispause == 350){
						entrada = true;
						break;
					}

				}
			player1->rotateNPC(0,-90,0);

			player1->clearCameraControl(this->device);
			//player1->moveCameraControl(this->device);
			player1->setCameraControl(true);
			counting->clearTime();
			counter->setVisible(true);
			}
			this->driver->beginScene(true, true, SColor(255,100,101,140));
			if(breakcount == false){
				key_a_down = receiver->IsKeyDown(KEY_KEY_A);
				key_d_down = receiver->IsKeyDown(KEY_KEY_D);
				if(key_a_down == true && key_d_down == false){
					if(alternando == false){
						count_passos++;
						alternando = true;
					}
				}
				if(key_a_down == false && key_d_down == true){
					if(alternando == true){
						count_passos++;
						alternando = false;
					}
				}
				if(count_passos >= 4) breakcount = true;
				if(breakcount == true){
					counter->setVisible(false);
					counting->stopTime();
					counting->clearTime();
					player1->setCameraControl(true);
					queimou_largada = true;
					player1->setMesh("system//models//menina//menina_C.md2");
					receiver->clearKeyDown(KEY_KEY_A);
					receiver->clearKeyDown(KEY_KEY_D);
				}
			}
			if(breakcount == false){
				//player1->setAnimationMD2("idle");
				if(once_set_largando == false){
					player1->getNode()->setLoopMode(false);
					player1->setMesh("system//models//menina//menina_LARGANDO.md2");
					qntd_frames = player1->getNode()->getEndFrame();
					once_set_largando = true;
				}
				if(once_animation == false){
					once_animation = true; 
					current_animation_speed = player1->getNode()->getAnimationSpeed();
				}
				if((qntd_frames) <= player1->getNode()->getFrameNr()){
					player1->getNode()->setAnimationSpeed(0);
					player1->getNode()->setCurrentFrame(player1->getNode()->getEndFrame()-1);
				}
				if(!wcscmp(counting->Count(),L"   4") == 0){
					if(wcscmp(counting->Count(),L"   3") == 0){

						
						counter->setImage(driver->getTexture("system//images//ingame//signal//1.png"));	
					}
					if(wcscmp(counting->Count(),L"   2") == 0){
						counter->setImage(driver->getTexture("system//images//ingame//signal//2.png"));

					}
					if(wcscmp(counting->Count(),L"   1") == 0){
						counter->setImage(driver->getTexture("system//images//ingame//signal//3.png"));
					}
					
				}else{
					counter->setVisible(false);
					vai_image->setVisible(true);
					counting->stopTime();
					counting->clearTime();
					player1->setCameraControl(true);
					//player1->setMesh("system//models//menina//menina_C.md2");
					player1->getNode()->setLoopMode(false),player1->setMesh("system//models//menina//menina_INICIANDO.md2");
					this->torcida_gritando = sound_engine->play3D("system//sounds//APLAUSOS_1.mp3",
	                              vec3df(0,0,0), false, false, true);
					this->torcida_gritando->setIsLooped(false);

					player1->getNode()->setAnimationSpeed(current_animation_speed);
					qntd_frames = player1->getNode()->getEndFrame();
					breakcount = true;
				}
			}else{
				bool bool_paused_game = this->receiver.IsKeyUp(KEY_ESCAPE);
			    if(bool_paused_game == true){
					quit = true;
					break;
			    }
				if((qntd_frames) <= player1->getNode()->getFrameNr() && once_largada == false){
					player1->getNode()->setLoopMode(true); 
					player1->setMesh("system//models//menina//menina_C.md2");
					cpu1->setMesh("system//models//menina//menina_C.md2");
					cpu2->setMesh("system//models//menina//menina_C.md2");
					cpu3->setMesh("system//models//menina//menina_C.md2");
					once_largada = true;
				}
					clock = counting->getClockInString();
			
					if(i <= 100){
						i++;
						if(i >= 100)vai_image->setVisible(false);
						if(i >= 100 && queimou_largada == true){
							position_small->setVisible(false);
							continue_space->setVisible(true);
							vai_image->setVisible(false);
							queimou_largada_imagem->setVisible(true);
							ending_options = 3;
							break;
						}
					}
					
						textTEMPO->setText(clock);
					/*
					GETTING POSITION (PLACE IN GAME)
					*/
					colocacao_player = this->getPosition(player1,cpu1,cpu2,cpu3);
					if(colocacao_player == 1)position_small->setImage(driver->getTexture("system//images//ingame//position//player1//1.png"));
					else if(colocacao_player == 2)position_small->setImage(driver->getTexture("system//images//ingame//position//player1//2.png"));
					else if(colocacao_player == 3)position_small->setImage(driver->getTexture("system//images//ingame//position//player1//3.png"));
					else if(colocacao_player == 4)position_small->setImage(driver->getTexture("system//images//ingame//position//player1//4.png"));

					/*
					PLAYER PART
					*/
					player1->Jogabilidade(camera);
				 
					player1->moveCameraControl(this->device);
					
					if(p_collide->colisao(player1->getPosition().Z,player1->getPosition().X)){
						ending_options = 2;
						break;
					}
					// Steps Sound
					if(player1->getNode()->getFrameNr() >= 25 && player1->getNode()->getStartFrame() <= 35) {
						if(play_once_steps == false){
							this->passo_esquerdo = steps_engine->play3D("system//sounds//PASSO2.mp3",
								vec3df(0,0,0), false, false, true);
							this->passo_esquerdo->setIsLooped(false);
							
							play_once_steps = true;
						}
					}else{
						play_once_steps = false;
					}

					if(player1->getNode()->getFrameNr() >= 55 && player1->getNode()->getStartFrame() <= 75) {
						if(play_once_steps_2 == false){
								this->passo_direito = steps_engine->play3D("system//sounds//PASSO3.mp3",
								vec3df(0,0,0), false, false, true);
							this->passo_direito->setIsLooped(false);
							play_once_steps_2 = true;
						}
					}else{
						play_once_steps_2 = false;
					}
			
					/*
					NPC 1 PART
					*/
					colocacao_npc = this->getPositionNPC(cpu1,player1,cpu2,cpu3);
					vector3df cpu1Pos = cpu1->getPosition();
					inteligenceArtificial_npc1->refreshExternalVariables(velocidade_npc_1,player1->getVelocidade(),cpu1Pos,
																		player1->getPosition(),colocacao_npc,colocacao_player);
					if(inteligenceArtificial_npc1->shouldIrunFaster() == true){
						velocidade_npc_1 += inteligenceArtificial_npc1->getAceleracao();
					}

					// Desacelera Antes da Curva
					
					if(cpu1->getPosition().X >= 6763.3208 && cpu1->getLapFlag() == 0) velocidade_npc_1 = inteligenceArtificial_npc1->getVelocidadeMin();
					
					// Anda para Frente Caso lapflag for falso
					if(cpu1->getPosition().X <= 7248.1719 && cpu1->getLapFlag() == 0){
						cpu1Pos.X += velocidade_npc_1;
					}
					else{ 
						if(cpu1->getLapFlag() == 0)cpu1->setLapFlag(1);
					}

					//Faz a curva quando bater X 7196.1719
					if(cpu1->getPosition().X >= 7196.1719 && cpu1->getLapFlag() == 1){
						cpu1->rotateNPC(0,-135,0);
						cpu1Pos.Z +=velocidade_npc_1;
						if(cpu1Pos.Z >= 55.286934){
							if(cpu1->getLapFlag() == 1)cpu1->rotateNPC(0,90,0),inteligenceArtificial_npc1->resetStamina(),
																										cpu1->setLapFlag(2);
						}

					}
					// Desce at� o Final
					if(cpu1->getPosition().X >= -812.32806 && cpu1->getLapFlag() == 2)cpu1Pos.X += (velocidade_npc_1*-1);
					else if(cpu1->getLapFlag() == 2) cpu1->setLapFlag(3);					

					if(queimou_largada == false)cpu1->setPosition(cpu1Pos);

					/*
					NPC 2 PART
					*/
					colocacao_npc = this->getPositionNPC(cpu2,player1,cpu1,cpu3);
					vector3df cpu2Pos = cpu2->getPosition();
					inteligenceArtificial_npc2->refreshExternalVariables(velocidade_npc_2,player1->getVelocidade(),cpu2Pos,
																		player1->getPosition(),colocacao_npc,colocacao_player);
					if(inteligenceArtificial_npc2->shouldIrunFaster() == true){
						velocidade_npc_2 += inteligenceArtificial_npc2->getAceleracao();
					}
					// Desacelera Antes da Curva
					if(cpu2->getPosition().X >= 6763.3208 && cpu2->getLapFlag() == 0) velocidade_npc_2 = inteligenceArtificial_npc2->getVelocidadeMin();
					// Anda para Frente Caso lapflag for falso
					if(cpu2->getPosition().X <= 7248.1719 && cpu2->getLapFlag() == 0){
						cpu2Pos.X += velocidade_npc_2;
					}
					else{ 
						if(cpu2->getLapFlag() == 0)cpu2->setLapFlag(1);
					}

					//Faz a curva quando bater X 7196.1719
					if(cpu2->getPosition().X >= 7196.1719 && cpu2->getLapFlag() == 1){
						cpu2->rotateNPC(0,-135,0);
						cpu2Pos.Z +=velocidade_npc_2;
						if(cpu2Pos.Z >= 425.54263){
							if(cpu2->getLapFlag() == 1)cpu2->rotateNPC(0,90,0),inteligenceArtificial_npc2->resetStamina(),
																										cpu2->setLapFlag(2);
						}

					}
					// Desce at� o Final
					if(cpu2->getPosition().X >= -812.32806 && cpu2->getLapFlag() == 2){
						cpu2Pos.X += (velocidade_npc_2*-1);
					}
					else if(cpu2->getLapFlag() == 2) cpu2->setLapFlag(3);
					if(queimou_largada == false)cpu2->setPosition(cpu2Pos);

					/*
					NPC 3 PART
					*/
					colocacao_npc = this->getPositionNPC(cpu3,player1,cpu1,cpu2);
					vector3df cpu3Pos = cpu3->getPosition();
					inteligenceArtificial_npc3->refreshExternalVariables(velocidade_npc_3,player1->getVelocidade(),cpu3Pos,
																		player1->getPosition(),colocacao_npc,colocacao_player);
					if(inteligenceArtificial_npc3->shouldIrunFaster() == true){
						velocidade_npc_3 += inteligenceArtificial_npc1->getAceleracao();
					}

					// Desacelera Antes da Curva
					if(cpu3->getPosition().X >= 6763.3208 && cpu3->getLapFlag() == 0) velocidade_npc_3 = inteligenceArtificial_npc3->getVelocidadeMin();	
					// Anda para Frente Caso lapflag for falso
					if(cpu3->getPosition().X <= 7248.1719 && cpu3->getLapFlag() == 0){
						cpu3Pos.X += velocidade_npc_2;
					}
					else{ 
						if(cpu3->getLapFlag() == 0)cpu3->setLapFlag(1);
					}

					//Faz a curva quando bater X 7196.1719
					if(cpu3->getPosition().X >= 7196.1719 && cpu3->getLapFlag() == 1){
						cpu3->rotateNPC(0,-135,0);
						cpu3Pos.Z +=velocidade_npc_3;
						if(cpu3Pos.Z >= 612.63312){
							if(cpu3->getLapFlag() == 1)cpu3->rotateNPC(0,90,0),inteligenceArtificial_npc3->resetStamina(),cpu3->setLapFlag(2);
						}

					}
					// Desce at� o Final
					if(cpu3->getPosition().X >= -812.32806 && cpu3->getLapFlag() == 2)cpu3Pos.X += (velocidade_npc_3*-1);
					else if(cpu3->getLapFlag() == 2) cpu3->setLapFlag(3);

					if(queimou_largada == false)cpu3->setPosition(cpu3Pos);


					cpu1->setModelSpeed(inteligenceArtificial_npc1->getCurrentSpeed());
					cpu2->setModelSpeed(inteligenceArtificial_npc2->getCurrentSpeed());
					cpu3->setModelSpeed(inteligenceArtificial_npc3->getCurrentSpeed());

					/*
					FIM NPCS PART
					*/

					if(player1->getPosition().X >= 7171.6719){
						player1->setLapFlag(true);
						if(once_play_torcida == false){
							this->torcida_gritando = sound_engine->play3D("system//sounds//APLAUSOS_2.mp3",
	                              vec3df(0,0,0), false, false, true);
							this->torcida_gritando->setIsLooped(false);
							once_play_torcida = true;
						}
					}
					if(player1->getLapFlag() == true && player1->getPosition().X <= -812.32806){
					ending_options = 1;
					break;
				}
					
		}
            this->smgr->drawAll();
            this->guienv->drawAll();

            this->driver->endScene();
			
    }
	int frame_final;
	bool vitoria = false;
	
	if(quit == false && ending_options == 1){
		if(colocacao_player == 1){
			position_final_image->setImage(driver->getTexture("system//images//ingame//position//finalPosition//1.png"));
			player1->setMesh("system//models//menina//menina_W.md2"); 
			frame_final = player1->getNode()->getEndFrame(), vitoria = true;
			this->torcida_gritando = sound_engine->play3D("system//sounds//APLAUSOS_4.mp3",
	                              vec3df(0,0,0), false, false, true);
					this->torcida_gritando->setIsLooped(false);
		}else if(colocacao_player == 2)position_final_image->setImage(driver->getTexture("system//images//ingame//position//finalPosition//2.png")), player1->getNode()->setLoopMode(false),player1->setMesh("system//models//menina//menina_CANSADA.md2");
		else if(colocacao_player == 3)position_final_image->setImage(driver->getTexture("system//images//ingame//position//finalPosition//3.png")), player1->getNode()->setLoopMode(false),player1->setMesh("system//models//menina//menina_CANSADA.md2");
		else if(colocacao_player == 4)position_final_image->setImage(driver->getTexture("system//images//ingame//position//finalPosition//4.png")), player1->getNode()->setLoopMode(false),player1->setMesh("system//models//menina//menina_CANSADA.md2");
		position_small->setVisible(false);
		continue_space->setVisible(true);
		position_final_image->setVisible(true);
	}else if ( ending_options == 2){
		position_small->setVisible(false);
		continue_space->setVisible(true);
		ultrapassou_raia->setVisible(true);
		vai_image->setVisible(false);
		player1->getNode()->setLoopMode(false),player1->setMesh("system//models//menina//menina_CANSADA.md2");
	}
	cpu1->setMesh("system//models//menina//menina_I.md2");
	cpu2->setMesh("system//models//menina//menina_I.md2");
	cpu3->setMesh("system//models//menina//menina_I.md2");

	receiver->clearKeyDown(KEY_SPACE); // LIMPANDO BUFFER DO TECLADO
	bool once_cansa = false;
	bool once_cansa2 = false;
	this->musica_corrida->stop();
	if(vitoria == false){
		this->musica_derrota->setIsPaused(false);
	}else{
		this->musica_vitoria->setIsPaused(false);
	}
	while(this->device->run())
    {
			if(quit == true)break;
			bool space_key_down = receiver->IsKeyDown(KEY_SPACE);
			if(once_cansa == false && vitoria == false){
						once_cansa = true; 
						player1->getNode()->setLoopMode(false); 
						player1->setMesh("system//models//menina//menina_CANSADA.md2");
						frame_final = player1->getNode()->getEndFrame();
			}
			if((frame_final-10) <= player1->getNode()->getFrameNr() && once_cansa2 == false){
				player1->getNode()->setLoopMode(true); 
				if(vitoria == false){
					player1->setMesh("system//models//menina//menina_CANSADA_loop.md2");
				}else{
					player1->setMesh("system//models//menina//menina_W_loop.md2");
				}	
				once_cansa2 = true;
			}
			this->driver->beginScene(true, true, SColor(255,100,101,140));
			player1->turnCameraPlayer(this->device,0,25.5,0,0,0.2);
		    if(space_key_down)break;
			this->smgr->drawAll();
            this->guienv->drawAll();
            this->driver->endScene();
	}
	// Finalizando audios
	this->torcida->stop();
	this->torcida_gritando->stop();
	player1->setPauseAllSounds();
	// Fim finalizando audios
	if(colocacao_player == 1 && queimou_largada == false && ending_options == 1) return 1;
	else if(quit == true) return 2;
	else return 0;
}

short Stage::getPosition(Main_Char* player1, NPC *cpu1, NPC *cpu2, NPC *cpu3){
	short count = 0;
	if(player1->getLapFlag() == false){
		if(player1->getPosition().X > cpu1->getPosition().X && cpu1->getLapFlag() == 0)count++;
		if(player1->getPosition().X > cpu2->getPosition().X && cpu2->getLapFlag() == 0)count++;
		if(player1->getPosition().X > cpu3->getPosition().X && cpu3->getLapFlag() == 0)count++;
	}else{
		if(player1->getPosition().X < cpu1->getPosition().X && cpu1->getLapFlag() >= 0)count++;
		else if(cpu1->getLapFlag() == 0)count++;
		if(player1->getPosition().X < cpu2->getPosition().X && cpu2->getLapFlag() >= 0)count++;
		else if(cpu2->getLapFlag() == 0)count++;
		if(player1->getPosition().X < cpu3->getPosition().X && cpu3->getLapFlag() >= 0)count++;
		else if(cpu3->getLapFlag() == 0)count++;
	}
	
	if(count == 0)return 4;
	if(count == 1)return 3;
	if(count == 2)return 2;
	if(count == 3)return 1;
}

short Stage::getPositionNPC(NPC *cpu1, Main_Char* player1, NPC *cpu2, NPC *cpu3){
	short count = 0;
	if(this->lapflag == false){
		if(cpu1->getPosition().X > player1->getPosition().X && player1->getLapFlag() == false)count++;
		if(cpu1->getPosition().X > cpu2->getPosition().X && cpu2->getLapFlag() == 0)count++;
		if(cpu1->getPosition().X > cpu3->getPosition().X && cpu3->getLapFlag() == 0)count++;
	}else{
		if(cpu1->getPosition().X < player1->getPosition().X && player1->getLapFlag() == true)count++;
		else if(player1->getLapFlag() == false)count++;
		if(cpu1->getPosition().X < cpu2->getPosition().X && cpu2->getLapFlag() >= 0)count++;
		else if(cpu2->getLapFlag() == 0)count++;
		if(cpu1->getPosition().X < cpu3->getPosition().X && cpu3->getLapFlag() >= 0)count++;
		else if(cpu3->getLapFlag() == 0)count++;
	}
	if(count == 0)return 4;
	if(count == 1)return 3;
	if(count == 2)return 2;
	if(count == 3)return 1;
}

void Stage::setLightning(bool enabled){
	this->lightning = enabled;
}

int Stage::rand_intRange(int a, int b){
	/*
		M�todo que randomiza um valor float para n�s.
	*/
	return ((b-a)*((int)rand()/RAND_MAX))+a;
}
