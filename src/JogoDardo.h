/*
Header para a classe do jogo de dardos
�bner Zanetti
*/

#ifndef __JOGODARDO_H_INCLUDED__
#define __JOGODARDO_H_INCLUDED__

#include <irrlicht.h>
#include <irrKlang.h>
#include "Main_Char.h"
#include "Main_Char.h"
#include "Menu_Questao.h"

#include "MenuNovoJogo.h"


using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;
using namespace std;
using namespace irrklang;

#ifdef _IRR_WINDOWS_
#pragma comment(lib, "Irrlicht.lib")
#pragma comment(lib, "irrKlang.lib") // link with irrKlang.dll
#pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#endif



enum eBtnJogoDardo
{
	GUI_ID_BUTTON_JD_MENU_FASES = 1,
	GUI_ID_BUTTON_JD_WINDOWS_CLOSE = 16,
	GUI_ID_BUTTON_JD_WINDOWS_MINIMIZE = 17,
	GUI_ID_BUTTON_JD_MAIN_MENU = 18

};




class JogoDardo : public IEventReceiver{

    public:
	    JogoDardo(IVideoDriver* driver, 
			      ISceneManager* smgr,
			      IGUIEnvironment* guienv, 
			      IrrlichtDevice *device,
				  ISoundEngine* sound_engine,
				  struct sJogo* jogo);

		void createJogoDardo(short dificuldade);
		
		virtual bool OnEvent(const SEvent& event);
        virtual ~JogoDardo(void);
		

	private:
		IVideoDriver* driver;
		ISoundEngine* sound_engine;
		ISound* intro_music;
		ISceneManager* smgr;
		IGUIEnvironment* guienv;
		IrrlichtDevice* device;
		struct sJogo* jogo;
		//JogoDardo receiver_options_jogodardo;

		int w_width;
		int w_height;

		void drawMainChar();
		void drawLaneFloor();
		void activateMainCamera();

		void drawBackground();
		void drawWindowButtons();
		void drawSkipButton();



};


#endif
