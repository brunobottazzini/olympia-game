#include "MenuPersonagemSelecao.h"
#include "MenuNovoJogo.h"
#include "MenuPrincipal.h"
#include "IntroCorrida.h"

MenuPersonagemSelecao::MenuPersonagemSelecao(IVideoDriver* driver, 
			         ISceneManager* smgr,
			         IGUIEnvironment* guienv, 
			         IrrlichtDevice *device,
				     ISoundEngine* sound_engine,
					 struct sJogo* jogo){

	this->driver = driver;
	this->smgr = smgr;
	this->guienv = guienv;
	this->device = device;
	this->sound_engine = sound_engine;
	this->jogo = jogo;

	this->w_width = driver->getScreenSize().Width;  // largura da tela
	this->w_height = driver->getScreenSize().Height; // altura da tela

	this->what_char = MASCULINO;
	this->jogo->char_texture_index = 1;
	this->jogo->selected_gender = MASCULINO;

}


// M�TODO: createMenuPersonagemSelecao
// DESCRI��O: d� in�cio � cria��o da tela de edi��o do personagem
// ARGUMENTOS: nenhum
// RETORNO: void
void MenuPersonagemSelecao::createMenuPersonagemSelecao(){

	MenuPersonagemSelecao receiver_options_menupersonagemselecao(this->driver, this->smgr, this->guienv, this->device, this->sound_engine, this->jogo);
	this->device->setEventReceiver(&receiver_options_menupersonagemselecao);

	// Limpa a cena e os elementos GUI
	this->smgr->clear();
	this->guienv->clear();

	drawBackground();
	drawCharWindow();
	drawNameTextBox();
	drawChooseGenderTitle();
	drawChooseAppearenceTitle();
    drawGenderText();
	drawAppearanceIndexText();
	drawSelectionButtons();
	drawStarButton();
	drawWindowButtons();

	// Desenha a cena criada
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(0,0,0,0));
		this->smgr->drawAll();
		this->guienv->drawAll();
        this->driver->endScene();
	}

}



void MenuPersonagemSelecao::drawCharWindow(){
    
	IGUIImage* img_char = guienv->addImage(core::rect<s32>( (w_width / 2) - 262 , 120, (w_width / 2), 510), 0);
	img_char->setScaleImage(false);
	img_char->setUseAlphaChannel(true);

	int index = this->jogo->char_texture_index;
	ITexture* texture_genero;

	if(this->jogo->selected_gender == MASCULINO){

		switch(index){

		    case 1:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//menino//menino_branco_1.png");
				break;
		    case 2:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//menino//menino_branco_2.png");
				break;
		    case 3:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//menino//menino_branco_3.png");
				break;
		    case 4:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//menino//menino_branco_4.png");
				break;
			case 5:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//menino//menino_bege_1.png");
				break;
			case 6:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//menino//menino_bege_2.png");
				break;
			case 7:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//menino//menino_bege_3.png");
				break;
			case 8:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//menino//menino_bege_4.png");
				break;
			case 9:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//menino//menino_verde_1.png");
				break;
			case 10:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//menino//menino_verde_2.png");
				break;
			case 11:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//menino//menino_verde_3.png");
				break;
			case 12:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//menino//menino_verde_4.png");
				break;
			case 13:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//menino//menino_vermelho_1.png");
				break;
			case 14:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//menino//menino_vermelho_2.png");
				break;
			case 15:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//menino//menino_vermelho_3.png");
				break;
			case 16:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//menino//menino_vermelho_4.png");
				break;

		}


		img_char->setImage( texture_genero );

	}else{

		switch(index){

		    case 1:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//menina//menina_branco_1.png");
				break;
		    case 2:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//menina//menina_branco_2.png");
				break;
		    case 3:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//menina//menina_branco_3.png");
				break;
		    case 4:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//menina//menina_branco_4.png");
				break;
			case 5:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//menina//menina_bege_1.png");
				break;
			case 6:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//menina//menina_bege_2.png");
				break;
			case 7:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//menina//menina_bege_3.png");
				break;
			case 8:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//menina//menina_bege_4.png");
				break;
			case 9:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//menina//menina_verde_1.png");
				break;
			case 10:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//menina//menina_verde_2.png");
				break;
			case 11:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//menina//menina_verde_3.png");
				break;
			case 12:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//menina//menina_verde_4.png");
				break;
			case 13:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//menina//menina_vermelho_1.png");
				break;
			case 14:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//menina//menina_vermelho_2.png");
				break;
			case 15:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//menina//menina_vermelho_3.png");
				break;
			case 16:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//menina//menina_vermelho_4.png");
				break;

		}

		img_char->setImage( texture_genero );


	}


}




void MenuPersonagemSelecao::drawNameTextBox(){

	IGUIImage* img_digite_nome = guienv->addImage(core::rect<s32>((w_width / 2) + 10 , 120, (w_width / 2) + 266, 152), 0);
	img_digite_nome->setScaleImage(false);
	img_digite_nome->setUseAlphaChannel(true);
	img_digite_nome->setImage( driver->getTexture("system//images//menus//menu_personagens//txt_didigte_nome.png") );

    IGUIFont* fonte = device->getGUIEnvironment()->getFont("system//font//NewAthenaUnicode14px//newathenaunicode_14px.xml");
    core::rect< s32 > rectEditBox((w_width / 2) + 10 , 150, (w_width / 2) + 266, 182);
	this->name_text = guienv->addEditBox(L"", rectEditBox, true, 0, -1);
	this->name_text->setOverrideFont(fonte);
	this->name_text->setOverrideColor(video::SColor(255, 255, 255, 255));

	/*stringw str = L"G�nero: ";
	str += this->what_char;
	this->name_text->setText(str.c_str());*/

}



void MenuPersonagemSelecao::drawSelectionButtons(){



	// Bot�o para selecionar o personagem a esquerda
	IGUIButton* btn_esquerda = guienv->addButton(rect<s32>( (w_width / 2) + 10, 242, (w_width / 2) + 42, 274 ), 0, GUI_ID_BUTTON_SELECT_LEFT, L"");
	btn_esquerda->setImage(driver->getTexture("system//images//menus//menu_personagens//btn_esquerda.png"));

	// Bot�o para selecionar o personagem a direita
	IGUIButton* btn_direita = guienv->addButton(rect<s32>( (w_width / 2) + 234, 242, (w_width / 2) + 266, 274 ), 0, GUI_ID_BUTTON_SELECT_RIGHT, L"");
	btn_direita->setImage(driver->getTexture("system//images//menus//menu_personagens//btn_direita.png"));



	// Bot�o para selecionar a aparencia a esquerda
	IGUIButton* btn_ap_esquerda = guienv->addButton(rect<s32>( (w_width / 2) + 10, 337, (w_width / 2) + 42, 369 ), 0, GUI_ID_BUTTON_SELECT_AP_LEFT, L"");
	btn_ap_esquerda->setImage(driver->getTexture("system//images//menus//menu_personagens//btn_esquerda.png"));

	// Bot�o para selecionar a aparencia a direita
	IGUIButton* btn_ap_direita = guienv->addButton(rect<s32>( (w_width / 2) + 234, 337, (w_width / 2) + 266, 369 ), 0, GUI_ID_BUTTON_SELECT_AP_RIGHT, L"");
	btn_ap_direita->setImage(driver->getTexture("system//images//menus//menu_personagens//btn_direita.png"));



}


void MenuPersonagemSelecao::drawChooseGenderTitle(){

	IGUIImage* img_escolha_genero = guienv->addImage(core::rect<s32>((w_width / 2) + 10 , 210, (w_width / 2) + 266, 242), 0);
	img_escolha_genero->setScaleImage(false);
	img_escolha_genero->setUseAlphaChannel(true);
	img_escolha_genero->setImage( driver->getTexture("system//images//menus//menu_personagens//txt_escolha_genero.png") );

}


void MenuPersonagemSelecao::drawChooseAppearenceTitle(){

	IGUIImage* img_escolha_aparencia = guienv->addImage(core::rect<s32>((w_width / 2) + 10 , 300, (w_width / 2) + 266, 332), 0);
	img_escolha_aparencia->setScaleImage(false);
	img_escolha_aparencia->setUseAlphaChannel(true);
	img_escolha_aparencia->setImage( driver->getTexture("system//images//menus//menu_personagens//txt_escolha_aparencia.png") );

}


void MenuPersonagemSelecao::drawGenderText(){


	if( getWhatChar() == 0 ){
	    
		texture_genero = driver->getTexture("system//images//menus//menu_personagens//txt_genero_masculino.png");

	}else{
		
	    texture_genero = driver->getTexture("system//images//menus//menu_personagens//txt_genero_feminino.png");

	}

	this->img_genero = guienv->addImage(core::rect<s32>((w_width / 2) + 10 , 242, (w_width / 2) + 266, 274), 0);
	this->img_genero->setScaleImage(false);
	this->img_genero->setUseAlphaChannel(true);
	this->img_genero->setImage( texture_genero );

}


void MenuPersonagemSelecao::drawAppearanceIndexText(){

	int index = this->jogo->char_texture_index;
	    
		switch(index){

		    case 1:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//txt_aparencia_1_16.png");
				break;
		    case 2:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//txt_aparencia_2_16.png");
				break;
		    case 3:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//txt_aparencia_3_16.png");
				break;
		    case 4:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//txt_aparencia_4_16.png");
				break;
			case 5:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//txt_aparencia_5_16.png");
				break;
			case 6:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//txt_aparencia_6_16.png");
				break;
			case 7:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//txt_aparencia_7_16.png");
				break;
			case 8:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//txt_aparencia_8_16.png");
				break;
			case 9:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//txt_aparencia_9_16.png");
				break;
			case 10:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//txt_aparencia_10_16.png");
				break;
			case 11:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//txt_aparencia_11_16.png");
				break;
			case 12:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//txt_aparencia_12_16.png");
				break;
			case 13:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//txt_aparencia_13_16.png");
				break;
			case 14:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//txt_aparencia_14_16.png");
				break;
			case 15:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//txt_aparencia_15_16.png");
				break;
			case 16:
				texture_genero = driver->getTexture("system//images//menus//menu_personagens//txt_aparencia_16_16.png");
				break;

		}


	this->img_genero = guienv->addImage(core::rect<s32>((w_width / 2) + 10 , 337, (w_width / 2) + 266, 369), 0);
	this->img_genero->setScaleImage(false);
	this->img_genero->setUseAlphaChannel(true);
	this->img_genero->setImage( texture_genero );

}



void MenuPersonagemSelecao::drawStarButton(){

    // Bot�o para selecionar o personagem a esquerda
	IGUIButton* btn_esquerda = guienv->addButton(rect<s32>( (w_width / 2) + 10, 436, (w_width / 2) + 266, 500 ), 0, GUI_ID_BUTTON_MPS_START, L"");
	btn_esquerda->setImage(driver->getTexture("system//images//menus//menu_personagens//btn_iniciar_campanha.png"));


}




void MenuPersonagemSelecao::drawWindowButtons(){


	// Bot�o para voltar ao menu principal
	IGUIButton* btn_menu_principal = guienv->addButton(rect<s32>(10, w_height - 138, 138, w_height - 74), 0, GUI_ID_BUTTON_MPS_MAIN_MENU, L"");
	btn_menu_principal->setImage(driver->getTexture("system//images//menus//btn_voltar_menu_principal.png"));

	// Bot�o para minimizar a janela
	IGUIButton* btn_window_minimize = guienv->addButton(rect<s32>(w_width - 64, 0, w_width - 32, 32), 0, GUI_ID_BUTTON_MPS_WINDOWS_MINIMIZE, L"");
	btn_window_minimize->setImage(driver->getTexture("system//images//menus//btn_window_minimize.png"));

	// Bot�o para fechar a janela
	IGUIButton* btn_window_close = guienv->addButton(rect<s32>(w_width - 32, 0, w_width, 32), 0, GUI_ID_BUTTON_MPS_WINDOWS_CLOSE, L"");
	btn_window_close->setImage(driver->getTexture("system//images//menus//btn_window_close.png"));


}




void MenuPersonagemSelecao::drawBackground(){


	IGUIImage* bg_img = guienv->addImage(core::rect<s32>(0 ,0, w_width, w_height), 0);
	bg_img->setScaleImage(true);
	bg_img->setImage( driver->getTexture("system//images//menus//bg_menus.jpg") );

	IGUIImage* img_rodape = guienv->addImage(core::rect<s32>(0, w_height - 64, w_width, w_height), 0);
	img_rodape->setScaleImage(false);
	img_rodape->setUseAlphaChannel(true);
	img_rodape->setImage( driver->getTexture("system//images//menus//menus_rodape.png") );

	IGUIImage* bg_title = guienv->addImage(core::rect<s32>( (w_width / 2) - 256 , 50, (w_height / 2) + 512, 105), 0);
	bg_title->setScaleImage(false);
	bg_title->setUseAlphaChannel(true);
	bg_title->setImage( driver->getTexture("system//images//menus//menu_personagens//title_personagem_seleca.png"));

	IGUIImage* bg_game_title = guienv->addImage(core::rect<s32>(0 ,0, 256, 256), 0);
	bg_game_title->setScaleImage(false);
	bg_game_title->setUseAlphaChannel(true);
	bg_game_title->setImage( driver->getTexture("system//images//menus//btn_game_olympia.png") );


}




void MenuPersonagemSelecao::setWhatChar(){

	if( getWhatChar() == MASCULINO ){
	    this->what_char = FEMININO;
		this->jogo->selected_gender = FEMININO;
	}else{
		this->what_char = MASCULINO;
		this->jogo->selected_gender = MASCULINO;
	}
	
	drawCharWindow();
	drawGenderText();
	drawSelectionButtons();

	/*IGUIStaticText* txt_paginacao = this->guienv->addStaticText(L"", core::rect<s32>(15,300,200,350), false, false, 0);
	txt_paginacao->setOverrideColor(video::SColor(0xff597d3d));
	txt_paginacao->setTextAlignment(EGUIA_CENTER, EGUIA_UPPERLEFT);
	txt_paginacao->setText( L"" );
	stringw str = L"Genero ";
	str += this->jogo->selected_gender;
	txt_paginacao->setText( str.c_str() );*/

}


void MenuPersonagemSelecao::setCharAppearence(int direction){

	if(direction == 1){
		decrementCharIndex();
	}else{
		incrementCharIndex();
	}
	
	drawCharWindow();
	drawAppearanceIndexText();
	drawSelectionButtons();

}



void MenuPersonagemSelecao::incrementCharIndex(){

	if( this->jogo->char_texture_index >= 16 ){
		this->jogo->char_texture_index = 1;
	}else{
		this->jogo->char_texture_index ++;
	}

}


void MenuPersonagemSelecao::decrementCharIndex(){

	if( this->jogo->char_texture_index <= 1 ){
		this->jogo->char_texture_index = 16;
	}else{
		this->jogo->char_texture_index --;
	}

}


int MenuPersonagemSelecao::getWhatChar(){

	return this->jogo->selected_gender;

}



// M�TODO: OnEvent - sobrecarga
// DESCRI��O: trata eventos ocorridos na tela (clique de mouse, teclas pressionadas, etc)
// ARGUMENTOS:
//     - const SEvent& event: struct contendo dados sobre o evento
// RETORNO: True se o evento foi processado
bool MenuPersonagemSelecao::OnEvent(const SEvent& event){

	MenuPrincipal* menu_principal = new MenuPrincipal(this->driver, 
		                                              this->smgr,
		                                              this->guienv, 
										              this->device,
										              this->sound_engine);

	IntroCorrida* intro_corrida = new IntroCorrida(this->driver, 
		                                  this->smgr,
										  this->guienv, 
										  this->device,
										  this->sound_engine,
										  this->jogo);


	/*
		Neste metodo e descrito acoes que serao dadas caso haja um botao (que foi passado o ID) ira realizar
		Lembre-se que este ID e declarado no struct enum.
	*/
	if (event.EventType == EET_GUI_EVENT){

		s32 id = event.GUIEvent.Caller->getID();
		IGUIEnvironment* env = this->device->getGUIEnvironment();

		switch(event.GUIEvent.EventType){
			
			// Caso o tipo de evento seja clique em um bot�o da GUI
			case EGET_BUTTON_CLICKED:
				switch(id){

					
				   case GUI_ID_BUTTON_SELECT_LEFT:
				       setWhatChar();
					   return true;

				   case GUI_ID_BUTTON_SELECT_RIGHT:
				       setWhatChar();
					   return true;

				   case GUI_ID_BUTTON_SELECT_AP_LEFT:
					   setCharAppearence(1);
					   return true;

				   case GUI_ID_BUTTON_SELECT_AP_RIGHT:
				       setCharAppearence(2);
					   return true;

				   case GUI_ID_BUTTON_MPS_START:
					   this->sound_engine->stopAllSounds();
					   intro_corrida->createIntroCorrida();
						return true;

					case GUI_ID_BUTTON_MPS_MAIN_MENU:
						menu_principal->createMenuPrincipal();
						return true;


				    case GUI_ID_BUTTON_MPS_WINDOWS_MINIMIZE:
						this->device->minimizeWindow();
						return true;
					case GUI_ID_BUTTON_MPS_WINDOWS_CLOSE:
						exit(1);
						return true;

					default:
						return false;

				}
				break;

			default:
				break;
		}
	}

	return false;
}




MenuPersonagemSelecao::~MenuPersonagemSelecao(void){
}
