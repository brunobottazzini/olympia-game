#ifndef __MAIN_CHAR_H_INCLUDED__
#define __MAIN_CHAR_H_INCLUDED__

#include <irrlicht.h>
#include <irrKlang.h>
#include "intro.h"


using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;
using namespace std;
using namespace irrklang;

// LIMITE DE VELOCIDADE DEFINIDO
#define VELOCIDADE_MIN 2
#define VELOCIDADE_MAX 6
// QUANTO QUE SUBTRAI E SOMA POR ITERACAO
#define SUM_VELOCIDADE 0.5
#define SUB_VELOCIDADE 0.5
//
#define SENSIBILIDADE 0.2
#define MAX_SENSE 5


#ifdef _IRR_WINDOWS_
#pragma comment(lib, "Irrlicht.lib")
#pragma comment(lib, "irrKlang.lib") // link with irrKlang.dll
#pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#endif


class Main_Char{
public:
	  Main_Char(IrrlichtDevice* device, IVideoDriver* driver, MyEventReceiver* receiver, char caminhoModelo [], char caminhoTextura [], int size_w, int size_h, bool lightning);
   	  void setPosition(float posX, float posY, float posZ);
	  void showInterface(bool show);
	  void setPosition(vector3df npcPos);
	  void setAnimationMD2(irr::scene::EMD2_ANIMATION_TYPE anim);
	  void setAnimationMD2(char path[]);
	  void setScale(float posX, float posY, float posZ);
	  void setScale(vector3df scale);
	  void moveCameraControl(IrrlichtDevice *device);
	  void rotateNPC(float x, float y, float z);
	  void setCameraControl(bool enabled);
	  void turnCameraPlayer(IrrlichtDevice *device, float distance_x, float distance_y,float distance_z, float deepness, float speed);
	  void setHUDToBurningVideo();
	  void setLapFlag(bool enabled);
	  void setMesh(char mesh[]);
	  void setPauseAllSounds();
	  void clearCameraControl(IrrlichtDevice *device);
	  bool getLapFlag();
	  float getVelocidade();
	  bool Jogabilidade(ICameraSceneNode* camera);
	  vector3df getPosition();
	  IAnimatedMeshSceneNode* getNode();
private:
	  IAnimatedMeshSceneNode* node;
	  IAnimatedMesh* mesh;
	  IVideoDriver* driver;
	  IGUIImage* foot_left;
	  IGUIImage* foot_right;
	  IGUIImage* arrow_left;
	  IGUIImage* arrow_right;
	  IGUIImage* stamina;
	  ISound* ofegante;
	  ISound* muito_ofegante;
	  ISoundEngine* sound_engine_2;
	  ISceneManager* smgr;
	  MyEventReceiver* receiver;// Initiating objec keyboard (Look at the keyboard.h)
	  bool still_running;
	  bool switch_feet; // Variavel para Trocar os P�s
	  bool lapFlag;
	  bool setSpeed;
	  char * caminhotexture;
	  float velocidade_anim; // Velocidade Animacao
	  float velocidade_max; // Velovidade Maxima do Jogador
	  float change_x;
	  float stamina_bar_size;
	  float zdirection;
	  float direction;
	  short iteracao;
	  short iteracao_dec;
	  short iteracao_dec2;
	  short interacao_sw_foot;
	  bool ligthning;
	  bool key_d_down;
	  bool key_q_down;
	  bool key_e_down;
	  bool key_a_down;
	  bool key_d_up;
	  bool key_a_up;
	  bool key_q_up;
	  bool key_e_up;
	  bool enabledMouse;
	  int size_w;
	  int size_h;
};

#endif