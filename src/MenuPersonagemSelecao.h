/*
Header para a classe que gerencia o menu de sele��o de fases
�bner Zanetti
*/

#ifndef __MENUPERSONAGEMSELECAO_H_INCLUDED__
#define __MENUPERSONAGEMSELECAO_H_INCLUDED__

#include <irrlicht.h>
#include <irrKlang.h>


using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;
using namespace irrklang;

#ifdef _IRR_WINDOWS_
#pragma comment(lib, "Irrlicht.lib")
#pragma comment(lib, "irrKlang.lib") // link with irrKlang.dll
#pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#endif



enum eBtnMenuPersonagemSelecao
{
	
	GUI_ID_BUTTON_SELECT_LEFT = 1,
	GUI_ID_BUTTON_SELECT_RIGHT = 2,
	GUI_ID_BUTTON_SELECT_AP_LEFT = 3,
	GUI_ID_BUTTON_SELECT_AP_RIGHT = 4,
	GUI_ID_BUTTON_MPS_START = 15,
	GUI_ID_BUTTON_MPS_WINDOWS_CLOSE = 16,
	GUI_ID_BUTTON_MPS_WINDOWS_MINIMIZE = 17,
	GUI_ID_BUTTON_MPS_MAIN_MENU = 18

};

enum eWhatChar
{
	
	MASCULINO = 0,
	FEMININO = 1

};



class MenuPersonagemSelecao : public IEventReceiver{

    public:
	    MenuPersonagemSelecao(IVideoDriver* driver, 
			      ISceneManager* smgr,
			      IGUIEnvironment* guienv, 
			      IrrlichtDevice *device,
				  ISoundEngine* sound_engine,
				  struct sJogo* jogo);

		void createMenuPersonagemSelecao();
		
		virtual bool OnEvent(const SEvent& event);
        virtual ~MenuPersonagemSelecao(void);
		

	private:
		IVideoDriver* driver;
		ISoundEngine* sound_engine;
		ISceneManager* smgr;
		IGUIEnvironment* guienv;
		IrrlichtDevice *device;
		struct sJogo* jogo;
		IGUIEditBox* name_text;
		IGUIImage* img_genero;
		ITexture* texture_genero;

		int what_char;

		int w_width;
		int w_height;

		void drawCharWindow();

		void drawBackground();
		void drawWindowButtons();
		void drawSelectionButtons();
	    void drawChooseGenderTitle();
		void drawChooseAppearenceTitle();
		void drawGenderText();
		void drawAppearanceIndexText();
		void setGenderTextVisibility(IGUIImage* igm, IGUIImage* igf);
		void drawNameTextBox();
		void drawStarButton();

		void setWhatChar();
		int  getWhatChar();

		void setCharAppearence(int direction);

		void incrementCharIndex();
		void decrementCharIndex();

		void setGenderChar();

};


#endif
