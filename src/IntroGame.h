/*
Header para a classe que mostra a intro do game
�bner Zanetti
*/

#ifndef __INTROGAME_H_INCLUDED__
#define __INTROGAME_H_INCLUDED__

#include <irrlicht.h>
#include <irrKlang.h>
#include "MenuNovoJogo.h"

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;
using namespace std;
using namespace irrklang;

#ifdef _IRR_WINDOWS_
#pragma comment(lib, "Irrlicht.lib")
#pragma comment(lib, "irrKlang.lib") // link with irrKlang.dll
#pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#endif



enum eBtnIntroGame
{
	GUI_ID_BUTTON_PULAR_INTRO = 10000,
	GUI_ID_BUTTON_IG_WINDOWS_CLOSE = 10001,
	GUI_ID_BUTTON_IG_WINDOWS_MINIMIZE = 10002,
	GUI_ID_BUTTON_IG_MAIN_MENU = 10003

};




class IntroGame : public IEventReceiver{

    public:
	    IntroGame(IVideoDriver* driver, 
			      ISceneManager* smgr,
			      IGUIEnvironment* guienv, 
			      IrrlichtDevice *device,
				  ISoundEngine* sound_engine);

		void createIntroGame();
		
		virtual bool OnEvent(const SEvent& event);
        virtual ~IntroGame(void);
		

	private:
		IVideoDriver* driver;
		ISoundEngine* sound_engine;
		ISceneManager* smgr;
		IGUIEnvironment* guienv;
		IrrlichtDevice *device;
		IGUIInOutFader* fader;
		ISound* intro_music;
        
		int w_width;
		int w_height;
		int frame;
		//ITexture* tex;

		void drawIntroFrames();
		void playIntroGameNarration();
		void playGameIntro();
		void drawBackground();
		void drawWindowButtons();
		void drawSkipButton();

};


#endif
