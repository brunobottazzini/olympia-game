#include <irrlicht.h>

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;


class MyEventReceiver_menu : public IEventReceiver
{
public:
        virtual bool OnEvent(const SEvent& event);
        virtual bool IsKeyDown(EKEY_CODE keyCode);
		virtual bool IsKeyUp(EKEY_CODE keyCode);
		virtual bool IsKeyToggled(EKEY_CODE keyCode);
		virtual void clearKeyDown(EKEY_CODE keyCode);
        MyEventReceiver_menu();
private:
        bool KeyIsDownPrev[KEY_KEY_CODES_COUNT];
		bool KeyIsDown[KEY_KEY_CODES_COUNT];
		bool KeyIsToggled[KEY_KEY_CODES_COUNT];
		bool KeyInToggle[KEY_KEY_CODES_COUNT];
};

