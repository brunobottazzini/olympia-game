/*
Model of the Class to get Menu
Bruno Lara Bottazzini
*/

#ifndef __MENU_QUESTAO_H_INCLUDED__
#define __MENU_QUESTAO_H_INCLUDED__

#include <irrlicht.h>

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

#ifdef _IRR_WINDOWS_
#pragma comment(lib, "Irrlicht.lib")
#endif

struct SAppContext3
{
	IrrlichtDevice *device;
	short chosen_options;
};

/*enum btnQuiz
{
	GUI_ID_BUTTON_QUIZ_A = 10,
	GUI_ID_BUTTON_QUIZ_B = 11,
	GUI_ID_BUTTON_QUIZ_C = 12,
	GUI_ID_BUTTON_QUIZ_D = 13,
};*/

class MenuEventReceiver_QUESTIONS : public IEventReceiver
{
public:
	MenuEventReceiver_QUESTIONS(SAppContext3 &context);
	virtual bool OnEvent(const SEvent& event);
private:
	SAppContext3 & Context;
};

#endif