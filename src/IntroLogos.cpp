#include "IntroLogos.h"
#include "MenuPrincipal.h"

IntroLogos::IntroLogos(IVideoDriver* driver,
			         ISceneManager* smgr,
			         IGUIEnvironment* guienv,
			         IrrlichtDevice *device,
				     ISoundEngine* sound_engine,
					 MyEventReceiver* receiver
					 ){

	this->driver = driver;
	this->smgr = smgr;
	this->guienv = guienv;
	this->device = device;
	this->sound_engine = sound_engine;
	this->receiver_logos = receiver;
	
	this->w_width = driver->getScreenSize().Width;  // largura da tela
	this->w_height = driver->getScreenSize().Height; // altura da tela

}


// M�TODO: createMenuPrincipal
// DESCRI��O: d� in�cio � cria��o da tela com o menu de escolha de fases
// ARGUMENTOS: nenhum
// RETORNO: void
void IntroLogos::createIntroLogos(){

	IntroLogos receiver_options_intrologos(this->driver, this->smgr, this->guienv, this->device, this->sound_engine, this->receiver_logos);
	this->device->setEventReceiver(&receiver_options_intrologos);



	// Limpa a cena e os elementos GUI
	this->smgr->clear();
	this->guienv->clear();

	//drawBackground();

	//drawSkipButton();

	//drawWindowButtons();

	playIntroLogos();
		
	// Desenha a cena criada
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(0,0,0,0));
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
	}

}



void IntroLogos::playIntroLogos(){

	MenuPrincipal* menu_principal = new MenuPrincipal(this->driver, 
		                                              this->smgr,
		                                              this->guienv, 
										              this->device,
										              this->sound_engine);

	// C�digo para pular a Intro Logos (apenas para facilitar os testes)
	menu_principal->createMenuPrincipal();

	scene::ISceneNode* bill = smgr->addBillboardSceneNode(0, core::dimension2d<f32>(100, 100));
	bill->setMaterialFlag(video::EMF_LIGHTING, false);
	bill->setPosition(vector3df(0.0f,0.0f,0.0f));
    bill->setScale(vector3df(0.2f,0.2f,0.2f));
	smgr->addCameraSceneNode(0, vector3df(0,30,-40), vector3df(0,5,0));
	scene::ICameraSceneNode* camera = device->getSceneManager()->getActiveCamera();

	fader = device->getGUIEnvironment()->addInOutFader();

	if(receiver_logos->IsKeyDown(KEY_SPACE))fader->fadeIn(3000);
	bill->setMaterialTexture(0, driver->getTexture("system//images//intro//logo_metrocamp.jpg"));
	receiver_logos->clearKeyDown(KEY_SPACE);

	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		if(teste || receiver_logos->IsKeyDown(KEY_SPACE))break;
	}
	fader->fadeOut(5000);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		if(teste || receiver_logos->IsKeyDown(KEY_SPACE))break;

	}
	
	fader->fadeIn(3000);
	bill->setMaterialTexture(0, driver->getTexture("system//images//intro//logo_unesp.jpg"));
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		if(teste || receiver_logos->IsKeyDown(KEY_SPACE))break;
	}
	fader->fadeOut(5000);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		if(teste || receiver_logos->IsKeyDown(KEY_SPACE))break;
	}
	bill->setMaterialTexture(0,driver->getTexture("system//images//intro//logo_tech.jpg"));
	
	fader->fadeIn(3000);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		if(teste || receiver_logos->IsKeyDown(KEY_SPACE))break;
	}
	fader->fadeOut(3000);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		if(teste || receiver_logos->IsKeyDown(KEY_SPACE))break;
	}
	bill->setMaterialTexture(0,driver->getTexture("system//images//intro//logo_tech2.jpg"));
	
	fader->fadeIn(3000);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		if(teste || receiver_logos->IsKeyDown(KEY_SPACE))break;
	}
	fader->fadeOut(3000);
	while(this->device->run()){
		this->driver->beginScene(true, true, SColor(255,100,101,140));
		bool teste = fader->isReady();
		this->smgr->drawAll();
		this->guienv->drawAll();
		this->driver->endScene();
		if(teste || receiver_logos->IsKeyDown(KEY_SPACE))break;

		//Parte para ir a pr�xima tela
		menu_principal->createMenuPrincipal();

	}
}




void IntroLogos::drawBackground(){


	IGUIImage* bg_img = guienv->addImage(core::rect<s32>(0 ,0, w_width, w_height), 0);
	bg_img->setScaleImage(true);
	bg_img->setImage( driver->getTexture("system//images//menus//bg_menus.jpg") );

	IGUIImage* img_rodape = guienv->addImage(core::rect<s32>(0, w_height - 64, w_width, w_height), 0);
	img_rodape->setScaleImage(false);
	img_rodape->setUseAlphaChannel(true);
	img_rodape->setImage( driver->getTexture("system//images//menus//menus_rodape.png") );

	/*IGUIImage* img_game_title = guienv->addImage(core::rect<s32>( (w_width / 2) - 256 , 10, (w_height / 2) + 512, 266), 0);
	img_game_title->setScaleImage(false);
	img_game_title->setUseAlphaChannel(true);
	img_game_title->setImage( driver->getTexture("system//images//menus//menu_principal//img_game_title.png"));*/


}



void IntroLogos::drawSkipButton(){


	// Bot�o para iniciar novo jogo
	IGUIButton* btn_intro_corrida = guienv->addButton(rect<s32>( w_width - 138, w_height - 145, w_width - 10, w_height - 79), 0, GUI_ID_BUTTON_IL_PULAR_INTRO, L"");
	btn_intro_corrida->setImage(driver->getTexture("system//images//menus//btn_pular_intro.png"));


}


void IntroLogos::drawWindowButtons(){

	// Bot�o para minimizar a janela
	IGUIButton* btn_window_minimize = guienv->addButton(rect<s32>(w_width - 64, 0, w_width - 32, 32), 0, GUI_ID_BUTTON_IL_WINDOWS_MINIMIZE, L"");
	btn_window_minimize->setImage(driver->getTexture("system//images//menus//btn_window_minimize.png"));

	// Bot�o para fechar a janela
	IGUIButton* btn_window_close = guienv->addButton(rect<s32>(w_width - 32, 0, w_width, 32), 0, GUI_ID_BUTTON_IL_WINDOWS_CLOSE, L"");
	btn_window_close->setImage(driver->getTexture("system//images//menus//btn_window_close.png"));

}




// M�TODO: OnEvent - sobrecarga
// DESCRI��O: trata eventos ocorridos na tela (clique de mouse, teclas pressionadas, etc)
// ARGUMENTOS:
//     - const SEvent& event: struct contendo dados sobre o evento
// RETORNO: True se o evento foi processado
bool IntroLogos::OnEvent(const SEvent& event){

	/*MenuPrincipal* menu_principal = new MenuPrincipal(this->driver,
		                                             this->smgr,
										             this->guienv,
										             this->device,
										             this->sound_engine);*/



	/*
		Neste metodo e descrito acoes que serao dadas caso haja um botao (que foi passado o ID) ira realizar
		Lembre-se que este ID e declarado no struct enum.
	*/
	if (event.EventType == EET_GUI_EVENT){

		s32 id = event.GUIEvent.Caller->getID();
		IGUIEnvironment* env = this->device->getGUIEnvironment();

		switch(event.GUIEvent.EventType){

			// Caso o tipo de evento seja clique em um bot�o da GUI
			case EGET_BUTTON_CLICKED:
				switch(id){

					case GUI_ID_BUTTON_IL_PULAR_INTRO:
						return true;

				    case GUI_ID_BUTTON_IL_WINDOWS_MINIMIZE:
						this->device->minimizeWindow();
						return true;
					case GUI_ID_BUTTON_IL_WINDOWS_CLOSE:
						exit(1);
						return true;

					default:
						return false;

				}
				break;

			default:
				break;
		}
	}

	return false;
}




IntroLogos::~IntroLogos(void){
}
