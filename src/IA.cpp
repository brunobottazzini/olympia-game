#include "IA.h"

IA::IA(short dificuldade){
	/*
		Construtor da classe, aqui � realizado as principais configura��es para 
		a inteligencia artificial.
		Lembrando que � passado o nivel de dificuldade. e tamb�m � randomizado
		os atributos fisicos dos computadores.
	*/
	this->dificuldade = dificuldade;
	this->stamina = 0;
	this->iteracao = 0;
	//RANDOM ATRIBUTOS E CONDICAO FISICA DO PC
	if(dificuldade == 0){
		this->forca_max = this->rand_FloatRange(0.4,0.6);
		this->resistencia = this->rand_FloatRange(3.0,8.0);
		this->velocidade_max = this->rand_FloatRange(4.5,7.5);
		this->velocidade_min = this->rand_FloatRange(1.8,2.4);
	}else if(dificuldade == 1){
		this->forca_max = this->rand_FloatRange(0.5,0.7);
		this->resistencia = this->rand_FloatRange(4.0,8.0);
		this->velocidade_max = this->rand_FloatRange(5.5,7.5);
		this->velocidade_min = this->rand_FloatRange(2.0,2.4);
	}else{
		this->forca_max = this->rand_FloatRange(0.6,0.8);
		this->resistencia = this->rand_FloatRange(5.0,8.0);
		this->velocidade_max = this->rand_FloatRange(6.5,7.5);
		this->velocidade_min = this->rand_FloatRange(2.2,2.4);
	}
	//
}

float IA::getCurrentSpeed(){
	return this->velocidade_npc;
}
void IA::setDificuldade(short dificuldade){
	/*
		Metodo que para atribui�ao de dificuldade (Caso o Jogador queira mudar
		a dificuldade no meio do jogo)
	*/
	this->dificuldade = dificuldade;
}

float IA::getVelocidadeMin(){
	/*
		Metodo que retorna a velocidade MINIMA da IA que acabamos de instanciar.
		Esta velocidade minima � atirbuida aleatoriamente quando � construido 
		o objeto.
	*/
	return this->velocidade_min;
}

float IA::getAceleracao(){
	/*
		Metodo que retorna a for�a ou aceleracao da IA que acabamos de instanciar.
		Esta for�a � atirbuida aleatoriamente quando � construido 
		o objeto.
	*/
	return this->forca_max;
}

void IA::refreshExternalVariables(float velocidade_npc, float velocidade_player, vector3df posicao_npc, vector3df posicao_player, int colocacao_npc, int colocacao_player){
	/*
		Este m�todo tem o fator X para o desenvolvimento da inteligencia artificial. Mesmo ela
		sendo simples de ser "criada" � aonde a IA recebe todos os parametros de fora e com 
		estes parametros � a onde iremos desenvolver o "pensamento" da IA.

		Cada parametro passado pode ser um fator de decisao final tomada pela IA.
		Lembrando que a decisao final pode ser impedida pelo fatores fisicos do jogador onde 
		cada PC � limitado a ela.
	*/
	this->velocidade_npc = velocidade_npc;
	this->velocidade_player = velocidade_player;
	this->posicao_npc = posicao_npc;
	this->posicao_player = posicao_player;
	this->colocacao_npc = colocacao_npc;
	this->colocacao_player = colocacao_player;
}

bool IA::shouldIrunFaster(){
	/*
		Ap�s a atualizar os parametros externos � provavel que verificar se
		o PC deve ou n�o acelerar.

		Para isso � aqui que ter� a sua tomada de decis�o na qual ela retornara
		true (deve ser acelerado) ou false ( mantenha a velocidade )

		Nisso h� tipos de pensamentos atribuidos ao computador al�m dos fatores
		externos e os limites fisicos.

		Estes pensamentos s�o jogadores que tentam estipular uma m�dia de velocidade
		para n�o se desgastar.
		S�o jogadores que tentam manter a velocidade m�dia alta ou jogadores que pretendem
		apenas "detonar" sua for�a f�sica � nesse m�todo que todo isso ser� notado.

		Cada pensamento faz certas perguntas para IA onde ela processa se � verdade ou n�o as
		perguntas e depedendo como ela volta � que sera possivel tomar uma decisao.
		Por exemplo:

		Pergunta chamando M�todo:
		isMyPlaceGood - (Minha coloca��o � Boa ?) E ai neste metodo dpeendendo da dificuldade e
		etc que trar� um sim ou n�o caso for n�o h� uma POSSIBILIDADE nem seja remota do computador
		querer acelerar e ai teremos mais dois impecilhos na frente que s�o:
		A Fisica do Jogador e a Stamina..
		Fazemos estas perguntas:
		A fisica do jogador � plausivel ?
		Ele tem stamina sobrando ?
		Ele esta em sua velocidade maxima ?

		No final desse m�todo � que teremos nossa resposta se devemos ou n�o acelerar.
	*/
	int points = 0;
	if(this->iteracao >= 5){
		this->iteracao = 0;
		this->stamina += 1;
	}
	if(this->stamina >= this->resistencia){
		this->iteracao += 1;
		if(this->iteracao <= 0)this->iteracao = 0,this->stamina -= 1;
		return false;
	}
	if(this->dificuldade == 0){
		if(this->thinking == 1){
			if(this->isNPCMaxSpeed() == true)return false; // PREZA A VELOCIDADE
			if(this->isNPCSpeedGood() == false)points += 20;
			if(this->isPLSpeedTooGood() == true)points += 20;
			if(this->isMyPlaceGood() == false)points += 5;
			if(this->isNPCBehindPL() == true)points += 15;
			if(this->isMyStaminaGood() == false)points -= 30;
			if(this->isNPCFarAwayFromPL() == true) points +=15;
		}else if(this->thinking == 2){ 
			if(this->isNPCMaxSpeed() == true)return false; // PREZA A COLOCACAO
			if(this->isNPCSpeedGood() == false)points += 10;
			if(this->isPLSpeedTooGood() == true)points += 10;
			if(this->isMyPlaceGood() == false)points += 30;
			if(this->isNPCBehindPL() == true)points += 15;
			if(this->isMyStaminaGood() == false)points -= 30;
			if(this->isNPCFarAwayFromPL() == true) points +=30;
		}else if(this->thinking == 3){					
			if(this->isNPCMaxSpeed() == true)return false; // PREZA M�dia dos Fatores
			if(this->isNPCSpeedGood() == false)points += 10;
			if(this->isPLSpeedTooGood() == true)points += 10;
			if(this->isMyPlaceGood() == false)points += 20;
			if(this->isNPCBehindPL() == true)points += 20;
			if(this->isMyStaminaGood() == false)points -= 5;
			if(this->isNPCFarAwayFromPL() == true) points +=30;
		}
		if(points >= 50){
			this->iteracao += 1; 
			return true;
		}
		return false;
	}else if(this->dificuldade == 1){
		if(this->thinking == 1){
			if(this->isNPCMaxSpeed() == true)return false; // PREZA A VELOCIDADE
			if(this->isNPCSpeedGood() == false)points += 25;
			if(this->isPLSpeedTooGood() == true)points += 15;
			if(this->isMyPlaceGood() == false)points += 10;
			if(this->isNPCBehindPL() == true)points += 20;
			if(this->isMyStaminaGood() == false)points -= 20;
			if(this->isNPCFarAwayFromPL() == true) points +=20;
		}else if(this->thinking == 2){ 
			if(this->isNPCMaxSpeed() == true)return false;  // PREZA A COLOCACAO
			if(this->isNPCSpeedGood() == false)points += 10;
			if(this->isPLSpeedTooGood() == true)points += 10;
			if(this->isMyPlaceGood() == false)points += 35;
			if(this->isNPCBehindPL() == true)points += 15;
			if(this->isMyStaminaGood() == false)points -= 30;
			if(this->isNPCFarAwayFromPL() == true) points +=50;
		}else if(this->thinking == 3){
			if(this->isNPCMaxSpeed() == true)return false;
			if(this->isNPCSpeedGood() == false)points += 15;
			if(this->isPLSpeedTooGood() == true)points += 15;
			if(this->isMyPlaceGood() == false)points += 25;
			if(this->isNPCBehindPL() == true)points += 20;
			if(this->isMyStaminaGood() == false)points -= 10;
			if(this->isNPCFarAwayFromPL() == true) points +=35;
		}
		if(points >= 50){
			this->iteracao += 1; 
			return true;
		}
		return false;
	}else if(this->dificuldade >= 2){
		if(this->thinking == 1){
			if(this->isNPCMaxSpeed() == true)return false;  // PREZA A VELOCIDADE
			if(this->isNPCSpeedGood() == false)points += 30;
			if(this->isPLSpeedTooGood() == true)points += 20;
			if(this->isMyPlaceGood() == false)points += 20;
			if(this->isNPCBehindPL() == true)points += 25;
			if(this->isMyStaminaGood() == false)points -= 5;
			if(this->isNPCFarAwayFromPL() == true) points +=40;
		}else if(this->thinking == 2){ 
			if(this->isNPCMaxSpeed() == true)return false; // PREZA A COLOCACAO
			if(this->isNPCSpeedGood() == false)points += 20;
			if(this->isPLSpeedTooGood() == true)points += 20;
			if(this->isMyPlaceGood() == false)points += 40;
			if(this->isNPCBehindPL() == true)points += 25;
			if(this->isMyStaminaGood() == false)points -= 5;
			if(this->isNPCFarAwayFromPL() == true) points +=50;
		}else if(this->thinking == 3){
			if(this->isNPCMaxSpeed() == true)return false; // PREZA M�dia dos Fatoress
			if(this->isNPCSpeedGood() == false)points += 20;
			if(this->isPLSpeedTooGood() == true)points += 20;
			if(this->isMyPlaceGood() == false)points += 30;
			if(this->isNPCBehindPL() == true)points += 30;
			if(this->isMyStaminaGood() == false)points -= 5;
			if(this->isNPCFarAwayFromPL() == true) points +=50;
		}
		if(points >= 50){
			this->iteracao += 1; 
			return true;
		}
		this->iteracao -= 1;
		if(this->iteracao <= 0)this->stamina -= 1;
		return false;
	}
	this->iteracao -= 1;
	if(this->iteracao <= 0)this->stamina -= 1;
	return false;
}

bool IA::isMyStaminaGood(){
	/*
		M�todo que retorna se a STAMINA do computador esta boa ou n�o
	*/
	if(this->stamina < this->resistencia)return true;
	return false;
}

bool IA::isNPCBehindPL(){
	/*
		M�todo que retorna se o NPC esta atras ou nao do Player
	*/
	if(this->posicao_npc < this->posicao_player)return true;
	return false;
}

bool IA::isNPCSpeedGood(){
	/*
		M�todo que retorna se o NPC esta em uma velocidade razoavel
		(cria competi��es com o jogador sendo que ele tem que estar at�
		10% acima da velocidade atual do jogador
	*/
	short temp = this->velocidade_player * 1.1;
	if(velocidade_npc >= temp)return true;
	return false;
}

bool IA::isNPCFarAwayFromPL(){
	/*
		M�todo que retorna se o NPC esta muito longe do player
		(Tenta evita retardatario mas nao eh a plavra final sendo que
		h� a fisica do jogador que possa ser impedida a acelera��o)
	*/
	int temp = this->posicao_player.X - this->posicao_npc.X;
	if(temp >= 50)return true;
	else if(temp <= -50)return true;
	return false;
}

bool IA::isPLSpeedTooGood(){
	/*
		M�todo se a velocidade do jogador esta muito alta.
	*/
	short temp = this->velocidade_npc * 1.2;
	if(this->velocidade_player > velocidade_npc)return true;
	return false;
}

bool IA::isMyPlaceGood(){
	/*
		M�todo que retorna se o computador esta em uma coloca��o boa
	*/
	if(dificuldade == 1){
		if(this->colocacao_npc == 1)return true;
		if(this->colocacao_npc == 2)return true;
		if(this->colocacao_npc == 3)return false;
		if(this->colocacao_npc == 4)return false;
	}
	if(dificuldade == 2){
		if(this->colocacao_npc == 1)return true;
		if(this->colocacao_npc == 2)return false;
		if(this->colocacao_npc == 3)return false;
		if(this->colocacao_npc == 4)return false;
	}
	if(dificuldade == 3){
		if(this->colocacao_npc == 1)return true;
		if(this->colocacao_npc == 2)return false;
		if(this->colocacao_npc == 3)return false;
		if(this->colocacao_npc == 4)return false;
	}
	return false;
}

bool IA::isNPCMaxSpeed(){
	/*
		M�todo que retorna se o NPC esta em sua velocidade maxima
	*/
	if(this->velocidade_npc >= this->velocidade_max)return true;
	return false;
}

void IA::setThinking(short number){
	/*
		M�todo que atribui o tipo de pensamento do PC:
		(Conservador, Tenta estabelecer velocidade maxima, tenta
		ter uma gerencia boa de stamina e etc...)
	*/
	this->thinking = number;
}

float IA::rand_FloatRange(float a, float b){
	/*
		M�todo que randomiza um valor float para n�s.
	*/
	return ((b-a)*((float)rand()/RAND_MAX))+a;
}

void IA::resetStamina(){
	/*
		M�todo que atribui a stamina do PC para zero.
	*/
	this->stamina = 0;
}