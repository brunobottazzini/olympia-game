#ifndef __MENU_H_INCLUDED__
#define __MENU_H_INCLUDED__

/*
Model of the Class to get Menu
Bruno Lara Bottazzini
*/
#include <irrlicht.h>

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

#ifdef _IRR_WINDOWS_
#pragma comment(lib, "Irrlicht.lib")
#endif

struct SAppContext
{
	IrrlichtDevice *device;
	IGUIListBox* renderBox;
	IGUIListBox* resolutionBox;
	IGUICheckBox* full_check;
	IGUICheckBox* anti_check;
	IGUICheckBox* sync_check;
	IGUICheckBox* shadow_check;
	s32 renderOption;
	s32 resolution;
    bool startButton;
	bool fullscreen;
	bool antialiasing;
	bool shadow;
	bool synvertical;
};

enum
{
	GUI_ID_QUIT_BUTTON = 101,
	GUI_ID_WINDOW_OPTIONS,
	GUI_ID_FILE_OPEN_BUTTON,
	GUI_ID_START,
	GUI_ID_OK_BUTTON_OPTIONS,
	GUI_ID_TRANSPARENCY_SCROLL_BAR
};

class MenuEventReceiver : public IEventReceiver
{
public:
	MenuEventReceiver(SAppContext & context);
	virtual bool OnEvent(const SEvent& event);
private:
	SAppContext & Context;
	s32 selected;
	s32 resolution;
	IGUIWindow* window;
	bool fullscreen;
	bool antialiasing;
	bool shadow;
	bool color_setted;
	bool synvertical;
};

#endif