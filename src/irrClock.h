#ifndef __IRRCLOCK_H_INCLUDED__
#define __IRRCLOCK_H_INCLUDED__

#include <irrlicht.h>

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;
using namespace std;

#ifdef _IRR_WINDOWS_
#pragma comment(lib, "Irrlicht.lib")
#pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#endif

class irrClock
{
    public:
	     irrClock(IrrlichtDevice * device);
		 void clearTime();
		 void startTime();
		 void stopTime();
		 void setTime(u32 milesegundos);
		 wchar_t* getClockInString();
		 wchar_t* Count();
    private:
		 IrrlichtDevice * device;
		 ITimer* mainTime;
		 u32 milesegundos;
		 int milesegundos_t;
		 int segundos_t;
		 int minutos_t;

};

#endif